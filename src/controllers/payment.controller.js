const httpStatus = require('http-status');
const mailer = require('../utils/mailer');
const config = require('../config/config');
const catchAsync = require('../utils/catchAsync');
const {
  userService,
  stripeService,
  cardService,
  transactionService,
  orderService,
  cartService,
  couponService,
} = require('../services');
const { default: ShortUniqueId } = require('short-unique-id');

const uid = new ShortUniqueId({ dictionary: 'number', length: 10 });

const payme = catchAsync(async (req, res) => {
  //token of customer
  const token = req.body.token;
  const save_card = req.body.save_card;
  const user = req.user;
  const cartId = req.body.orderData.cartId;
  delete req.body.orderData.cartId;
  let customerId;
  let amountData = {
    amount: Math.round(req.body.orderData.payment.finalamt * 100),
    description: 'Purchase Order',
    currency: 'USD',
  };

  //make stripe id if not available
  if (!user.stripe_id) {
    let customerData = {
      fname: user.fname,
      lname: user.lname,
      email: user.email,
    };
    customerId = await stripeService.createCustomerStripe(customerData);
    user.stripe_id = customerId;
    user.save();
  } else {
    customerId = user.stripe_id;
  }

  //token type to understand if need to make new card
  const tokenType = token.split('_');
  if (tokenType[0] === 'tok') {
    const card = await stripeService.createCard(token, user.stripe_id);
    amountData['source'] = card.id;
  } else {
    amountData['source'] = token;
  }

  amountData['customer'] = customerId;

  let stripeCharge;
  let transObj;

  if (req.body.orderData.payment.finalamt === 0) {
    //create stripe charge object for payment
    transObj = {
      userId: user.id,
      amount: 0,
      type: 'Order',
      status: 'success',
    };
  } else {
    stripeCharge = await stripeService.create_charge(amountData);
    transObj = {
      userId: user.id,
      amount: stripeCharge.amount / 100,
      note: stripeCharge.description,
      type: 'Order',
      status: stripeCharge.status,
      charge: stripeCharge.id,
    };
  }

  //add transaction data to transaction model
  const transresult = await transactionService.transactionAdd(transObj);

  const bookingObj = req.body.orderData;
  const purchaseOrder = {
    ...bookingObj,
    orderId: uid(),
    userId: user.id,
    transaction_id: transresult._id,
    orderStatus: 'order confirmed',
  };

  //save the purchase data
  await orderService.placeOrder(purchaseOrder);

  await cartService.removeCartById(cartId);

  if (req.body.orderData.coupon && req.body.orderData.coupon !== '') {
    await couponService.decrimentCoupon(req.body.orderData.coupon);
  }

  // Welcome Mail //
  const welcomeMailDetails = {
    to: user.email,
    from: config.email.from,
    templateId: 'd-e47f0321cf1a442b988874647062f6e8', // Welcome Mail Template Id //
  };
  await mailer.sendEmail(welcomeMailDetails);
  // Welcome Mail //

  res.status(httpStatus.OK).send({
    serverResponse: {
      code: 200,
      message: 'Your payment is successfull!',
    },
    stripeCharge,
    transObj,
    purchaseOrder,
  });
});

module.exports = {
  payme,
};
