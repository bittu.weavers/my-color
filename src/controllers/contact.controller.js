const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const mailer = require('../utils/mailer');
const config = require('../config/config');

const sendMail = catchAsync(async (req, res) => {
  const mailData = req.body;
  const welcomeMailDetails = {
    to: mailData.email,
    from: config.email.from,
    subject: mailData.subject,
    templateId: 'd-669961c2fc14426fa2c4f4b612255281', // Welcome Mail Template Id //
    dynamicTemplateData: {
      first_name: mailData.first_name,
      last_name: mailData.last_name,
      email: mailData.email,
      number: mailData.phone_number,
      subject: mailData.subject,
      message: mailData.message,
    },
  };
  await mailer.sendEmail(welcomeMailDetails);
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: 200,
      message: 'Thank you for contacting with us, we will get back to you shortly!',
    },
  });
});
module.exports = {
  sendMail,
};
