const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const { videopostService } = require('../services');
// Add Video
const addVideo = catchAsync(async (req, res) => {
  await videopostService.addVideoData(req.body);
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: httpStatus.OK,
      message: 'Video post added successfully',
    },
  });
});
// Delete Video
const deleteVideo = catchAsync(async (req, res) => {
  await videopostService.deleteVideo(req.body.id);
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: httpStatus.OK,
      message: 'Video Code deleted successfully',
    },
  });
});
// Get Video List
const getVideoList = catchAsync(async (req, res) => {
  const videoList = await videopostService.getAllVideoList();
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: httpStatus.OK,
      message: 'Success',
    },
    videoListData: videoList,
  });
});
// Edit Video
const editVideo = catchAsync(async (req, res) => {
  const results = await videopostService.getVideoById(req.body.id);
  if (results) {
    res.status(httpStatus.OK).send({
      serverResponse: {
        code: httpStatus.OK,
        message: 'Success',
      },
      result: results,
    });
  }
});
// Update Video
const updateVideo = catchAsync(async (req, res) => {
  const results = await videopostService.updateVideoById(req.body.id, req.body);
  if (results) {
    res.status(httpStatus.OK).send({
      serverResponse: {
        code: httpStatus.OK,
        message: 'Video post updated successfully.',
      },
      result: results,
    });
  }
});
module.exports = {
  addVideo,
  deleteVideo,
  getVideoList,
  editVideo,
  updateVideo,
};
