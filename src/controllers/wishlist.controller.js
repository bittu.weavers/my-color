const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const { wishlistService } = require('../services');
// Add Product To Wishlist
const addproducttowishlist = catchAsync(async (req, res) => {
  await wishlistService.createWishlist(req);
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: 200,
      message: 'Product added to wishlist',
    },
  });
});

// Delete Product Wishlist
const deleteproductwishlist = catchAsync(async (req, res) => {
  const results = await wishlistService.deleteProductWishlistById(req);
  if (results) {
    res.status(httpStatus.OK).send({
      serverResponse: {
        code: httpStatus.OK,
        message: 'Product removed from wishlist.',
      },
    });
  }
});
const getWishlist = catchAsync(async (req, res) => {
  const current_page = req.params.current_page ? req.params.current_page : 1;
  const results = await wishlistService.getWishlist(req,current_page);
  if (results) {
    res.status(httpStatus.OK).send({
      serverResponse: {
        code: httpStatus.OK,
        message: 'Wishlist.',
      },
      results
    });
  }
});
module.exports = {
  addproducttowishlist,
  deleteproductwishlist,
  getWishlist
};
