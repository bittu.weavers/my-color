const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const { orderService, stripeService } = require('../services');
const mailer = require('../utils/mailer');
const config = require('../config/config');

const getOrder = catchAsync(async (req, res) => {
  const orderDetails = await orderService.getOrderDetails(req.params.orderid);
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: 200,
      message: 'Order Details',
    },
    orderDetails,
  });
});

const cancellOrder = catchAsync(async (req, res) => {
  const orderDetails = await orderService.cancellOrder(req.body.orderId);
  // Welcome Mail //
  const welcomeMailDetails = {
    to: req.user.email,
    from: config.email.from,
    templateId: 'd-e47f0321cf1a442b988874647062f6e8', // Welcome Mail Template Id //
  };
  await mailer.sendEmail(welcomeMailDetails);
  // Welcome Mail //
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: 200,
      message: 'Order Caneclled',
    },
    orderDetails,
  });
});

const getAllOrders = catchAsync(async (req, res) => {
  const allOrder = await orderService.getAllOrders(req.user.id);
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: 200,
      message: 'All Order Details',
    },
    allOrder,
  });
});

const getAllOrdersAdmin = catchAsync(async (req, res) => {
  const allOrder = await orderService.ListOrders(req);
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: 200,
      message: 'All Order Details',
    },
    allOrder,
  });
});

const setOrderStatus = catchAsync(async (req, res) => {
  const order = await orderService.updateStatus(req.body);
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: 200,
      message: 'Status Updated',
    },
  });
});

const refund = catchAsync(async (req, res) => {
  const order = await orderService.refundInit(req.body);
  const test = await stripeService.create_refund(order);
  let amt;
  if (req.body.amount && req.body.amount !== '') {
    amt = req.body.amount;
  } else {
    amt = null;
  }
  await orderService.afterRefundUpdates(req.body.orderId, order.transaction_id, amt);
  console.log(test);
  // Welcome Mail //
  const welcomeMailDetails = {
    to: req.body.email,
    from: config.email.from,
    templateId: 'd-e47f0321cf1a442b988874647062f6e8', // Welcome Mail Template Id //
  };
  await mailer.sendEmail(welcomeMailDetails);
  // Welcome Mail //
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: 200,
      message: 'Refund Initiated',
    },
  });
});

const orderDetails = catchAsync(async (req, res) => {
  const order = await orderService.getOrderData(req.params.id);

  res.status(httpStatus.OK).send({
    serverResponse: {
      code: 200,
      message: 'Refund Initiated',
    },
    order,
  });
});

module.exports = {
  getOrder,
  getAllOrders,
  cancellOrder,
  getAllOrdersAdmin,
  setOrderStatus,
  refund,
  orderDetails,
};
