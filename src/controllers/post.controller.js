const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const { postService } = require('../services');
const decodeHwt = require('jwt-decode');

// Add Video
const addPost = catchAsync(async (req, res) => {
  const token = req.headers.authorization ? req.headers.authorization.split(' ')[1] : '';
  await postService.addPostData(req.body, decodeHwt(token));
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: httpStatus.OK,
      message: 'Post added successfully',
    },
  });
});
// Delete Post
const deletePost = catchAsync(async (req, res) => {
  await postService.deletePost(req.params.id);
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: httpStatus.OK,
      message: 'Post deleted successfully',
    },
  });
});
// Get Post List
const getPostList = catchAsync(async (req, res) => {
  const current_page = req.params.current_page ? req.params.current_page : 1;
  const { post_type } = req.params;
  const postList = await postService.getAllPostList(current_page, post_type);
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: httpStatus.OK,
      message: 'Success',
    },
    postListData: postList,
  });
});
const getPostListFrontEnd = catchAsync(async (req, res) => {
  const current_page = req.params.current_page ? req.params.current_page : 1;
  const { post_type } = req.params;
  const postList = await postService.getAllPostListFrontEnd(current_page, post_type);
  const featurePost = current_page == 1 ? await postService.featurePost(post_type) : null;
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: httpStatus.OK,
      message: 'Success',
    },
    postListData: postList,
    featurePost,
  });
});
// Edit Post
const editPost = catchAsync(async (req, res) => {
  const results = await postService.getPostById(req.params.id);
  if (results) {
    res.status(httpStatus.OK).send({
      serverResponse: {
        code: httpStatus.OK,
        message: 'Success',
      },
      result: results,
    });
  }
});
// Update Post
const updatePost = catchAsync(async (req, res) => {
  const results = await postService.updatePostById(req.params.id, req.body);
  if (results) {
    res.status(httpStatus.OK).send({
      serverResponse: {
        code: httpStatus.OK,
        message: 'Post updated successfully.',
      },
      result: results,
    });
  }
});
// make Primary Post
const makePrimary = catchAsync(async (req, res) => {
  const results = await postService.makePrimaryById(req.params.id, req.params.post_type);
  if (results) {
    res.status(httpStatus.OK).send({
      serverResponse: {
        code: httpStatus.OK,
        message: 'Post status change successfully.',
      },
      result: results,
    });
  }
});
/**
 * Get Post By Sluge and Post type
 */
const getPostBySlug = catchAsync(async (req, res) => {
  const post = await postService.findPostBySlug(req.params.post_type, req.params.slug);
  res.status(httpStatus.OK).send({
    serverResponce: {
      code: 200,
      message: 'Post Fetch Successfully',
    },
    result: {
      post,
    },
  });
});

// Get Related Post By Post type
const getRelatedPost = catchAsync(async (req, res) => {
  const relatedPost = await postService.findRelatedPost(req.params.post_type, req.params.pexslug);
  res.status(httpStatus.OK).send({
    serverResponce: {
      code: 200,
      message: 'Related Post Fetch Successfully',
    },
    result: {
      relatedPost,
    },
  });
});

module.exports = {
  addPost,
  deletePost,
  getPostList,
  editPost,
  updatePost,
  makePrimary,
  getPostBySlug,
  getRelatedPost,
  getPostListFrontEnd,
};
