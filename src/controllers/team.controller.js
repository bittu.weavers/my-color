const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const { teamService } = require('../services');

const createTeam = catchAsync(async (req, res) => {
  const user_id = req.user.id;
  const team = await teamService.addTeamData(req.body, user_id);
  res.status(httpStatus.CREATED).send({
    serverResponse: {
      code: 200,
      message: 'Team Member created Successfully',
    },
    team,
  });
});
const getTeams = catchAsync(async (req, res) => {
  const currentTeam = req.body.currentTeam || 1;
  const teams = await teamService.getTeams(currentTeam);
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: 200,
      message: 'Teams Fetch Successfully',
    },
    teams,
  });
});
const getTeamListFrontEnd = catchAsync(async (req, res) => {
  const current_page = req.params.current_page ? req.params.current_page : 1;
  const { team_type } = req.params;
  const teamList = await teamService.getAllTeamListFrontEnd(current_page, team_type);
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: httpStatus.OK,
      message: 'Success',
    },
    teamList,
  });
});
const deleteTeam = catchAsync(async (req, res) => {
  const id = req.params.id ? req.params.id : null;
  await teamService.deleteTeam(id);
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: 200,
      message: 'Team Member Deleted Successfully',
    },
  });
});
const editTeam = catchAsync(async (req, res) => {
  const { id } = req.params;
  const team = await teamService.getTeamById(id);
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: 200,
      message: 'Team Fetch Successfully',
    },
    result: {
      team,
    },
  });
});
const updateTeam = catchAsync(async (req, res) => {
  const { id } = req.params;
  const team = await teamService.updateTeamById(id, req.body);
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: 200,
      message: 'Team Member Updated Successfully',
    },
    result: {
      team,
    },
  });
});

module.exports = {
  createTeam,
  getTeams,
  deleteTeam,
  editTeam,
  updateTeam,
  getTeamListFrontEnd,
};
