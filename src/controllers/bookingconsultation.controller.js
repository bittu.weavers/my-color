const httpStatus = require('http-status');
const mailer = require('../utils/mailer');
const config = require('../config/config');
const catchAsync = require('../utils/catchAsync');
const { bookingconsultationService, userService, transactionService, stripeService } = require('../services');

// Add Booking Consultation
const bookingconsultationadd = catchAsync(async(req, res) => {
    await bookingconsultationService.bookConsultationAdd(req.body);
    res.status(httpStatus.OK).send({
        serverResponse: {
            code: 200,
            message: 'Consultation booking is successful, please proceed with the payment!',
        },
    });
});

// Update Booking Consultation
const bookingconsultationupdate = catchAsync(async(req, res) => {
    await bookingconsultationService.bookConsultationUpdate(req.body.id, req.body);
    res.status(httpStatus.OK).send({
        serverResponse: {
            code: 200,
            message: 'Consultation booking is updated successfully',
        },
    });
});

// Status Update Booking Consultation
const bookingconsultationstatuschange = catchAsync(async(req, res) => {
    await bookingconsultationService.bookConsultationStatusUpdate(req.body.id, req.body);
    res.status(httpStatus.OK).send({
        serverResponse: {
            code: 200,
            message: 'Consultation booking status updated successfully',
        },
    });
});

// Delete Booking Consultation
const bookingconsultationdelete = catchAsync(async(req, res) => {
    await bookingconsultationService.bookConsultationDelete(req.body.id);
    res.status(httpStatus.OK).send({
        serverResponse: {
            code: 200,
            message: 'Consultation booking is deleted successfully',
        },
    });
});
// Delete Booking Consultation Admin
const bookingconsultationdeleteAdmin = catchAsync(async(req, res) => {
    await bookingconsultationService.bookConsultationDelete(req.params.id);
    res.status(httpStatus.OK).send({
        serverResponse: {
            code: 200,
            message: 'Consultation booking is deleted successfully',
        },
    });
});
// Booking Consultation Details

const bookingconsultationdetails = catchAsync(async(req, res) => {
    const result = await bookingconsultationService.bookConsultationDetails(req.body.id);
    res.status(httpStatus.OK).send({
        serverResponse: {
            code: 200,
            message: 'Success',
        },
        result: {
            bookingconsultationdetails: result,
        },
    });
});

// List Booking Consultation
const bookingconsultationlist = catchAsync(async (req, res) => {
    const user_id = req.user.id;
    const resultspastbooking = await bookingconsultationService.bookConsultationPastList(user_id);
    const resultsfuturebooking = await bookingconsultationService.bookConsultationFutureList(user_id);
    res.status(httpStatus.OK).send({
        serverResponse: {
            code: 200,
            message: 'Success',
        },
        result: {
            bookingconsultationpastlist: resultspastbooking,
            bookingconsultationfuturelist: resultsfuturebooking,
        },
    });
});
//
const getAllBooking = catchAsync(async(req, res) => {
    const pageNumber = req.params.pageNumber ? req.params.pageNumber : 1;
    const bookings = await bookingconsultationService.getAllBookConsultation(pageNumber);
    res.status(httpStatus.OK).send({
        serverResponse: {
            code: 200,
            message: 'Success',
        },
        result: {
            bookings,
        },
    });
});



// Colourist Booking
const getColouristAllBooking = catchAsync(async(req, res) => {
    const pageNumber = ms.pageNumber ? req.params.pageNumber : 1;
    const colouristId = req.user.id;
    const bookings = await bookingconsultationService.getAllColouristBookConsultation(pageNumber, colouristId);
    res.status(httpStatus.OK).send({
        serverResponse: {
            code: 200,
            message: 'Success',
        },
        result: {
            bookings,
        },
    });
});
// Booking Consultation Details
const bookingconsultationdetailsAdmin = catchAsync(async(req, res) => {
    const result = await bookingconsultationService.bookConsultationDetails(req.params.id);
    res.status(httpStatus.OK).send({
        serverResponse: {
            code: 200,
            message: 'Success',
        },
        result: {
            bookingconsultationdetails: result,
        },
    });
});
// Update Booking Consultation Admin
const bookingconsultationupdateAdmin = catchAsync(async(req, res) => {
    await bookingconsultationService.bookConsultationUpdate(req.params.id, req.body);
    res.status(httpStatus.OK).send({
        serverResponse: {
            code: 200,
            message: 'Consultation booking is updated successfully',
        },
    });
});
// Booking Consultation Payment
const consultationpayment = catchAsync(async(req, res) => {
    const token = req.body.tokenid;
    const save_card = req.body.save_card;
    const user = req.user;
    let customerId;
    let amountData = {
        amount: 15 * 100,
        description: 'Booking Consultation',
        currency: 'USD', 
    };
    if (!user.stripe_id) {
        let customerData = {
            fname : user.fname,
            lname : user.lname,
            email : user.email,
        };
        customerId =  await stripeService.createCustomerStripe(customerData);
        user.stripe_id = customerId;
        user.save();
    }else{
        customerId = user.stripe_id;
    }
    const tokenType = token.split('_');
    if (tokenType[0] === 'tok') {
        const card = await stripeService.createCard(token,user.stripe_id);
        amountData['source']=card.id;
    } else{
        amountData['source']=token;
    } 
    amountData['customer']=customerId;
    const stripeCharge =  await stripeService.create_charge(amountData);
    const transObj = {
        userId: user.id,
        amount: stripeCharge.amount / 100,
        note: stripeCharge.description,
        type: 'Booking',
        status: stripeCharge.status,
    };
    const transresult = await transactionService.transactionAdd(transObj);
    const bookingObj = req.body.bookingData;
    const newbookingObj = {...bookingObj, userId: user.id, transaction_id: transresult._id, bookingstatus: 'Booked' };
    await bookingconsultationService.bookConsultationAdd(newbookingObj);

    // booking consultation Mail //
    const welcomeMailDetails = {
        to: req.body.email,
        from: config.email.from,
        templateId: 'd-e47f0321cf1a442b988874647062f6e8', // Welcome Mail Template Id //
      };
      await mailer.sendEmail(welcomeMailDetails);
      // booking consultation Mail //


    res.status(httpStatus.OK).send({
        serverResponse: {
            code: 200,
            message: 'Your payment is successfull!',
        },
        stripeCharge,
        transObj,
        newbookingObj,
    }); 
});
// Add Booking Slots
const addBookingSlots = catchAsync(async(req, res) => {
    let newBody = [];
    req.body['availability'].map(x => {
        if (!x.status) {
            delete x.timeslots;
            x['timeslots'] = [];
        }
        newBody.push(x);
    });
    let paylod = {
        userId: req.body['userId'],
        availability: newBody
    }
    const result = await bookingconsultationService.addBookingSlotData(paylod);
    res.status(httpStatus.OK).send({
        serverResponse: {
            code: 200,
            message: "Booking Slot Update Successfully",
        },
        result: {
            bookingSlots: result.results,
        },
    });
});

// List Booking Slots - Admin
const getBookingSlots = catchAsync(async(req, res) => {
    const bookingSlots = await bookingconsultationService.getBookingSlots(req.params.userId);
    res.status(httpStatus.OK).send({
        serverResponse: {
            code: 200,
            message: 'Success',
        },
        bookingSlots,
    });
});


// List Booking Slots - colourist
const getColouristBookingSlots = catchAsync(async(req, res) => {
    const bookingSlots = await bookingconsultationService.getBookingSlots(req.user.id);
    res.status(httpStatus.OK).send({
        serverResponse: {
            code: 200,
            message: 'Success',
        },
        bookingSlots,
    });
});

// Delete Availability Slots
const deleteAvailabilitySlots = catchAsync(async(req, res) => {
    await bookingconsultationService.availabilityslotsDelete(req.params.id);
    res.status(httpStatus.OK).send({
        serverResponse: {
            code: 200,
            message: 'Availability slots is deleted successfully',
        },
    });
});

function convertTZ(date, tzString) {
    return new Date((typeof date === 'string' ? new Date(date) : date).toLocaleString('en-US', { timeZone: tzString }));
}

function sortby(array, cfg) {
    if (!(array instanceof Array && array.length)) return [];
    if (toString.call(cfg) !== '[object Object]') cfg = {};
    if (typeof cfg.parser !== 'function') cfg.parser = parse;
    cfg.desc = cfg.desc ? -1 : 1;
    return array.sort(function(a, b) {
        a = getItem.call(cfg, a);
        b = getItem.call(cfg, b);
        return cfg.desc * (a < b ? -1 : +(a > b));
    });
}
// List Booking Slots - Frontend
const getBookingSlotsDate = catchAsync(async(req, res) => {
    const { timeZone } = req.body;
    const bookingSlotsDateWithIds = await bookingconsultationService.getBookingSlotsDate();
    console.log(Intl.DateTimeFormat().resolvedOptions().timeZone);
    const conss = [];
    bookingSlotsDateWithIds.forEach((value) => {
        const { buffer_time } = value;

        value.available_time.forEach((val) => {
            //  val.push({ buffer_time: buffer_time })
            conss.push({...val, buffer_time });
        });
    });

    const skackc = conss.map((x) => {
        const time = new Date(x.datetime).toLocaleString('en-US', {
            timeZone,
            year: 'numeric',
            month: '2-digit',
            day: '2-digit',
            hour: '2-digit',
            minute: '2-digit',
        });

        const sp = time.split(',');

        return { datetime: sp[0], time: sp[1].trim(), sort: time, buffer_time: x.buffer_time };
    });
    const finalObj = {};
    skackc.forEach((games) => {
        const date = games.datetime;
        if (finalObj[date]) {
            finalObj[date].push(games);
        } else {
            finalObj[date] = [games];
        }
    });
    Object.keys(finalObj).forEach((val) => {
        console.log(val);
        finalObj[val].sort(function compare(a, b) {
            const dateA = new Date(a.sort);
            const dateB = new Date(b.sort);
            return dateA - dateB;
        });
    });

    res.status(httpStatus.OK).send({
        serverResponse: {
            code: 200,
            message: 'Success',
        },
        finalObj,
    });
});

// Available Days For Booking
/* const getAvailabilityDays = catchAsync(async (req, res) => {
  const bookingSlotsDetails = await bookingconsultationService.getAvailabilityDays();
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: 200,
      message: 'Success',
    },
    bookingSlotsDetails : bookingSlotsDetails[0].result
  });
}); */
const getDateByDay = function(d) {
    let now = new Date();
    now.setDate(now.getDate() - now.getDay() + d);
    return now;
}

const getAvailabilityDays = catchAsync(async(req, res) => {
    const { timeZone } = req.body;
    let timeslots1 = [];
    const bookingSlotsDetails = await bookingconsultationService.getAvailabilityDays();
    if (bookingSlotsDetails[0].result.length) {
        //const data = bookingSlotsDetails[0].result;



        let bufferTime = "";
        let starttime, interval, endtime, startHourInMinute, endHourInMinute, timeSlotsNew, obj;
        // need to change the time slot by timezone
        bookingSlotsDetails[0].result.forEach(value1 => {

            let d_t = getDateByDay(value1['daynum']);
            value1['user'].forEach(value => {
                bufferTime = value['buffertime'];
                value['timeslots'].forEach(value1 => {

                    let year = d_t.getFullYear();
                    let month = ("0" + (d_t.getMonth() + 1)).slice(-2);
                    let day = ("0" + d_t.getDate()).slice(-2);
                    starttime = value1['starttime'].toString();
                    starttime = starttime.slice(0, -2);
                    interval = (bufferTime != '') ? bufferTime + value1['slot'] : value1['slot'];
                    //interval =40;

                    endtime = value1['endtime'].toString();
                    endtime = endtime.slice(0, -2);
                    let ausstartdate = year + '-' + month + '-' + day + ' ' + ("0" + (starttime)).slice(-2) + ':00 +10';
                    let ausenddate = year + '-' + month + '-' + day + ' ' + ("0" + (endtime)).slice(-2) + ':00 +10';
                    let timeZoneStartDate = new Date(ausstartdate).toLocaleString('en-US', {
                        timeZone
                    });
                    let timeZoneStartDateNew = new Date(timeZoneStartDate);
                    let startDayNum = timeZoneStartDateNew.getDay();
                    let timeZoneEndDate = new Date(ausenddate).toLocaleString('en-US', {
                        timeZone
                    });
                    let timeZoneEndDateNew = new Date(timeZoneEndDate);
                    let endDayNum = timeZoneEndDateNew.getDay();
                    if (startDayNum == endDayNum) {
                        startHourInMinute = timeZoneStartDateNew.getHours() * 60 + timeZoneStartDateNew.getMinutes();
                        endHourInMinute = timeZoneEndDateNew.getHours() * 60 + timeZoneEndDateNew.getMinutes();
                        timeSlotsNew = timeslots(startHourInMinute, endHourInMinute, interval);
                        obj = { 'dayNum': startDayNum, 'userId': value['userId'], 'image': value['image'], 'title': value['title'], 'timeslots': timeSlotsNew };
                        timeslots1.push(obj);
                    } else if (startDayNum > endDayNum) {
                        startHourInMinute = timeZoneStartDateNew.getHours() * 60 + timeZoneStartDateNew.getMinutes();
                        endHourInMinute = 24 * 60;
                        timeSlotsNew = timeslots(startHourInMinute, endHourInMinute, interval);
                        obj = { 'dayNum': startDayNum, 'userId': value['userId'], 'image': value['image'], 'title': value['title'], 'timeslots': timeSlotsNew };
                        timeslots1.push(obj);
                        let lastTime = timeSlotsNew[timeSlotsNew.length - 1];
                        let [hours, minutes] = lastTime.split(':');
                        startHourInMinute = 23 * 60 + parseInt(minutes) + interval - 24 * 60;
                        endHourInMinute = timeZoneEndDateNew.getHours() * 60 + timeZoneEndDateNew.getMinutes();
                        timeSlotsNew = timeslots(startHourInMinute, endHourInMinute, interval);
                        obj = { 'dayNum': endDayNum, 'userId': value['userId'], 'image': value['image'], 'title': value['title'], 'timeslots': timeSlotsNew };
                        timeslots1.push(obj);
                    }
                });
            });

        });
    }
    let groups = ['dayNum', 'userId'];
    let helper = {};
    let result = [];
    if (timeslots1.length) {
        result = timeslots1.reduce(function(r, o) {
            let key = o.dayNum + '-' + o.userId;

            if (!helper[key]) {
                helper[key] = Object.assign({}, o); // create a copy of o
                let found = r.find(p => (p.dayNum === o.dayNum));
                if (found) {
                    found.user = [helper[key]].concat(found.user);
                } else {
                    r.push({ 'dayNum': o.dayNum, 'user': helper[key] });
                }

            } else {
                helper[key].timeslots = helper[key].timeslots.concat(o.timeslots);
            }
            return r;
        }, []);
    }
    //console.log(bookingDataDetail);
    res.status(httpStatus.OK).send({
        serverResponse: {
            code: 200,
            message: 'Success',
        },
        bookingSlotsDetails: result
    });
});

const timeslots = (startHourInMinute, endHourInMinute, interval) => {
        const timeslots1 = [];
        for (let i = 0; startHourInMinute < 24 * 60; i++) {
            let hour_format = "AM";
            if (startHourInMinute > endHourInMinute) break;
            let hh = Math.floor(startHourInMinute / 60); // getting hours of day in 0-24 format
            if (hh >= 12) {
                hour_format = "PM";
            }
            if (hh > 12) {
                hh = Math.floor(hh % 12);
            }
            let mm = startHourInMinute % 60; // getting minutes of the hour in 0-55 format;
            //console.log(('0' + (hh % 24)).slice(-2) + ':' + ('0' + mm).slice(-2)+' '+hour_format);
            timeslots1.push(('0' + (hh % 24)).slice(-2) + ':' + ('0' + mm).slice(-2) + ' ' + hour_format)

            startHourInMinute = startHourInMinute + interval;
        }
        return timeslots1;
    }
    // Booking Slots Details - Frontend
const getBookingSlotsDetails = catchAsync(async(req, res) => {
    const bookingSlotsDetails = await bookingconsultationService.getBookingSlotsByDate(req.body.date);
    const newarray = bookingSlotsDetails[0].available_time;
    newarray.sort((a, b) => (a > b ? 1 : a < b ? -1 : 0));
    res.status(httpStatus.OK).send({
        serverResponse: {
            code: 200,
            message: 'Success',
        },
        bookingSlotsDetails: bookingSlotsDetails[0],
        newarray,
    });
});


/* Time Slot  BY colourist Frontend */
const getAvailableTimeSlot = catchAsync(async(req, res) => {
    const { timeZone, colouristId, dayNum, selectedDate } = req.body;
    const bookingSlotsDetails = await bookingconsultationService.getAvailabilityTimeColourist(colouristId, dayNum);
    let timeslots1 = [];
    let bufferTime = "";
    let starttime, interval, endtime, startHourInMinute, endHourInMinute;
    // need to change the time slot by timezone
    bookingSlotsDetails[0]['availability'].forEach(value => {
        bufferTime = value['buffertime'];
        value['timeslots'].forEach(value1 => {
            starttime = value1['starttime'].toString();
            starttime = starttime.slice(0, -2);
            interval = bufferTime + value1['slot'];
            endtime = value1['endtime'].toString();
            endtime = endtime.slice(0, -2);
            let ausstartdate = selectedDate + ' ' + ("0" + (starttime)).slice(-2) + ':00 +10';
            let ausenddate = selectedDate + ' ' + ("0" + (endtime)).slice(-2) + ':00 +10';
            let timeZoneStartDate = new Date(ausstartdate).toLocaleString('en-US', {
                timeZone,
                hour: '2-digit',
                minute: '2-digit',
                hour12: false
            });
            let timeZoneEndDate = new Date(ausenddate).toLocaleString('en-US', {
                timeZone,
                hour: '2-digit',
                minute: '2-digit',
                hour12: false
            });
            let spStart = timeZoneStartDate.split(':');
            let spEnd = timeZoneEndDate.split(':');
            startHourInMinute = spStart[0] * 60 + parseInt(spStart[1]);
            endHourInMinute = spEnd[0] * 60 + parseInt(spEnd[1]);
            for (let i = 0; startHourInMinute < 24 * 60; i++) {
                let hour_format = "AM";
                if (startHourInMinute >= endHourInMinute) break;
                let hh = Math.floor(startHourInMinute / 60); // getting hours of day in 0-24 format

                if (hh >= 12) {
                    hour_format = "PM";
                }
                if (hh > 12) {
                    hh = Math.floor(hh % 12);
                }
                let mm = startHourInMinute % 60; // getting minutes of the hour in 0-55 format
                timeslots1.push(('0' + (hh % 24)).slice(-2) + ':' + ('0' + mm).slice(-2) + ' ' + hour_format)

                startHourInMinute = startHourInMinute + interval;
            }
        });
    });
    res.status(httpStatus.OK).send({
        serverResponse: {
            code: 200,
            message: 'Success',
        },
        bookingSlotsDetails: timeslots1,
    });
});

module.exports = {
    bookingconsultationadd,
    bookingconsultationdelete,
    bookingconsultationlist,
    bookingconsultationupdate,
    bookingconsultationdetails,
    bookingconsultationstatuschange,
    getAllBooking,
    bookingconsultationdetailsAdmin,
    bookingconsultationupdateAdmin,
    consultationpayment,
    bookingconsultationdeleteAdmin,
    addBookingSlots,
    getBookingSlots,
    deleteAvailabilitySlots,
    getBookingSlotsDate,
    getBookingSlotsDetails,
    getColouristBookingSlots,
    getAvailabilityDays,
    getAvailableTimeSlot,
    getColouristAllBooking

};
