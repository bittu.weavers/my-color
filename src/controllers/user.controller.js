const httpStatus = require('http-status');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { userService } = require('../services');

// Add User
const addUser = catchAsync(async (req, res) => {
  const user = await userService.createUser(req.body);

  res.status(httpStatus.OK).send({
    serverResponse: {
      code: httpStatus.OK,
      message: 'User Add Successfully',
    },
    result: {
      userData: user,
    },
  });
});

// Get User Detail
const getUserDetail = catchAsync(async (req, res) => {
  const user = await userService.getUserById(req.body.userId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: httpStatus.OK,
      message: 'Success',
    },
    result: {
      userData: user,
    },
  });
});
// Edit User
const editUser = catchAsync(async (req, res) => {
  const user = await userService.getUserById(req.params.userId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: httpStatus.OK,
      message: 'Success',
    },
    result: {
      userData: user,
    },
  });
});
// Update Admin User
const updateUser = catchAsync(async (req, res) => {
  const user = await userService.updateUserById(req.user.id, req.body);
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: httpStatus.OK,
      message: 'Your profile has been updated successfully.',
    },
    result: {
      userData: user,
    },
  });
});
// Update User
const updateAdminUser = catchAsync(async (req, res) => {
  const user = await userService.updateUserById(req.params.userId, req.body);
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: httpStatus.OK,
      message: 'User Data update successfullly',
    },
    result: {
      userData: user,
    },
  });
});

// Change Password
const changePassword = catchAsync(async (req, res) => {
  await userService.changePassword(req.body.email, req.body.oldPassword, req.body.newPassword);
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: httpStatus.OK,
      message: 'Your password has been changed successfully.',
    },
  });
});

// Delete Social User
const deleteSocialUser = catchAsync(async (req, res) => {
  const results = await userService.deleteSocialUserAccount(req.body.socialId, req.body.usertype);
  if (results) {
    res.status(httpStatus.OK).send({
      serverResponse: {
        code: httpStatus.OK,
        message: 'Your Social account deleted successfully',
      },
    });
  }
});
/**
 * Get users
 */
const getUserList = catchAsync(async (req, res) => {
  const current_page = req.params.current_page ? req.params.current_page : 1;
  const userList = await userService.getAllUsersList(current_page, req.body);
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: httpStatus.OK,
      message: 'Successrwerewr',
    },
    userList,
  });
});

/* Delete User */

const deleteUser = catchAsync(async (req, res) => {
  const results = await userService.deleteUserById(req.params.userId);
  if (results) {
    res.status(httpStatus.OK).send({
      serverResponse: {
        code: httpStatus.OK,
        message: 'Your user account deleted successfully',
      },
    });
  }
});

/* Edit Profile */
const editProfile = catchAsync(async (req, res) => {
  const user = await userService.getUserById(req.user.id);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: httpStatus.OK,
      message: 'Success',
    },
    result: {
      userData: user,
    },
  });
});
/* Update Profile */
const updateProfile = catchAsync(async (req, res) => {
  const user = await userService.updateUserById(req.user.id, req.body);
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: httpStatus.OK,
      message: 'User Data update successfullly',
    },
    result: {
      userData: user,
    },
  });
});

module.exports = {
  getUserDetail,
  updateUser,
  changePassword,
  deleteSocialUser,
  addUser,
  getUserList,
  editUser,
  updateAdminUser,
  deleteUser,
  editProfile,
  updateProfile,
};
