const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const { productService } = require('../services');

// Add product
const addProduct = catchAsync(async(req, res) => {
    //console.log(JSON.stringify(req.body));
    const product = await productService.addProductData(req.body);
    res.status(httpStatus.OK).send({
        serverResponse: {
            code: 200,
            message: 'Product Added Successfully',
        },
        product,
    });
});

// Update Product

const updateProduct = catchAsync(async(req, res) => {
    const results = await productService.updateProductById(req.params.id, req.body);
    if (results) {
        res.status(httpStatus.OK).send({
            serverResponse: {
                code: httpStatus.OK,
                message: 'Product updated successfully.',
            },
            result: results,
        });
    }
});

// Add product Review
const addReview = catchAsync(async(req, res) => {
  let checkStatus=await productService.checkUserCanSubmitReview(req);
    console.log(checkStatus)
  if(!checkStatus){
    const product = await productService.addReviewData(req);
    res.status(httpStatus.OK).send({
        serverResponse: {
            code: 200,
            message: 'Review Added Successfully. Your review will be publish after admin approval',
        },
        product,
    });
  }else{
    res.status(httpStatus.OK).send({
      serverResponse: {
          code: 202,
          message: 'You have already added a review for this product',
      }
  });
  }
});

// Product List
const getProductList = catchAsync(async(req, res) => {
    // eslint-disable-next-line camelcase
    const current_page = req.params.current_page ? req.params.current_page : 1;
    const productList = await productService.getProductListData(current_page);
    res.status(httpStatus.OK).send({
        serverResponse: {
            code: 200,
            message: 'Success',
        },
        productListData: productList,
    });
});

// Product Review
const getProductReviewById = catchAsync(async(req, res) => {
    // eslint-disable-next-line camelcase
    const current_page = req.params.current_page ? req.params.current_page : 1;
    // eslint-disable-next-line camelcase
    const p_id = req.params.pid;
    const reviewList = await productService.getReviewListData(p_id, current_page);
    res.status(httpStatus.OK).send({
        serverResponse: {
            code: 200,
            message: 'Success',
        },
        reviewListData: reviewList,
    });
});

// Product Review Details
const getProductReviewDetails = catchAsync(async(req, res) => {
    // eslint-disable-next-line camelcase
    const r_id = req.params.rid;
    const reviewDetails = await productService.getReviewDetailsData(r_id);
    res.status(httpStatus.OK).send({
        serverResponse: {
            code: 200,
            message: 'Success',
        },
        reviewDetailsData: reviewDetails,
    });
});

// All Product List
const getAllProductList = catchAsync(async(req, res) => {
    // eslint-disable-next-line camelcase
    const productList = await productService.getAllProductListData();
    res.status(httpStatus.OK).send({
        serverResponse: {
            code: 200,
            message: 'Success',
        },
        productListData: productList,
    });
});

// Product Delete
const deleteProduct = catchAsync(async(req, res) => {
    const id = req.params.id ? req.params.id : null;
    await productService.deleteProduct(id);
    res.status(httpStatus.OK).send({
        serverResponse: {
            code: 200,
            message: 'Product Deleted Successfully',
        },
    });
});

// Product Review Delete
const deleteReview = catchAsync(async(req, res) => {
    const id = req.params.id ? req.params.id : null;
    await productService.deleteReview(id);
    res.status(httpStatus.OK).send({
        serverResponse: {
            code: 200,
            message: 'Product Review Deleted Successfully',
        },
    });
});

// Product Edit

const editProduct = catchAsync(async(req, res) => {
    const { id } = req.params;
    const product = await productService.getProductById(id);
    res.status(httpStatus.OK).send({
        serverResponse: {
            code: 200,
            message: 'Product Fetch Successfully',
        },
        result: {
            product,
        },
    });
});
// Product filter Data
const productFilter = catchAsync(async(req, res) => {
    // eslint-disable-next-line camelcase
    const current_page = req.params.current_page ? req.params.current_page : 1;
    const productList = await productService.getProductFilterData(current_page, req);
    res.status(httpStatus.OK).send({
        serverResponse: {
            code: 200,
            message: 'Success',
        },
        productListData: productList,
    });
});

const productFilterlist = catchAsync(async(req, res) => {
    const filterlist = await productService.productFilterlist();
    res.status(httpStatus.OK).send({
        serverResponse: {
            code: 200,
            message: 'Success',
        },
        filterlist,
    });
});

// Product review status update
const reviewStatusUpdate = catchAsync(async(req, res) => {
    await productService.reviewStatusUpdate(req.body.id, req.body);
    res.status(httpStatus.OK).send({
        serverResponse: {
            code: 200,
            message: 'Review Status Updated Successfully',
        },
    });
});
const getproductByslug = catchAsync(async(req, res) => {
    const product = await productService.findProductBySlug(req.params.slug,req);

    res.status(httpStatus.OK).send({
        serverResponse: {
            code: 200,
            message: 'Product fetch Successfully',
        },
        product,
    });
});
// Product Review
const getProductReviewByIduser = catchAsync(async(req, res) => {
    // eslint-disable-next-line camelcase
    const current_page = req.params.current_page ? req.params.current_page : 1;
    // eslint-disable-next-line camelcase
    const p_id = req.params.pid;
    const reviewList = await productService.getReviewListDataUser(p_id, current_page);
    res.status(httpStatus.OK).send({
        serverResponse: {
            code: 200,
            message: 'Success',
        },
        reviewListData: reviewList,
    });
});
//ProductList For Header Search
const getProductListForHeader = catchAsync(async (req, res) => {
    const productList = await productService.productListForHeaderSearch(req, res);
    res.status(httpStatus.OK).send({
        serverResponse: {
            code: 200,
            message: 'Success',
        },
        productListData: productList,
    });
})

module.exports = {
    addProduct,
    getProductList,
    deleteProduct,
    editProduct,
    getProductReviewById,
    addReview,
    getProductReviewDetails,
    reviewStatusUpdate,
    deleteReview,
    productFilter,
    productFilterlist,
    getproductByslug,
    getProductReviewByIduser,
    getAllProductList,
    updateProduct,
    getProductListForHeader
};
