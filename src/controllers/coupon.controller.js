const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const { couponService } = require('../services');
// Add Coupon
const addCouponCode = catchAsync(async (req, res) => {
  await couponService.addCouponCodeData(req.body);
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: httpStatus.OK,
      message: 'Coupon Code added successfully',
    },
  });
});
// Delete Coupon
const deleteCoupon = catchAsync(async (req, res) => {
  await couponService.deleteCouponCode(req.params.id);
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: httpStatus.OK,
      message: 'Coupon Code deleted successfully',
    },
  });
});
// Get Coupon List
const getCouponList = catchAsync(async (req, res) => {
  const current_page = req.params.current_page ? req.params.current_page : 1;
  let pagesize = req.params.pagesize ? req.params.pagesize : 10;
  pagesize = pagesize*1;
  const couponList = await couponService.getAllCouponList(current_page,pagesize);
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: httpStatus.OK,
      message: 'Success',
    },
    couponListData : couponList,
  });
});
// Edit Coupon
const editCoupon = catchAsync(async (req, res) => {
  const results = await couponService.getCouponCodeById(req.params.id);
  if (results) {
    res.status(httpStatus.OK).send({
      serverResponse: {
        code: httpStatus.OK,
        message: 'Success',
      },
      result: results,
    });
  }
});
// Update Coupon
const updateCoupon = catchAsync(async (req, res) => {
  const results = await couponService.updateCouponById(req.params.id, req.body);
  if (results) {
    res.status(httpStatus.OK).send({
      serverResponse: {
        code: httpStatus.OK,
        message: 'Coupon updated successfully.',
      },
      result: results,
    });
  }
});
const generate_Coupon_code = catchAsync(async (req, res) => {
  const couponCode = await generateCode(6);
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: httpStatus.OK,
      message: 'Coupon generate successfully.',
    },
    couponCode: couponCode,
  });
  
});
const generateCode= async(length) => {
  let text = "";
  const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
  for ( var i=0; i < length; i++ ) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  const coupon_data = await couponService.getCouponByCode(text);
  
  if(coupon_data !=null && coupon_data.length){
    
    generateCode(length);
  }
  console.log(text);
  return text;
}
module.exports = {
  addCouponCode,
  deleteCoupon,
  getCouponList,
  editCoupon,
  updateCoupon,
  generate_Coupon_code
};
