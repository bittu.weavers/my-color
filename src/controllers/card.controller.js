const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const { cardService,userService,stripeService } = require('../services');

// Card add
const addCard = catchAsync(async (req, res) => {
  if(!req.user.stripe_id){
    let customerData = {
      fname : req.user.fname,
      lname : req.user.lname,
      email : req.user.email,
  };
  customerId =  await stripeService.createCustomerStripe(customerData);
  req.user.stripe_id = customerId;
  req.user.save();
  }
  const cardDetail = await cardService.createCard(req.body.token,req.user.stripe_id);
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: httpStatus.OK,
      message: 'Card added successfully',
    },
    cardDetail : cardDetail,
  });
});

// Card List
const getCardList = catchAsync(async (req, res) => {
    const cardlist = await cardService.getCardListData(req.user.stripe_id);
    res.status(httpStatus.OK).send({
        serverResponse: {
          code: httpStatus.OK,
          message: 'Success',
        },
        cardlist : cardlist,
    });
});

// Delete Card
const deleteCard = catchAsync(async (req, res) => {
    await cardService.deleteCardData(req.params.cardId,req.user.stripe_id);
    res.status(httpStatus.OK).send({
        serverResponse: {
          code: httpStatus.OK,
          message: 'Card delete successfully',
        },
    });
});

//Default Card 
const defaultCard = catchAsync(async (req, res) => {
    await cardService.defaultCardData(req.params.cardId,req.user.stripe_id);
    res.status(httpStatus.OK).send({
        serverResponse: {
          code: httpStatus.OK,
          message: 'This is now selected as your default card',
        },
    });
});


module.exports = {
    addCard,
    getCardList,
    deleteCard,
    defaultCard
};
  