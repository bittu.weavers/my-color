const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const taxService = require('../services/tax.service');
const mailer = require('../utils/mailer');
const config = require('../config/config');

const addSetting = catchAsync(async (req, res) => {
    const data = req.body
    await taxService.taxupdate(data)
    res.status(httpStatus.OK).send({
        serverResponse: {
            code: 200,
            message: 'Tax Updated Successfully',
        },
    });
});

const getAlltax = catchAsync(async (req, res) => {
    const alltax = await taxService.getTaxes()
    res.status(httpStatus.OK).send({
        serverResponse: {
            code: 200,
            message: 'Tax Updated Successfully',
        },
        alltax
    });
});


const addNewTax = catchAsync(async (req, res) => {
    const data = req.body
    await taxService.addTax(data)
    res.status(httpStatus.OK).send({
        serverResponse: {
            code: 200,
            message: 'Tax Updated Successfully',
        },
    });
});


const getTax = catchAsync(async (req, res) => {
    const data = req.params.id
    const tax = await taxService.getTaxbyId(data)
    res.status(httpStatus.OK).send({
        serverResponse: {
            code: 200,
            message: 'Tax Updated Successfully',
        },
        tax
    });
});



const deleteTax = catchAsync(async (req, res) => {
    const data = req.params.id
    const tax = await taxService.removeTaxById(data)
    res.status(httpStatus.OK).send({
        serverResponse: {
            code: 200,
            message: 'Tax Deleted',
        }
    });
});


const filterTax = catchAsync(async (req, res) => {
    const tax = await taxService.getTaxWithFilter(req.body.country,req.body.state)
    res.status(httpStatus.OK).send({
        serverResponse: {
            code: 200,
            message: 'Tax Data',
        },
        tax
    });
});



module.exports = {
    addSetting,
    getAlltax,
    addNewTax,
    getTax,
    deleteTax,
    filterTax
};
