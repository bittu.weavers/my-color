const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');

const { productreviewService } = require('../services');

// Add Product Review
const productReviewAdd = catchAsync(async (req, res) => {
  await productreviewService.addProductreview(req.body);
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: httpStatus.OK,
      message: 'Product review submitted successfully',
    },
  });
});

// Change Product Review Status
const productreviewstatus = catchAsync(async (req, res) => {
  const updatereviewdata = await productreviewService.changeproductreviewstatus(req.body.id, req.body);
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: httpStatus.OK,
      message: 'Product review status chnaged successfully',
    },
    result: {
      UpdatedReviewData: updatereviewdata,
    },
  });
});

// Delete Product Review
const deleteproductreview = catchAsync(async (req, res) => {
  await productreviewService.deleteproductsreview(req.body.id);
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: httpStatus.OK,
      message: 'Product review deleted successfully',
    },
  });
});

module.exports = {
  productReviewAdd,
  productreviewstatus,
  deleteproductreview,
};
