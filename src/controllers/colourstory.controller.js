const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const colourservice = require("../services/colourstories.service");

const addStory = catchAsync(async (req,res) => {

  const addData = await colourservice.addStoryDb(req.body)

  res.status(httpStatus.OK).send({
    serverResponse: {
      code: httpStatus.OK,
      message: 'Added Successfully!',
    },
    addData
  });
})

const showList = catchAsync(async (req,res) => {
  const current_page = req.params.current_page ? req.params.current_page : 1;
  const list = await colourservice.getAllData(current_page)
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: httpStatus.OK,
      message: 'All Stories',
    },
    list : list
  });
})



const getStory = catchAsync(async (req,res) => {

  const story = await colourservice.getStoryData(req.params.id)

  res.status(httpStatus.OK).send({
    serverResponse: {
      code: httpStatus.OK,
      message: 'Story Data',
    },
    story
  });
})


const updateStory = catchAsync(async (req,res) => {

  await colourservice.updateStory(req.body)

  res.status(httpStatus.OK).send({
    serverResponse: {
      code: httpStatus.OK,
      message: 'Story Data Updated',
    },
  });
})


const deleteStory = catchAsync(async (req,res) => {

  await colourservice.deletethestory(req.params.id)

  res.status(httpStatus.OK).send({
    serverResponse: {
      code: httpStatus.OK,
      message: 'Story Data Deleted',
    },
  });
})

const storyList = catchAsync(async (req,res) => {
  const list = await colourservice.storyList()
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: httpStatus.OK,
      message: 'All Stories',
    },
    list
  });
})

module.exports = {
  addStory,
  showList,
  getStory,
  updateStory,
  deleteStory,
  storyList
};
