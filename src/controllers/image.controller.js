const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const uploadService = require('../services/image.service');

const uploadImage = catchAsync(async (req, res) => {
  const image = await uploadService.uploadImage(req);
  res.status(httpStatus.CREATED).send({
    serverResponce: {
      code: 200,
      message: 'Image Upload Successfully',
    },
    result: {
      image,
    },
  });
});
const getImages = catchAsync(async (req, res) => {
  const currentPage = req.body.currentPage || 1;
  const images = await uploadService.getImages(currentPage);
  res.status(httpStatus.OK).send({
    serverResponce: {
      code: 200,
      message: 'Images Fetch Successfully',
    },
    images,
  });
});
const deleteImage = catchAsync(async (req, res) => {
  const path = req.body.path ? req.body.path : null;
  await uploadService.deleteimage(path, req.body);
  res.status(httpStatus.OK).send({
    serverResponce: {
      code: 200,
      message: 'Image Deleted',
    },
  });
});
module.exports = {
  uploadImage,
  getImages,
  deleteImage,
};
