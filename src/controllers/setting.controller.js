const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const { settingService } = require('../services');

// Add Setting
const addSetting = catchAsync(async (req, res) => {
  await settingService.addSettingData(req.body);
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: 200,
      message: 'Settings Added Successfully',
    },
  });
});

module.exports = {
  addSetting,
};
