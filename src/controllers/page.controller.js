const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const { pageService } = require('../services');

const createPage = catchAsync(async (req, res) => {
  const token = req.headers.authorization ? req.headers.authorization.split(' ')[1] : '';
  const page = await pageService.createPage(req, token);
  res.status(httpStatus.CREATED).send({
    serverResponse: {
      code: 200,
      message: 'Page created Successfully',
    },
    result: {
      page,
    },
  });
});
const getPages = catchAsync(async (req, res) => {
  const currentPage = req.body.currentPage || 1;
  const pages = await pageService.getPages(currentPage);
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: 200,
      message: 'Pages Fetch Successfully',
    },
    pages,
  });
});
const getPageBySlug = catchAsync(async (req, res) => {
  const page = await pageService.findPageBySlug(req.params.slug);
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: 200,
      message: 'Page Fetch Successfully',
    },
    page
  });
});
const deletePage = catchAsync(async (req, res) => {
  const id = req.params.id ? req.params.id : null;
  await pageService.deletePage(id);
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: 200,
      message: 'Page Deleted',
    },
  });
});
const editPage = catchAsync(async (req, res) => {
  const { id } = req.params;
  const page = await pageService.editPage(id);
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: 200,
      message: 'Page Fetch Successfully',
    },
    result: {
      page,
    },
  });
});
const updatePage = catchAsync(async (req, res) => {
  const token = req.headers.authorization ? req.headers.authorization : '';
  const { id } = req.params;
  const page = await pageService.updatePage(id, token, req);
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: 200,
      message: 'Page Updated Successfully',
    },
    result: {
      page,
    },
  });
});
const getSectionById = catchAsync(async (req, res) => {
  const sectionId = req.params.id;
  const section = await pageService.findBySectionId(sectionId);
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: 200,
      message: 'Section Found',
    },
    result: {
      section,
    },
  });
});
const removeImageBySectionId = catchAsync(async (req, res) => {
  const sectionId = req.params.id;
  await pageService.removeImage(sectionId,req.body);
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: 200,
      message: 'Image Removed',
    }
  });
});
const removeImageByblockId = catchAsync(async (req, res) => {
  const sectionId = req.params.id;
  const pageId = req.params.pageId;
   await pageService.removeBlockImage(sectionId,pageId,req.body);
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: 200,
      message: 'Image Removed',
    }
  });
});
const removeBlock = catchAsync(async (req, res) => {
  const id = req.params.id;
  const pageId = req.params.pageId;
  await pageService.removeBlock(pageId,id,req.body);
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: 200,
      message: 'Block Removed',
    }
  });
});
module.exports = {
  createPage,
  getPages,
  getPageBySlug,
  deletePage,
  editPage,
  updatePage,
  getSectionById,
  removeImageBySectionId,
  removeBlock,
  removeImageByblockId
};
