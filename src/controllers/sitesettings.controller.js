const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const settingsService = require("../services/sitesettings.service")


const addSiteLogo = catchAsync(async (req, res) => {
  await settingsService.addupsitelogo(req.body.logo,req.body.id)


  res.status(httpStatus.OK).send({
    serverResponse: {
      code: httpStatus.OK,
      message: 'Site Header Logo Updated',
    },
  });

});


const addSiteFooterLogo = catchAsync(async (req, res) => {
  await settingsService.addupsitefooterlogo(req.body.logo,req.body.id)


  res.status(httpStatus.OK).send({
    serverResponse: {
      code: httpStatus.OK,
      message: 'Site Footer Logo Updated',
    },
  });

});



const addSiteSocial = catchAsync(async (req, res) => {
  await settingsService.addNewSocial(req.body.social,req.body.id)

  res.status(httpStatus.OK).send({
    serverResponse: {
      code: httpStatus.OK,
      message: 'Social Link Added',
    },
  });

});


const updateSocial = catchAsync(async (req, res) => {
  // console.log("body",req.body.id)
  await settingsService.updateSocialNow(req.body.social,req.body.id)

  res.status(httpStatus.OK).send({
    serverResponse: {
      code: httpStatus.OK,
      message: 'Updated data succesfully',
    },
  });

});

const getLogo= catchAsync(async(req,res) =>{
  const logo = await settingsService.getLogoData()

  res.status(httpStatus.OK).send({
    serverResponse: {
      code: httpStatus.OK,
      message: 'Logo Data',
    },
    logo
  });
})

const getSocial= catchAsync(async(req,res) =>{
  const objectId=req.params.objectId;
  const id=req.params.id;

    const social = await settingsService.getSocialData(id,objectId)
      res.status(httpStatus.OK).send({
        serverResponse: {
          code: httpStatus.OK,
          message: 'Social Data',
        },
        social:social[0]
      });
})





const setfooter= catchAsync(async(req,res) =>{
  console.log("bodyyyyyyyyyyyyyy",req.body);
    await settingsService.updateFooter(req.body)

      res.status(httpStatus.OK).send({
        serverResponse: {
          code: httpStatus.OK,
          message: 'Footer Updated',
        },
      });
})


const setheader= catchAsync(async(req,res) =>{
  await settingsService.updateHeader(req.body)

    res.status(httpStatus.OK).send({
      serverResponse: {
        code: httpStatus.OK,
        message: 'Header Updated',
      },
    });
})
const deleteSocial = catchAsync(async(req,res)=>{
await settingsService.deleteSocialData(req.params.objectId,req.params.id);
    res.status(httpStatus.OK).send({
      serverResponse: {
        code: httpStatus.OK,
        message: 'Social link deleted',
      },
    });
})
module.exports = {
  addSiteLogo,
  addSiteSocial,
  updateSocial,
  getLogo,
  getSocial,
  setfooter,
  setheader,
  deleteSocial,
  addSiteFooterLogo
};
