const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
// const { Response } = require('../utils/response');

const { productcategoryService } = require('../services');

// Add Product Category
const productcategoryAdd = catchAsync(async (req, res) => {
  await productcategoryService.addProductcategory(req.body);

  // return new Response('', httpStatus.OK , 'Product Category added successfully').view();
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: httpStatus.OK,
      message: 'Product Category added successfully',
    },
  });
});

// List Product Category
const getCategoryListsData = catchAsync(async (req, res) => {
  const current_page = req.params.current_page ? req.params.current_page : 1;
  const results = await productcategoryService.getProductCategory(current_page);
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: httpStatus.OK,
      message: 'Success',
    },
    result: results,
  });
});

// Get Product Category
const getCategoryList = catchAsync(async (req, res) => {
  const results = await productcategoryService.getProductCategoryList();
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: httpStatus.OK,
      message: 'Success',
    },
    result: results,
  });
});

// Delete Product Category
const deleteProductcategory = catchAsync(async (req, res) => {
  const results = await productcategoryService.deleteProductcategoryById(req.params.id);
  if (results) {
    res.status(httpStatus.OK).send({
      serverResponse: {
        code: httpStatus.OK,
        message: 'Product Category Deleted successfully.',
      },
    });
  }
});

// Edit Product Category
const editProductcategory = catchAsync(async (req, res) => {
  const results = await productcategoryService.getProductcategoryById(req.params.id);
  if (results) {
    res.status(httpStatus.OK).send({
      serverResponse: {
        code: httpStatus.OK,
        message: 'Success',
      },
      result: results,
    });
  }
});

// Update Product Category
const updateProductcategory = catchAsync(async (req, res) => {
  const results = await productcategoryService.updateProductcategoryById(req.params.id, req.body);
  if (results) {
    res.status(httpStatus.OK).send({
      serverResponse: {
        code: httpStatus.OK,
        message: 'Product Category updated successfully.',
      },
    });
  }
});

// Status Change Product Category

const statusProductcategory = catchAsync(async (req, res) => {
  const categorydata = await productcategoryService.updateProductCategoryStatus(req.params.id);
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: httpStatus.OK,
      message: 'Product Category status has been changed successfully',
    },
    result: {
      categoryData: categorydata,
    },
  });
});

module.exports = {
  productcategoryAdd,
  getCategoryListsData,
  deleteProductcategory,
  updateProductcategory,
  statusProductcategory,
  editProductcategory,
  getCategoryList,
};
