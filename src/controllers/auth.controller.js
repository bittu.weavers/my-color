const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const { authService, userService, tokenService, otpService } = require('../services');
const mailer = require('../utils/mailer');
const config = require('../config/config');

const register = catchAsync(async (req, res) => {
  const user = await userService.createUser(req.body);
  const tokens = await tokenService.generateAuthTokens(user);
  if (user) {
    // Welcome Mail //
    const welcomeMailDetails = {
      to: req.body.email,
      from: config.email.from,
      templateId: 'd-e47f0321cf1a442b988874647062f6e8', // Welcome Mail Template Id //
    };
    await mailer.sendEmail(welcomeMailDetails);
    // Welcome Mail //
    res.status(httpStatus.OK).send({
      serverResponse: {
        code: httpStatus.OK,
        message: 'You have been successfully registered.',
      },
      result: {
        userData: user,
        tokens: {
          accessToken: tokens.access.token,
          refreshToken: tokens.refresh.token,
        },
      },
    });
   
  }
});

const login = catchAsync(async (req, res) => {
  const { email, password } = req.body;
  const user = await authService.loginUserWithEmailAndPassword(email, password);
  const tokens = await tokenService.generateAuthTokens(user);
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: httpStatus.OK,
      message: 'You have successfully logged in',
    },
    result: {
      userData: user,
      tokens: {
        accessToken: tokens.access.token,
        refreshToken: tokens.refresh.token,
      },
    },
  });
});

const refreshToken = catchAsync(async (req, res) => {
  const tokens = await authService.refreshAuth(req.body.refreshToken);
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: httpStatus.OK,
      message: 'Success',
    },
    result: {
      tokens: {
        accessToken: tokens.access.token,
        refreshToken: tokens.refresh.token,
      },
    },
  });
});




const logout = catchAsync(async (req, res) => {
  await authService.logout(req.body.refreshToken);
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: httpStatus.OK,
      message: 'You have been successfully logged out!',
    },
  });
});

// FORGET PASSWORD OTP GENERATE //
const forgotPasswordOTP = catchAsync(async (req, res) => {
  console.log(req.body)
  const otp = await otpService.generateResetPasswordOTP(req.body.email);
  const otpMailDetails = {
    to: req.body.email,
    from: config.email.from,
    subject: 'Reset Password Verification OTP',
    templateId: 'd-c450fa812e8d4b9980dbc5df5c547068', // Reset Password Template Id //
    dynamic_template_data: {
      otp,
    },
  };
  await mailer.sendEmail(otpMailDetails);
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: httpStatus.OK,
      message: 'Verification code has been sent to your email ID',
      otpMesage: otp,
    },
  });
});

// USER OTP VERIFICATION //
const otpVerification = catchAsync(async (req, res) => {
  const { email, otp } = req.body;
  await otpService.checkOtpVerifiaction(email, otp);
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: httpStatus.OK,
      message: 'Success',
      isVerificationSuccess: true,
    },
  });
});

// RESET PASSWORD WITH OTP //
const resetPassword = catchAsync(async (req, res) => {
  const resetPass = await otpService.resetPassword(req.body.email, req.body.otp, req.body.password);
  const otpMailDetails = {
    to: req.body.email,
    from: config.email.from,
    subject: 'Your password has been reset successfully.',
    templateId: 'd-c450fa812e8d4b9980dbc5df5c547068', // Reset Password Template Id //
    dynamic_template_data: {
      otp,
    },
  };
  await mailer.sendEmail(otpMailDetails);
  if (resetPass) {
    res.status(httpStatus.OK).send({
      serverResponse: {
        code: httpStatus.OK,
        message: 'Your password has been reset successfully.',
      },
    });
  }
});

// SOCIAL LOGIN
const socialLogin = catchAsync(async (req, res) => {
  const user = await authService.loginUserWithSocialID(req.body);
  const tokens = await tokenService.generateAuthTokens(user);
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: httpStatus.OK,
      message: 'You have successfully logged in',
    },
    result: {
      userData: user,
      tokens: {
        accessToken: tokens.access.token,
        refreshToken: tokens.refresh.token,
      },
    },
  });
});

module.exports = {
  register,
  login,
  logout,
  forgotPasswordOTP,
  otpVerification,
  resetPassword,
  socialLogin,
  refreshToken,
};
