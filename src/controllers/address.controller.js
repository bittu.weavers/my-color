const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const { addressService } = require('../services');

const getAddressList = catchAsync(async (req, res) => {
  const addressList = await addressService.getAddressByUserId(req.body.userId);
  res.status(httpStatus.OK).send({
    addressData: addressList,
  });
});

const addAddress = catchAsync(async (req, res) => {
  await addressService.createAddress(req.body.userId, req.body);
  res.status(httpStatus.OK).send({
    result: {
      code: 200,
      message: 'Address added successfully',
    },
  });
});

const addressDetail = catchAsync(async (req, res) => {
  const addressListDetail = await addressService.getAddressDetailByUserId(req.body.userId, req.body.addressId);
  res.status(httpStatus.OK).send({
    addressData: addressListDetail,
  });
});

const updateAddress = catchAsync(async (req, res) => {
  await addressService.updateAddressById(req.body.userId, req.body);
  res.status(httpStatus.OK).send({
    result: {
      code: 200,
      message: 'Address updated successfully',
    },
  });
});

const deleteAddress = catchAsync(async (req, res) => {
  await addressService.deleteAddressById(req.body.userId, req.body.addressId);
  res.status(httpStatus.OK).send({
    result: {
      code: 200,
      message: 'Address deleted successfully',
    },
  });
});

const changeStatusAddress = catchAsync(async (req, res) => {
  await addressService.changeStatusAddressById(req.body.userId, req.body);
  res.status(httpStatus.OK).send({
    result: {
      code: 200,
      message: 'Address status change successfully',
    },
  });
});

module.exports = {
  addAddress,
  getAddressList,
  addressDetail,
  updateAddress,
  deleteAddress,
  changeStatusAddress,
};
