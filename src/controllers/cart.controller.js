const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const { productService, cartService, attributeService, couponService,tokenService } = require('../services');
const mongoose = require('mongoose');


const assignUserId = catchAsync(async (req, res) => {
  const cart = await cartService.assignUser(req.body.userId, req.body.cartId)
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: httpStatus.OK,
      message: 'User Added To Cart!',
    },
    cart,
  });
})


const calculateCart = catchAsync(async (req,res)=> {
  const cart = await cartService.makeCalculations(req);
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: httpStatus.OK,
      message: 'Cart Calculations Details',
    },
    cart
  });
})

// Add to Cart
const addToCart = catchAsync(async (req, res) => {
  const cart = await cartService.addProdCart(req);
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: httpStatus.OK,
      message: 'Item Added To Cart',
    },
    cart
  });
});

const getCart = catchAsync(async (req, res) => {

  const cartItems = await cartService.getCartbyId(req);


  const itemAttr = await attributeService.getAttributeListData()

  res.status(httpStatus.OK).send({
    serverResponse: {
      code: httpStatus.OK,
      message: 'All Cart Items',
    },
    cartItems,
    itemAttr
  });

});


const updatecart = catchAsync(async (req,res) => {
  const update = await cartService.updateCartQt(req.body)
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: httpStatus.OK,
      message: 'Cart Updated Successfully',
    },
    update,
  });
})



const removeItem = catchAsync(async (req,res) => {
  const cartQt = await cartService.removeItem(req.body.cartId,req.body.itemId);
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: httpStatus.OK,
      message: 'Product Removed!',
    },
    cartQt
  });
})


const assignCoupon = catchAsync(async (req,res) => {
  const coupon = await couponService.getCouponByCodeAfterValidate(req.body.couponcode,req.body.cartId)
  let cartQt;
  if(coupon.length !== 0){
    cartQt = await cartService.assignCoupon(req);
  }
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: httpStatus.OK,
      message: 'Coupon Added',
    },
    coupon
  });
})


const removeCoupon = catchAsync(async (req,res) => {
  const cartQt = await cartService.deleteCouponFromCart(req);
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: httpStatus.OK,
      message: 'Coupon Removed',
    },
    cartQt
  });
})


const getQuantity = catchAsync(async (req,res) => {
  const cartQt = await cartService.totalQt(req);
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: httpStatus.OK,
      message: 'Coupon Removed',
    },
    cartQt
  });
})

module.exports = {
  addToCart,
  getCart,
  assignUserId,
  removeItem,
  updatecart,
  calculateCart,
  assignCoupon,
  removeCoupon,
  getQuantity,
};
