const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const { attributeService } = require('../services');

// Add Attribute
const addAttribute = catchAsync(async (req, res) => {
  await attributeService.addAttributeData(req.body);
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: 200,
      message: 'Attribute Added Successfully',
    },
  }); 
});

// Attribute List
const getAttributeList = catchAsync(async (req, res) => {
  const attributeList = await attributeService.getAttributeListData();
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: 200,
      message: 'Success',
    },
    result: {
      attributeList,
    },
  });
});

// Attribute Delete
const deleteAttribute = catchAsync(async (req, res) => {
  const id = req.params.id ? req.params.id : null;
  await attributeService.deleteAttribute(id);
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: 200,
      message: 'Attribute Deleted Successfully',
    },
  });
});

// Attribute Edit

const editAttribute = catchAsync(async (req, res) => {
  const { id } = req.params;
  const attribute = await attributeService.getAttributeById(id);
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: 200,
      message: 'Attribute Fetch Successfully',
    },
    result: {
      attribute,
    },
  });
});

// Attribute Update

const updateAttribute = catchAsync(async (req, res) => {
  const { id } = req.params;
  const attribute = await attributeService.updateAttributeById(id, req.body);
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: 200,
      message: 'Attribute Updated Successfully',
    },
    result: {
      attribute,
    },
  });
});

const getAttrsListById = catchAsync(async (req, res) => {
  const slugs = req.body.slugs;
  const attribute = await attributeService.getAttrsListById(slugs);
  res.status(httpStatus.OK).send({
    serverResponse: {
      code: 200,
      message: 'Attribute Fetch Successfully',
    },
    result: {
      attribute,
    },
  });
});
module.exports = {
  addAttribute,
  getAttributeList,
  deleteAttribute,
  editAttribute,
  updateAttribute,
  getAttrsListById,
};
