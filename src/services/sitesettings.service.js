const httpStatus = require('http-status');
const userService = require('./user.service');
const ApiError = require('../utils/ApiError');
const SiteSettings = require('../models/siteSettings.model');
const mongoose = require("mongoose")



const addupsitelogo = async (logo,id) => {
  try {
    const sitelogo = await SiteSettings.findById(id)
    sitelogo.header_logo = logo
    sitelogo.save()
    return
  } catch (error) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Site Data Not Found');
  }
};



const addupsitefooterlogo = async (logo,id) => {
  try {
    const sitelogo = await SiteSettings.findById(id)
    sitelogo.footer_logo = logo
    sitelogo.save()
    return
  } catch (error) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Site Data Not Found');
  }
};




const addNewSocial = async (link,id) => {
  try {
    return SiteSettings.findByIdAndUpdate(mongoose.Types.ObjectId(id),{$push:{social:link}})
  } catch (error) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Site Data Not Found');
  }
};

const updateSocialNow = async (link,id) => {
  try {
    return SiteSettings.findOneAndUpdate({'social._id':mongoose.Types.ObjectId(id)},{$set:{'social.$.link':link.link}})
  } catch (error) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Site Data Not Found');
  }
};

const getLogoData = async ()=>{
  try{
    return SiteSettings.findOne({})
  }catch(error){
    console.log(error)
    throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, 'Unable To Get Any Item');
  }
}

const getSocialData = async (id,objectId)=>{
  try{
    // console.log(id);
    // console.log(objectId);
    return SiteSettings.aggregate(
      [
        {
          '$match': {
            '_id': mongoose.Types.ObjectId(id)
          }
        }, {
          '$unwind': {
            'path': '$social', 
            'includeArrayIndex': 'string', 
            'preserveNullAndEmptyArrays': false
          }
        }, {
          '$match': {
            'social._id': mongoose.Types.ObjectId(objectId)
          }
        }, {
          '$project': {
            'social': 1
          }
        }
      ])
  }catch(error){
    console.log(error)
    throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, 'Unable To Get Any Item');
  }
}

const deleteSocialData = async (objectId,id)=>{
  try{
    return SiteSettings.findByIdAndUpdate({ "_id": mongoose.Types.ObjectId(objectId) },
    { $pull: { "social": { "_id": mongoose.Types.ObjectId(id) } } },
    { new: true }
  );
  }catch(error){
    throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, 'Unable To Delete');
  }
}

const updateFooter = async (body)=>{
  try{
    return SiteSettings.findByIdAndUpdate(body.id,{footer_text:body.footer_text,copyright_text:body.copyright_text,insta_followlink:body.insta_followlink})
  }catch(error){
    console.log(error)
    throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, 'Unable To Get Any Item');
  }
}


const updateHeader = async (body)=>{
  try{
    return SiteSettings.findByIdAndUpdate(body.id,{header_text:body.header_text})
  }catch(error){
    console.log(error)
    throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, 'Unable To Get Any Item');
  }
}


module.exports = {
  addupsitelogo,
  addNewSocial,
  updateSocialNow,
  getLogoData,
  getSocialData,
  updateFooter,
  updateHeader,
  deleteSocialData,
  addupsitefooterlogo
};
