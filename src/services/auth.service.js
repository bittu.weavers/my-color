const httpStatus = require('http-status');
const tokenService = require('./token.service');
const userService = require('./user.service');
const Token = require('../models/token.model');
const ApiError = require('../utils/ApiError');
const { tokenTypes } = require('../config/tokens');

/**
 * Login with username and password
 * @param {string} email
 * @param {string} password
 * @returns {Promise<User>}
 */
const loginUserWithEmailAndPassword = async (email, password) => {
  const user = await userService.getUserByEmail(email);
  if (user && (await user.isPasswordMatch(password))) {
    await userService.updateUserById(user.id, {
      isLoggedIn: true,
    });
    const user1 = await userService.getUserByEmail(email);
    return user1;
  }
  throw new ApiError(httpStatus.UNAUTHORIZED, 'Incorrect email or password');
};

/**
 * Logout
 * @param {string} refreshToken
 * @returns {Promise}
 */
const logout = async (refreshToken) => {
  const refreshTokenDoc = await Token.findOne({ token: refreshToken, type: tokenTypes.REFRESH, blacklisted: false });
  if (!refreshTokenDoc) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Token Not Found');
  }
  await refreshTokenDoc.remove();
  await userService.updateUserById(refreshTokenDoc.user, {
    isLoggedIn: false,
  });
};

/**
 * Refresh auth tokens
 * @param {string} refreshToken
 * @returns {Promise<Object>}
 */
const refreshAuth = async (refreshToken) => {
  try {
    const refreshTokenDoc = await tokenService.verifyToken(refreshToken, tokenTypes.REFRESH);
    const user = await userService.getUserById(refreshTokenDoc.user);
    if (!user) {
      throw new Error();
    }
    await refreshTokenDoc.remove();
    return tokenService.generateAuthTokens(user);
  } catch (error) {
    throw new ApiError(httpStatus.UNAUTHORIZED, 'Please authenticate');
  }
};

/**
 * Reset password
 * @param {string} resetPasswordToken
 * @param {string} newPassword
 * @returns {Promise}
 */
const resetPassword = async (resetPasswordToken, newPassword) => {
  try {
    const resetPasswordTokenDoc = await tokenService.verifyToken(resetPasswordToken, tokenTypes.RESET_PASSWORD);
    const user = await userService.getUserById(resetPasswordTokenDoc.user);
    if (!user) {
      throw new Error();
    }
    await Token.deleteMany({ user: user.id, type: tokenTypes.RESET_PASSWORD });
    await userService.updateUserById(user.id, { password: newPassword });
  } catch (error) {
    throw new ApiError(httpStatus.UNAUTHORIZED, 'Password reset failed');
  }
};

const loginUserWithSocialID = async (req) => {
  const { socialId, userType } = req;
  // const {userType = req.userType;
  req.isFacebook = false;
  req.isGoogle = false;
  req.isLoggedIn = true;

  let user = await userService.getUserBySocialId(socialId, userType);
  if (!user) {
    if (req.email !== '') {
      user = await userService.getUserByEmail(req.email);
    }
  }
  if (userType === 'facebook') {
    req.facebookId = socialId;
    req.isFacebook = true;
  }
  if (userType === 'google') {
    req.googleId = socialId;
    req.isGoogle = true;
  }
  if (!user) {
    if (!req.email) {
      throw new ApiError(405, 'Email is required');
    }
    user = await userService.createUser(req);
  } else {
    if (!req.email) {
      delete req.email;
    }
    user = await userService.updateUserById(user._id, req);
  }
  return user;
};

module.exports = {
  loginUserWithEmailAndPassword,
  logout,
  refreshAuth,
  resetPassword,
  loginUserWithSocialID,
};
