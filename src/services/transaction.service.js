const { transaction } = require('../models');

// Add Data Tranaction
const transactionAdd = async (transObj) => {
  const result = await transaction.create(transObj);
  return result;
};

module.exports = {
  transactionAdd,
};
