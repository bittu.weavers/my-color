const httpStatus = require('http-status');
const fs = require('fs');
const util = require('util');
const ApiError = require('../utils/ApiError');
const { uploadFile, getFileStream, deleteFile } = require('./s3.service');
const config = require('../config/config');
const Post = require('../models/postModule.model');

const ITEMS_PER_PAGE = 10;
const unlinkFile = util.promisify(fs.unlink);

/**
 * Upload a image
 * @param {Object} uploadbody
 * @returns {Promise<Blog>}
 */
const uploadImage = async (uploadreq) => {
  const { file } = uploadreq;
  const path = uploadreq.body.path ? uploadreq.body.path : '/images';
  if (file) {
    const result = await uploadFile(file, path);
    await unlinkFile(`public/images/${file.filename}`);
    return result;
  }
  return [];
};

const getImages = async (currentPage) => {
  try {
    const totalItems = await Upload.find().countDocuments();
    const images = await Upload.find()
      .sort({ created_at: -1 })
      .skip((currentPage - 1) * ITEMS_PER_PAGE)
      .limit(ITEMS_PER_PAGE);

    return {
      images,
      page: currentPage,
      limit: ITEMS_PER_PAGE,
      totalPages: Math.ceil(totalItems / ITEMS_PER_PAGE),
      totalResults: totalItems,
    };
  } catch (err) {
    if (err) {
      throw err;
    }
  }
};

const deleteimage = async (path, body) => {
  try {
    if (body.type == 'post') {
      const conditions = { _id: body.id }; let
      var update = { $inc: { image: {} } };
      const valueUpdate = { $set: { image: { url: '', path: '' } } };
      // await Post.updateOne(conditions, update);
      await Post.updateOne(conditions, valueUpdate);
    } else if (body.type == 'page') {
    }
    return await deleteFile(path);
  } catch (err) {
    if (err) {
      throw new ApiError(httpStatus.NOT_FOUND, err);
    }
  }
};
module.exports = {
  uploadImage,
  getImages,
  deleteimage,
};
