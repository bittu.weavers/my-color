const httpStatus = require('http-status');
const ApiError = require('../utils/ApiError');
const colourstorydb = require("../models/colourstories.model");
const mongoose = require("mongoose")

const ITEMS_PER_PAGE = 8;


const addStoryDb = async (data) => {
    try {
        data.name = data.first_name +" "+data.last_name
        delete data.first_name
        delete data.last_name
        return  colourstorydb.create(data)
    } catch (error) {
        console.log(error)
        throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, 'Unable To Add Entry');
    }

};


const getAllData = async (currentPage) => {
    try {
        const totalItems =await colourstorydb.find().countDocuments();
        const storyList=await colourstorydb.find().sort({ created_at: -1 })
        .skip((currentPage - 1) * ITEMS_PER_PAGE)
        .limit(ITEMS_PER_PAGE);
  
      const storyListData = {
        storyList,
        page: currentPage,
        limit: ITEMS_PER_PAGE,
        totalPages: Math.ceil(totalItems / ITEMS_PER_PAGE),
        totalResults: totalItems,
      };
        return storyListData;
    } catch (error) {
        console.log(error)
        throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, 'Unable To Get Any Item');
    }

};



const getStoryData = async (data) => {
    try {
        return  colourstorydb.find({_id:mongoose.Types.ObjectId(data)})
    } catch (error) {
        console.log(error)
        throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, 'Unable To Get Any Item');
    }

};


const updateStory = async (data) => {
    try {
        return  colourstorydb.findByIdAndUpdate(data.id,data)
    } catch (error) {
        console.log(error)
        throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, 'Unable To Get Any Item');
    }

};

const deletethestory = async (data) => {
    try {
        return  colourstorydb.findByIdAndDelete(data)
    } catch (error) {
        console.log(error)
        throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, 'Unable To Get Any Item');
    }

};


const storyList = async (currentPage) => {
    try {
        const totalItems =await colourstorydb.find().countDocuments();
        const storyList=await colourstorydb.find()
        let totalRating = 0;
        for(let s of storyList){
            totalRating+=s.rating_count;
        }
        let avgRating= (totalRating/totalItems);

      const storyListData = {
        storyList,
        totalItems,
        avgRating
      };
        return storyListData;
    } catch (error) {
        console.log(error)
        throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, 'Unable To Get Any Item');
    }

};
module.exports = {
    addStoryDb,
    getAllData,
    getStoryData,
    updateStory,
    deletethestory,
    storyList
};
