const httpStatus = require('http-status');
const Post = require('../models/postModule.model');
const ApiError = require('../utils/ApiError');
const { userService } = require('./index');
const mongoose = require("mongoose")

const ITEMS_PER_PAGE = 5;
// Add post
const addPostData = async (postBody, token) => {
  const userId = await userService.getUserById(mongoose.Types.ObjectId(token.sub));
  const postBodyObj = postBody;
  const finalObj = { ...postBodyObj, user: userId };
  const postdata = await Post.create(finalObj);
  return postdata;
};
// Get post By ID
const getPostById = async (id) => {
  return Post.findById(id);
};
// Delete post
const deletePost = async (id) => {
  const result = await getPostById(id);
  if (!result) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Post not found');
  }
  await result.remove();
  return result;
};
// Get Post List
const getAllPostList = async (currentPage, post_type) => {
  try {
    const totalItems = await Post.find({ post_type }).countDocuments();
    const posts = await Post.find({ post_type })
      .sort({ created_at: -1 })
      .populate('user', 'fname')
      .skip((currentPage - 1) * ITEMS_PER_PAGE)
      .limit(ITEMS_PER_PAGE);

    const postList = {
      posts,
      page: currentPage,
      limit: ITEMS_PER_PAGE,
      totalPages: Math.ceil(totalItems / ITEMS_PER_PAGE),
      totalResults: totalItems,
    };
    return postList;
  } catch (err) {
    if (err) {
      return new ApiError(httpStatus.NOT_FOUND, 'Post not found');
    }
  }
};
const getAllPostListFrontEnd = async (currentPage, post_type) => {
  try {
    const totalItems = await Post.find({ post_type, is_feature: false, is_active: true }).countDocuments();
    const posts = await Post.find({ post_type, is_feature: false, is_active: true })
      .sort({ created_at: -1 })
      .populate('user', 'fname')
      .skip((currentPage - 1) * 6)
      .limit(6);

    const postList = {
      posts,
      page: currentPage,
      limit: 6,
      totalPages: Math.ceil(totalItems / 6),
      totalResults: totalItems,
    };
    return postList;
  } catch (err) {
    if (err) {
      return new ApiError(httpStatus.NOT_FOUND, 'Post not found');
    }
  }
};
const featurePost = async (post_type) => {
  try {
    const featurePost = await Post.findOne({ is_feature: true, post_type });
    return featurePost;
  } catch (err) {
    if (err) {
      return new ApiError(httpStatus.NOT_FOUND, 'Post not found');
    }
  }
};
// Update Post
const updatePostById = async (id, updateBody) => {
  const result = await getPostById(id);
  if (!result) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Post not found');
  }
  Object.assign(result, updateBody);
  await result.save();
  return result;
};
// Update Post
const makePrimaryById = async (id, post_type) => {
  const result = await getPostById(id);
  if (!result) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Post not found');
  }
  await Post.updateMany({ post_type, is_feature: true }, { is_feature: false }, function (err, docs) {});
  if (result.is_feature) {
    result.is_feature = false;
  } else {
    result.is_feature = true;
  }
  await result.save();
  return result;
};
/**
 *
 * @param {*} post_type
 * @param {*} slug
 * @returns Object
 */
const findPostBySlug = async (post_type, slug) => {
  const post = await Post.findOne({ slug, post_type }).populate('user', 'name');
  if (!post) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Post not found');
  }
  return post;
};
const findRelatedPost = async (post_type, pexslug) => {
  const relatedposts = await Post.find({ post_type, slug: { $ne: pexslug } })
    .sort({ created_at: -1 })
    .limit(3);
  if (!relatedposts) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Related post not found');
  }
  return relatedposts;
};
module.exports = {
  addPostData,
  getPostById,
  deletePost,
  getAllPostList,
  updatePostById,
  makePrimaryById,
  findPostBySlug,
  findRelatedPost,
  featurePost,
  getAllPostListFrontEnd,
};
