const httpStatus = require('http-status');
const videopost = require('../models/videopost.model');
const ApiError = require('../utils/ApiError');
// Add Video
const addVideoData = async (userBody) => {
  const videodata = await videopost.create(userBody);
  return videodata;
};
// Get Video By ID
const getVideoById = async (id) => {
  return videopost.findById(id);
};
// Delete Video
const deleteVideo = async (id) => {
  const result = await getVideoById(id);
  if (!result) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Video not found');
  }
  await result.remove();
  return result;
};
// Get Video List
const getAllVideoList = async () => {
  const videoList = await videopost.find().sort({ createdAt: -1 });
  return videoList;
};
// Update Video
const updateVideoById = async (id, updateBody) => {
  const result = await getVideoById(id);
  if (!result) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Video not found');
  }
  Object.assign(result, updateBody);
  await result.save();
  return result;
};
module.exports = {
  addVideoData,
  getVideoById,
  deleteVideo,
  getAllVideoList,
  updateVideoById,
};
