// const httpStatus = require('http-status');
const setting = require('../models/setting.model');
// const ApiError = require('../utils/ApiError');

// Add setting
const addSettingData = async (settingBody) => {
  const settingdata = await setting.create(settingBody);
  return settingdata;
};

module.exports = {
  addSettingData,
};
