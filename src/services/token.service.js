const jwt = require('jsonwebtoken');
const moment = require('moment');
const mongoose = require('mongoose');
const httpStatus = require('http-status');
const config = require('../config/config');
const userService = require('./user.service');
const { Token } = require('../models');
const ApiError = require('../utils/ApiError');
const { tokenTypes } = require('../config/tokens');
const atob=require('atob');
/**
 * Generate token
 * @param {ObjectId} userId
 * @param {Moment} expires
 * @param {string} [secret]
 * @returns {string}
 */
const generateToken = (userId, expires, type, secret = config.jwt.secret) => {
    const payload = {
        sub: userId,
        iat: moment().unix(),
        exp: expires.unix(),
        type,
    };
    return jwt.sign(payload, secret);
};

// Get token By user_id
const getTokenByUserId = async(user) => {
    return Token.findOne({ user: mongoose.Types.ObjectId(user) });
};

// Delete token By user_id
const deleteTokenByUserId = async(userId) => {
    const result = await getTokenByUserId(userId);
    if (result) {
        // throw new ApiError(httpStatus.NOT_FOUND, 'token not found');
        await result.remove();
        return result;
    }
};

/**
 * Save a token
 * @param {string} token
 * @param {ObjectId} userId
 * @param {Moment} expires
 * @param {string} type
 * @param {boolean} [blacklisted]
 * @returns {Promise<Token>}
 */
/*const saveToken = async(token, userId, expires, type, blacklisted = false) => {
    await deleteTokenByUserId(userId);
    const tokenDoc = await Token.create({
        token,
        user: userId,
        expires: expires.toDate(),
        type,
        blacklisted,
    });
    return tokenDoc;
}; */

const saveToken = async (token, userId, expires, type, blacklisted = false) => {
  const tokenDoc = await Token.create({
    token,
    user: userId,
    expires: expires.toDate(),
    type,
    blacklisted,
  });
  return tokenDoc;
};

/**
 * Verify token and return token doc (or throw an error if it is not valid)
 * @param {string} token
 * @param {string} type
 * @returns {Promise<Token>}
 */
const verifyToken = async(token, type) => {
    const payload = jwt.verify(token, config.jwt.secret);
    const tokenDoc = await Token.findOne({ token, type, user: payload.sub, blacklisted: false });
    if (!tokenDoc) {
        throw new Error('Token not found');
    }
    return tokenDoc;
};
/**
 * Verify token and return token doc (or throw an error if it is not valid)
 * @param {string} token
 * @param {string} type
 * @returns {Promise<Token>}
 */
const checkverifyToken = async(token, type, role) => {
    const payload = jwt.verify(token, config.jwt.secret);
    const tokenDoc = await Token.findOne({
        token,
        type,
        user: payload.sub,
        blacklisted: false,
    });
    const checkUser = await userService.checkUserByIdAndRole(payload.sub, role);
    if (checkUser) {
        return tokenDoc;
    }
    throw new ApiError(httpStatus.NOT_FOUND, 'Token is invalid');
};

/**
 * Generate auth tokens
 * @param {User} user
 * @returns {Promise<Object>}
 */
/*const generateAuthTokens = async(user) => {
    if (await checkIfTokenexpired(user)) {
        // console.log('11');
        const tokenData = await getTokenByUserId(user.id);
        // console.log(tokenData);
        return {
            access: {
                token: tokenData.token,
                expires: tokenData.expires,
            },
            refresh: {
                token: tokenData.token,
                expires: tokenData.expires,
            },
        };
    }
    const accessTokenExpires = moment().add(config.jwt.accessExpirationMinutes, 'minutes');
    const accessToken = generateToken(user.id, accessTokenExpires, tokenTypes.ACCESS);

    const refreshTokenExpires = moment().add(config.jwt.refreshExpirationDays, 'days');
    const refreshToken = generateToken(user.id, refreshTokenExpires, tokenTypes.REFRESH);
    await saveToken(refreshToken, user.id, refreshTokenExpires, tokenTypes.REFRESH);
    return {
        access: {
            token: accessToken,
            expires: accessTokenExpires.toDate(),
        },
        refresh: {
            token: refreshToken,
            expires: refreshTokenExpires.toDate(),
        },
    };
}; */

const generateAuthTokens = async (user) => {
  const accessTokenExpires = moment().add(config.jwt.accessExpirationMinutes, 'minutes');
  const accessToken = generateToken(user.id, accessTokenExpires, tokenTypes.ACCESS);

  const refreshTokenExpires = moment().add(config.jwt.refreshExpirationDays, 'days');
  const refreshToken = generateToken(user.id, refreshTokenExpires, tokenTypes.REFRESH);
  await saveToken(refreshToken, user.id, refreshTokenExpires, tokenTypes.REFRESH);
  return {
    access: {
      token: accessToken,
      expires: accessTokenExpires.toDate(),
    },
    refresh: {
      token: refreshToken,
      expires: refreshTokenExpires.toDate(),
    },
  };
};

/**
 * Generate reset password token
 * @param {string} email
 * @returns {Promise<string>}
 */
const generateResetPasswordToken = async(email) => {
    const user = await userService.getUserByEmail(email);
    if (!user) {
        throw new ApiError(httpStatus.NOT_FOUND, 'No users found with this email');
    }
    const expires = moment().add(config.jwt.resetPasswordExpirationMinutes, 'minutes');
    const resetPasswordToken = generateToken(user.id, expires, tokenTypes.RESET_PASSWORD);
    await saveToken(resetPasswordToken, user.id, expires, tokenTypes.RESET_PASSWORD);
    return resetPasswordToken;
};

/* const checkIfTokenexpired = async(user) => {
    const tokenData = await getTokenByUserId(user._id);

    if (tokenData) {
        return isTodayOrFuture(tokenData.expires);
    }
    return false;
}; */

/*function stripTime(date) {
    date = moment(date);
    return date;
} */

/*function isTodayOrFuture(date) {
    date = stripTime(date);
    // console.log(date);
    // console.log(moment.now());
    return date.diff(stripTime(moment.now())) >= 0;
}*/

const getUserIdFromToken = async(req) => {
  if(req.headers.hasOwnProperty('authorization')){
    const authToken = (req.headers && req.headers.hasOwnProperty('authorization')) ? req.headers.authorization : '';
    const token=authToken.split(' ')[1]
    var base64Url = token.split('.')[1];
    var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    const userData=JSON.parse(atob(base64))
    return (userData)?userData.sub:'';
  }
    return '';
};
module.exports = {
    generateToken,
    saveToken,
    verifyToken,
    generateAuthTokens,
    generateResetPasswordToken,
    checkverifyToken,
    getUserIdFromToken
    // checkIfTokenexpired,
};
