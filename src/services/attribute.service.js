const httpStatus = require('http-status');
const Attribute = require('../models/attribute.model');
const ApiError = require('../utils/ApiError');
const mongoose = require('mongoose');

const ITEMS_PER_PAGE = 10;
// Add attribute
const addAttributeData = async (attributeBody) => {
  const attributedata = await Attribute.create(attributeBody);
  return attributedata;
};

// List attribute
const getAttributeListData = async () => {
  return Attribute.find();
};

// Get Attribute By ID
const getAttributeById = async (id) => {
  return Attribute.findById(id);
};

// Delete Attribute
const deleteAttribute = async (id) => {
  const result = await getAttributeById(id);
  if (!result) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Attribute not found');
  }
  await result.remove();
  return result;
};
const updateAttributeById = async (id, updateBody) => {
  const query = { _id: id };
  const valueUpdate = { $set: updateBody };
  const result = await Attribute.updateOne(query, valueUpdate, async function (err, result) {
    if (err) {
      throw new ApiError(httpStatus.BAD_REQUEST, 'Unable to process this request');
    }
  });
  return result;
};
const getAttrsListById = async (slugs) => {
  const result = await Attribute.find(
    {
      slug: {
        $in: slugs,
      },
    },
    function (err, docs) {}
  );
  if (!result) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Attribute not found');
  }
  return result;
};

const getAttributeValuesByArray = async (arr) => {
  let even = [];
  let odd = [];
  arr.forEach(async (element) => {
    element.forEach(async (item) => {
      item.forEach((val, index) => {
        if (index % 2 === 0) {
          even.push(val);
        } else {
          odd.push(mongoose.Types.ObjectId(val));
        }
      });
    });
  });
  const attr = await Attribute.aggregate([
    {
      $match: {
        slug: {
          $in: even,
        },
      },
    },
    {
      $project: {
        value: 1,
      },
    },
    {
      $unwind: {
        path: '$value',
        includeArrayIndex: 'string',
        preserveNullAndEmptyArrays: false,
      },
    },
    {
      $match: {
        'value._id': {
          $in: odd,
        },
      },
    },
    {
      $project: {
        _id: 0,
        value: 1,
      },
    },
  ]);
  return attr
};

module.exports = {
  addAttributeData,
  getAttributeListData,
  getAttributeById,
  deleteAttribute,
  updateAttributeById,
  getAttrsListById,
  getAttributeValuesByArray,
};
