const httpStatus = require('http-status');
const ApiError = require('../utils/ApiError');
const Order = require('../models/order.model');
const Transaction = require("../models/transaction.model");
const mongoose = require('mongoose');

const placeOrder = async (data) => {
  const order = await Order.create(data);
  if (!order) {
    throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, 'Can Not Place Order');
  }
  return order;
};

const getOrderDetails = async (orderid) => {
  try {
    const order = await Order.aggregate([
      {
        $match: {
          orderId: orderid,
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'userId',
          foreignField: '_id',
          as: 'user',
        },
      },
      {
        $unwind: {
          path: '$user',
          includeArrayIndex: 'string',
          preserveNullAndEmptyArrays: false,
        },
      },
    ]);
    return order;
  } catch (error) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Can Not Find Order Details');
  }
};

const getAllOrders = async (id) => {
  try {
    const orders = await Order.aggregate([
      {
        $match: {
          userId: mongoose.Types.ObjectId(id),
        },
      },
      {
        $sort: { createdAt: -1 },
      },
    ]);
    return orders;
  } catch (error) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Can Not Find Order Details');
  }
};

const cancellOrder = async (id) => {
  try {
    //send mails to user order cancelled
    //send mail to admin oderd cancelled
    const order = await Order.findOne({ orderId: id });
    order.orderStatus = 'cancelled';
    order.save();
    return order;
  } catch (error) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Can Not Find The Order');
  }
};

const ListOrders = async (req) => {
  try {
    let totalItems;
    let orders;
    if (req.params.orderId) {
      orders = await Order.find({ orderId: req.params.orderId })
        .populate('userId')
        .sort({ createdAt: -1 })
        .skip((parseInt(req.params.pageno) - 1) * 10)
        .limit(10);
      totalItems = await Order.find({ orderId: req.params.orderId }).sort({ createdAt: -1 }).countDocuments();
    } else {
      orders = await Order.find()
        .populate('userId')
        .sort({ createdAt: -1 })
        .skip((parseInt(req.params.pageno) - 1) * 10)
        .limit(10);
      totalItems = await Order.find().sort({ createdAt: -1 }).countDocuments();
    }
    const orderList = {
      orders,
      page: req.params.pageno,
      limit: 10,
      totalPages: Math.ceil(totalItems / 10),
      totalResults: totalItems,
    };
    return orderList;
  } catch (error) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Can Not Find The Order');
  }
};

const updateStatus = async (body) => {
  try {
    let ordstatus;

    if (body.orderStatus === 'confirm') {
      ordstatus = 'Order Confirmed';
    } else if (body.orderStatus === 'shipped') {
      ordstatus = 'Order Shipped';
    } else if (body.orderStatus === 'delivery') {
      ordstatus = 'Out For Delivery';
    } else if (body.orderStatus === 'delivered') {
      ordstatus = 'Order Delivered';
    } else if (body.orderStatus === 'refund') {
      ordstatus = 'Refund Initiated';
    } else if (body.orderStatus === 'refunded') {
      ordstatus = 'Refunded';
    } else if (body.orderStatus === 'cancelled') {
      ordstatus = 'Cancelled';
    }

    return await Order.updateOne({ _id: body.orderId }, { $set: { orderStatus: ordstatus } });
  } catch (error) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Can Not Find The Order');
  }
};

const refundInit = async (body) => {
  try {
    const orderObj = await Order.aggregate([
      {
        $match: {
          _id: mongoose.Types.ObjectId(body.orderId),
        },
      },
      {
        $lookup: {
          from: 'transactions',
          localField: 'transaction_id',
          foreignField: '_id',
          as: 'result',
        },
      },
      {
        $unwind: {
          path: '$result',
          includeArrayIndex: 'string',
          preserveNullAndEmptyArrays: false,
        },
      },
    ]);

    const charge = orderObj[0].result.charge;
    let data;
    if (body.amount && body.amount !== '') {
      data = {
        charge: charge,
        amount: Math.round(body.amount * 100),
        transaction_id: orderObj[0].transaction_id,
      };
    } else {
      data = {
        charge: charge,
        transaction_id: orderObj[0].transaction_id,
      };
    }

    return data;
  } catch (error) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Can Not Find The Order');
  }
};

const afterRefundUpdates = async (orderid,transactionid,amt) => {
  try {
    await Order.updateOne({_id:mongoose.Types.ObjectId(orderid)},{orderStatus:"refunded",refundAmount:amt});
    await Transaction.updateOne({_id:mongoose.Types.ObjectId(transactionid)},{refundstatus:true});
    return;
  } catch (error) {
    throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, 'Somethng Went Wrong');
  }
};


const getOrderData = async (orderid) => {
  try {
    const order  = await Order.aggregate([
      {
        $match: {
          _id: mongoose.Types.ObjectId(orderid)
        }
      }, {
        $lookup: {
          from: 'users', 
          let: {
            uid: '$userId', 
            aid: '$addressType'
          }, 
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: [
                    '$_id', '$$uid'
                  ]
                }
              }
            }, {
              $unwind: {
                path: '$address', 
                preserveNullAndEmptyArrays: true
              }
            }, {
              $match: {
                $expr: {
                  $eq: [
                    '$address._id', '$$aid'
                  ]
                }
              }
            }, {
              $project: {
                address: 1, 
                fname: 1, 
                lname: 1, 
                email: 1
              }
            }
          ], 
          as: 'address'
        }
      }
    ]);
    return order
  } catch (error) {
    console.log(error)
    throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, 'Somethng Went Wrong');
  }
};

module.exports = {
  placeOrder,
  getOrderDetails,
  getAllOrders,
  cancellOrder,
  ListOrders,
  updateStatus,
  refundInit,
  afterRefundUpdates,
  getOrderData
};
