const httpStatus = require('http-status');
const { wishlist, User } = require('../models');

const ApiError = require('../utils/ApiError');
const mongoose = require('mongoose');
const ITEMS_PER_PAGE = 3;
// Add Product To Wishlist
const createWishlist = async (req) => {
  // const wishlists = await User.create(userBody);
  const wishlists = await User.update({ _id: mongoose.Types.ObjectId(req.user.id) },
    {
      $push: {
        "wishlist": req.body
      }
    }
  );
  return wishlists;
};

// Get Product Wishlist By ID
const getProductwishlistById = async (id) => {
  return wishlist.findById(id);
};

//get WishlistItems
const getWishlist = async (req,currentPage=1) => {

  let getWishlist = [{
    $match: {
      _id: mongoose.Types.ObjectId(req.user.id)
    }
  }, {
    $project: {
      wishlist: 1
    }
  }, {
    $lookup: {
      from: 'products',
      pipeline: [
        {
          $match: {
            is_publish: "publish"
          }
        }
      ],
      localField: 'wishlist.productId',
      foreignField: '_id',
      as: 'products'
    }
  }, {
    $unwind: {
      path: "$products",
      preserveNullAndEmptyArrays: false
    }
  }, {
    $project: {
      wishlistId: "$wishlist._id",
      productId: "$products._id",
      title: "$products.title",
      price: "$products.price",
      type: "$products.type",
      min_price: "$products.min_price",
      max_price: "$products.max_price",
      short_desc: "$products.short_desc",
      weight: {
        $ifNull: ['$products.weight', ''],
      },
      slug: "$products.slug",
      product_image: "$products.product_image",
      variation: "$products.variation"
    }
  }, {
    $lookup:

    {
      from: 'reviews',
      pipeline: [
        { $match: { is_approve: true } }
      ],
      localField: 'productId',
      foreignField: 'product_id',
      as: 'review'
    }
  }, {
    $addFields: {
      reviewCount: { $size: "$review" },
      reviewAvgRaw: { $ceil: { $avg: "$review.rating_count" } },
      all_childattr: {
        '$concatArrays': [
          '$variation.attribute'
        ]
      },
    }
  }, {
    $addFields: {
      convertObjectToarray: {
        "$map": {
          "input": "$all_childattr",
          "as": "allchildAttr",
          "in": {
            id: { $objectToArray: "$$allchildAttr" },
          }
        }
      },

    }
  }, {
    $addFields: {
      allchildAttrids: {
        $reduce: {
          input: "$convertObjectToarray",
          initialValue: [],
          in: { $concatArrays: ["$$value.id", "$$this.id.v"] }
        }
      }
    }
  }, {
    $addFields: {
      childattrObjIds: {
        "$map": {
          "input": "$allchildAttrids",
          "as": "allchildAttr",
          "in": {
            childId: { $convert: { input: "$$allchildAttr", to: "objectId" } },
          }
        }
      }
    }
  }, {
    $lookup: {
      from: 'attributes',
      let: { "parrentAttrIds": "$childattrObjIds" },
      "pipeline": [
        {
          "$unwind": "$value"
        },

        { "$match": { "$expr": { "$in": ["$value._id", "$$parrentAttrIds.childId"] } } },
        {
          "$project": {
            _id: 1,
            name: 1,
            value: { $sum: 1 }
          }
        },
        {
          "$group": {
            _id: "$name",
            total: { $sum: "$value" }
          }
        }
      ],
      "as": "attrGroup"
    }
  },
  {
    $facet: {
      data: [{
        $skip: (currentPage - 1) * 4
      },
      {
        $limit: 4
      }
      ],
      pageInfo: [{
        $group: {
          _id: null,
          count: {
            $sum: 1
          }
        }
      }]
    }

  },
  {
    $unwind: {
      path: '$pageInfo',
    }
  }
  ]
  let wishlistItems=await User.aggregate(getWishlist);
  let count = (wishlistItems.length) ? wishlistItems[0].pageInfo.count : 0;
    let totalResults = (wishlistItems.length) ? wishlistItems[0].pageInfo.count : 0;
    const wishList = {
      wishlistItems: (wishlistItems.length) ? wishlistItems[0].data : [],
      page: currentPage,
      limit: 4,
      totalPages: Math.ceil(count / 4),
      totalResults: totalResults,
    };
  return wishList;
};
// Delete Product Category
const deleteProductWishlistById = async (req) => {
  const result = await User.findByIdAndUpdate({ _id: mongoose.Types.ObjectId(req.user.id) },
    { $pull: { "wishlist": { "productId": mongoose.Types.ObjectId(req.body.id) } } },
    { new: true }
  );
  return result;
};

module.exports = {
  createWishlist,
  getProductwishlistById,
  deleteProductWishlistById,
  getWishlist
};
