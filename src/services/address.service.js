const httpStatus = require('http-status');
const userService = require('./user.service');
const ApiError = require('../utils/ApiError');

/**
 * Add Address By User ID
 * @param {objectId} userID
 * @param {objectId} addressData
 */
const createAddress = async (userId, addressData) => {
  const address = await userService.addAddressById(userId, addressData);
  return address;
};

/**
 * Get Address List By User ID
 * @param {objectId} userID
 */
const getAddressByUserId = async (userId) => {
  const user = await userService.getUserById(userId);
  if (user) {
    return user.address;
  }
  throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
};

/**
 * Get Address detail By User ID
 * @param {objectId} userID
 * @param {objectId} addressId
 */
const getAddressDetailByUserId = async (userId, addressId) => {
  return userService.getAddressByAddressUserID(userId, addressId);
};

/**
 * update Address By User ID
 * @param {objectId} userID
 * @param {object} addressData
 */
const updateAddressById = async (userId, addressData) => {
  const address = await userService.updateAddressByUserId(userId, addressData);
  return address;
};

/**
 * Delete Address By User ID
 * @param {objectId} userID
 * @param {objectId} addressId
 */
const deleteAddressById = async (userId, addressId) => {
  await userService.deleteAddressByUserId(userId, addressId);
};

/**
 * change Address status By User ID
 * @param {objectId} userID
 * @param {object} addressData
 */
const changeStatusAddressById = async (userId, addressData) => {
  const address = await userService.changeStatusAddressByUserId(userId, addressData);
  return address;
};

module.exports = {
  createAddress,
  getAddressByUserId,
  getAddressDetailByUserId,
  updateAddressById,
  deleteAddressById,
  changeStatusAddressById,
};
