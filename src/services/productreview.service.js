const httpStatus = require('http-status');
const productreview = require('../models/productreview.model');
const ApiError = require('../utils/ApiError');

// Add Product Review
const addProductreview = async (userBody) => {
  const productreviews = await productreview.create(userBody);
  return productreviews;
};

// Get Product Review By ID
const getproductreviewbyid = async (id) => {
  return productreview.findById(id);
};

// Delete Product Review
const deleteproductsreview = async (id) => {
  const result = await getproductreviewbyid(id);
  if (!result) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Product review not found');
  }
  await result.remove();
  return result;
};

// Change Product Review Status
const changeproductreviewstatus = async (id, updateBody) => {
  const result = await getproductreviewbyid(id);
  if (!result) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Product review not found');
  }
  Object.assign(result, updateBody);
  await result.save();
  return result;
};

module.exports = {
  addProductreview,
  deleteproductsreview,
  getproductreviewbyid,
  changeproductreviewstatus,
};
