const httpStatus = require('http-status');
const Post = require('../models/postModule.model');
const ApiError = require('../utils/ApiError');
const mongoose = require("mongoose");
const Tax = require("../models/tax.model");

const taxupdate = async (data) => {
    try {
        await Tax.findByIdAndUpdate(data.id, data)
    } catch (error) {
        throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, 'Unable To Update');
    }
};

const getTaxes = async () => {
    try {
        const data = await Tax.find()
        return data
    } catch (error) {
        throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, 'Unable To Update');
    }
};


const addTax = async (data) => {
    try {
        await Tax.create(data)
    } catch (error) {
        throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, 'Unable To Add');
    }
};


const getTaxbyId = async (id) => {
    try {
        const data = await Tax.findById(id)
        return data
    } catch (error) {
        throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, 'Unable To Find Data');
    }
};

const removeTaxById = async (id) => {
    try {
        await Tax.findByIdAndDelete(id)
        return
    } catch (error) {
        throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, 'Unable To Find Data');
    }
};

const getTaxWithFilter = async (country, state) => {
    try {
        const tax = await Tax.aggregate([
            {
                $match: {
                    $and: [{ country: country }, { state: state }]
                }
            }
        ])
        return tax
    } catch (error) {
        throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, 'Unable To Find Data');
    }
};





module.exports = {
    taxupdate,
    getTaxes,
    addTax,
    getTaxbyId,
    removeTaxById,
    getTaxWithFilter
};
