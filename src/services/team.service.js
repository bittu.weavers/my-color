const httpStatus = require('http-status');
const Team = require('../models/team.model');
const ApiError = require('../utils/ApiError');
const { userService } = require('./index');

const ITEMS_PER_PAGE = 8;
// Add team
const addTeamData = async (teamBody, userId) => {
  const teamBodyObj = teamBody;
  const finalObj = { ...teamBodyObj, user: userId };
  const teamdata = await Team.create(finalObj);
  return teamdata;
};
// Get team By ID
const getTeamById = async (id) => {
  return Team.findById(id);
};
// Delete team
const deleteTeam = async (id) => {
  const result = await getTeamById(id);
  if (!result) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Team not found');
  }
  await result.remove();
  return result;
};
// Get Team List
const getAllTeamList = async (currentPage) => {
  try {
    const totalItems = await Team.find().countDocuments();
    const teams = await Team.find()
      .sort({ created_at: -1 })
      .populate('user', 'fname')
      .skip((currentPage - 1) * ITEMS_PER_PAGE)
      .limit(ITEMS_PER_PAGE);

    const teamList = {
      teams,
      page: currentPage,
      limit: ITEMS_PER_PAGE,
      totalPages: Math.ceil(totalItems / ITEMS_PER_PAGE),
      totalResults: totalItems,
    };
    return teamList;
  } catch (err) {
    if (err) {
      return new ApiError(httpStatus.NOT_FOUND, 'Team not found');
    }
  }
};
const getAllTeamListFrontEnd = async (currentPage) => {
  try {
    const totalItems = await Team.find().countDocuments();
    const teams = await Team.find()
      .sort({ created_at: -1 })
      .populate('user', 'fname')
      .skip((currentPage - 1) * ITEMS_PER_PAGE)
      .limit(ITEMS_PER_PAGE);

    return {
      teams,
      page: currentPage,
      limit: ITEMS_PER_PAGE,
      totalPages: Math.ceil(totalItems / ITEMS_PER_PAGE),
      totalResults: totalItems,
    };
  } catch (err) {
    if (err) {
      return new ApiError(httpStatus.NOT_FOUND, 'Team not found');
    }
  }
};

// Update Team
const updateTeamById = async (id, updateBody) => {
  const result = await getTeamById(id);
  if (!result) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Team not found');
  }
  Object.assign(result, updateBody);
  await result.save();
  return result;
};

module.exports = {
  addTeamData,
  getTeamById,
  deleteTeam,
  getAllTeamList,
  updateTeamById,
  getAllTeamListFrontEnd,
};
