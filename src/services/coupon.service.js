const httpStatus = require('http-status');
const mongoose  = require('mongoose');
const coupon = require('../models/coupon.model');
const ApiError = require('../utils/ApiError');
// Add Coupon
const addCouponCodeData = async (userBody) => {
  const coupondata = await coupon.create(userBody);
  return coupondata;
};
// Get Coupon By ID
const getCouponCodeById = async (id) => {
  return coupon.findById(id);
};

const decrimentCoupon = async (id) => {
  try {
    const couponData = await coupon.findById(mongoose.Types.ObjectId(id))
    if(couponData.coupon_limit - 1 <= 0){
      couponData.coupon_limit = 0
    }else{
      couponData.coupon_limit = couponData.coupon_limit - 1
    }
    couponData.save() 
  } catch (error) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Coupon Code not found');
  }
}

const getCouponByCode = async (coupon_code) => {
  try {
    aggreQuery = [
      {
        '$match': {
          'coupon_code': coupon_code, 
          'coupon_expiry_date': {
            '$gte': new Date()
          }, 
          'coupon_limit': {
            '$gt': 0
          }
        }
      }
    ]
    return coupon.aggregate(aggreQuery);
  } catch (error) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Coupon Code not found');
  }
};
// Delete Coupon
const deleteCouponCode = async (id) => {
  const result = await getCouponCodeById(id);
  if (!result) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Coupon Code not found');
  }
  await result.remove();
  return result;
};
// Get Coupon List
const getAllCouponList = async (currentPage,limit) => {
    try {
        const totalItems = await coupon.find().countDocuments();
        const couponList = await coupon.find()
            .sort({ created_at: -1 })
            .skip((currentPage - 1) * limit)
            .limit(limit);
        return {
            couponList:couponList,
            page: currentPage,
            limit: limit,
            totalPages: Math.ceil(totalItems / limit),
            totalResults: totalItems,
        };
    } catch (err) {
        if (err) {
            throw err;
        }
    }
};
// Update Coupon
const updateCouponById = async (id, updateBody) => {
  const result = await getCouponCodeById(id);
  if (!result) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Coupon not found');
  }
  Object.assign(result, updateBody);
  await result.save();
  return result;
};


const getCouponByCodeAfterValidate = async (coupon_code,cartid) => {
  try {
    aggreQuery = [
      {
        '$match': {
          'coupon_code': coupon_code, 
          'coupon_expiry_date': {
            '$gte': new Date()
          },
          'coupon_start_date':{
            '$lte': new Date()
          },
          'coupon_limit': {
            '$gt': 0
          }
        }
      }
    ]
    return coupon.aggregate(aggreQuery);
  } catch (error) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Coupon Code not found');
  }
};

module.exports = {
  addCouponCodeData,
  getCouponCodeById,
  deleteCouponCode,
  getAllCouponList,
  updateCouponById,
  getCouponByCode,
  decrimentCoupon,
  getCouponByCodeAfterValidate
};
