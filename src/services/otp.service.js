const httpStatus = require('http-status');
const userService = require('./user.service');
const ApiError = require('../utils/ApiError');
const { Otp } = require('../models');

const getVerificationByEmailOtp = async (email, otp) => {
  return Otp.findOne({ email, otp });
};

/**
 * Delete otp by email
 * @param {string} email
 * @returns {Promise<Otp>}
 */
const deleteByEmailOtp = async (email) => {
  return Otp.findOneAndDelete({ email });
};

/**
 * Generate reset password OTP
 * @param {string} email
 * @returns {Promise<string>}
 */
const generateResetPasswordOTP = async (email) => {
  const user = await userService.getUserByEmail(email);
  if (user) {
    await deleteByEmailOtp(email);
    const otp = Math.floor(100000 + Math.random() * 900000);
    await Otp.create({ email, otp });
    return otp;
  }
  throw new ApiError(httpStatus.NOT_FOUND, 'No user found with this email');
};

/**
 * Get Verification by email and otp
 * @param {string} email
 * @param {string} otp
 * @returns {Promise<Otp>}
 */

/**
 * Generate check OTP Verifiaction
 * @param {string} email
 * @param {string} otp
 */
const checkOtpVerifiaction = async (email, otp) => {
  const otpVerification = await getVerificationByEmailOtp(email, otp);
  if (otpVerification) {
    return otpVerification;
  }
  throw new ApiError(httpStatus.NOT_FOUND, 'No data found with this email');
};

/**
 * Reset password
 * @param {string} otp
 * @param {string} newPassword
 * @returns {Promise}
 */
const resetPassword = async (email, otp, newPassword) => {
  const user = await userService.getUserByEmail(email);
  if (user) {
    const otpVerification = await getVerificationByEmailOtp(email, otp);
    if (otpVerification) {
      await deleteByEmailOtp(email);
      return userService.updateUserById(user._id, { password: newPassword });
    }
    throw new ApiError(httpStatus.NOT_FOUND, 'OTP dose not match');
  }
  throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
};

module.exports = {
  generateResetPasswordOTP,
  checkOtpVerifiaction,
  getVerificationByEmailOtp,
  deleteByEmailOtp,
  resetPassword,
};
