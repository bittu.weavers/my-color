const httpStatus = require('http-status');
const productcategory = require('../models/productcategory.model');
const ApiError = require('../utils/ApiError');

const ITEMS_PER_PAGE = 10;
// Add Product Category
const addProductcategory = async (userBody) => {
  const productcategories = await productcategory.create(userBody);
  return productcategories;
};

// List Product Category
const getProductCategory = async (currentPage) => {
  try {
    const catQuery = [{$lookup: {
        from: 'products',
        localField: '_id',
        foreignField: 'categoryid.category_id',
        as: 'products'
      }}, {$addFields: 
          { totalReplies: { $size: "$products" } }
      },
      {
          $facet: {
              data: [{
                      $skip: (currentPage - 1) * ITEMS_PER_PAGE
                  },
                  {
                      $limit: ITEMS_PER_PAGE
                  }
              ],
              pageInfo: [{
                  $group: {
                      _id: null,
                      count: {
                          $sum: 1
                      }
                  }
              }]
          }
      },
      {
          $unwind: {
              path: '$pageInfo',
          }
      }
    ]

    const totalItems = await productcategory.find().countDocuments();
    const productcategorylist = await productcategory.aggregate(catQuery)
    return {
      productcategorylist,
      page: currentPage,
      limit: ITEMS_PER_PAGE,
      totalPages: Math.ceil(totalItems / ITEMS_PER_PAGE),
      totalResults: totalItems,
    };
  } catch (err) {
    if (err) {
      throw err;
    }
  }
};

// Get Product Category
const getProductCategoryList = async () => {
  return productcategory.find({is_active:true}, { category_name: 1 });
};

// Get Product Category By ID
const getProductcategoryById = async (id) => {
  return productcategory.findById(id);
};

// Delete Product Category
const deleteProductcategoryById = async (id) => {
  const result = await getProductcategoryById(id);
  if (!result) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Product Category not found');
  }
  await result.remove();
  return result;
};

// Edit Product Category By ID
const updateProductcategoryById = async (id, body) => {
  const result = await getProductcategoryById(id);
  if (!result) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Product Category not found');
  }

  Object.assign(result, body);
  await result.save();
  return result;
};
// Edit Product Category By ID
const updateProductCategoryStatus = async (id) => {
  const result = await getProductcategoryById(id);
  if (!result) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Product Category not found');
  }
  if (result.is_active) {
    Object.assign(result, { is_active: false });
    await result.save();
  } else {
    Object.assign(result, { is_active: true });
    await result.save();
  }

  return result;
};

module.exports = {
  addProductcategory,
  getProductCategory,
  deleteProductcategoryById,
  getProductcategoryById,
  updateProductcategoryById,
  updateProductCategoryStatus,
  getProductCategoryList,
};
