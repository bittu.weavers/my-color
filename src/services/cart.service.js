const httpStatus = require('http-status');
const Cart = require('../models/cart.model');
const Coupon = require('../models/coupon.model');
const Prod = require('../models/product.model');
const ApiError = require('../utils/ApiError');
const productService = require('./product.service');
const catchAsync = require('../utils/catchAsync');
const mongoose = require('mongoose');
const tokenService = require('./token.service');
const taxService = require('./tax.service');
const couponService = require('./coupon.service');

const assignUser = async (userid, cartid) => {
  const userCart = await Cart.findOne({ userId: userid });
  const cart = await Cart.findById(cartid);
  if (userCart) {
    let items = userCart['items'].concat(cart['items']);
    let key = '';
    const out = items.reduce((a, v) => {
      if (typeof v.variableProductId !== 'undefined') {
        key = v.variableProductId + '_' + v.productId;
      } else {
        key = v.productId;
      }
      if (a[key]) {
        a[key].quantity = a[key].quantity + v.quantity;
      } else {
        a[key] = v;
      }
      return a;
    }, {});

    const newItems = [];

    for (let [key, value] of Object.entries(out)) {
      newItems.push(value);
    }

    userCart.items = newItems;
    await userCart.save();
    await removeCartById(cartid);
    return userCart;
  } else {
    cart.userId = userid;
    await cart.save();
    return cart;
  }
};

const checkProductInCart = async (cartid, productid, varid = null, userid) => {
  let aggreQuery = [];
  if (userid) {
    aggreQuery.push({
      $match: {
        userId: mongoose.Types.ObjectId(userid),
      },
    });
  } else {
    aggreQuery.push({
      $match: {
        _id: mongoose.Types.ObjectId(cartid),
      },
    });
  }
  aggreQuery.push({
    $unwind: {
      path: '$items',
      includeArrayIndex: 'string',
      preserveNullAndEmptyArrays: false,
    },
  });

  console.log(varid);

  if (varid !== null && varid !== '') {
    console.log('executed');
    aggreQuery.push({
      $match: {
        'items.productId': mongoose.Types.ObjectId(productid),
        'items.variableProductId': mongoose.Types.ObjectId(varid),
      },
    });
  } else {
    console.log('executed else');
    aggreQuery.push({
      $match: {
        'items.productId': mongoose.Types.ObjectId(productid),
      },
    });
  }

  aggreQuery.push({
    $project: {
      items: 1,
      _id: 1,
    },
  });

  return Cart.aggregate(aggreQuery);
};

const makeCalculations = async (req) => {
  try {
    let isCart;
    //get cart object
    isCart = await getCartbyId(req);

    //array of total price of all items
    let allItems;
    let subtotal;

    if (isCart.length) {
      allItems = isCart[0].items.map((item) => {
        return item.quantity * item.price;
      });
      //total sum of all array items
      subtotal = allItems.reduce((partialSum, a) => partialSum + a, 0);
    } else {
      subtotal = 0;
    }

    //get tax from database
    let tax;
    const taxObj = await taxService.getTaxWithFilter(req.body.country, req.body.state);

    if (taxObj.length) {
      tax = taxObj[0].tax;
    } else {
      tax = 0;
    }

    //delivery data
    let dellivery = 0;

    //get coupon data
    const coupon = await couponService.getCouponByCode(req.body.couponcode);

    let discount;
    if (coupon.length) {
      if (coupon[0].is_percentage) {
        discount = (subtotal * coupon[0].coupon_percentage_value) / 100;
      } else {
        discount = coupon[0].coupon_amount;
      }
    } else {
      discount = 0;
    }

    let afterDiscount = subtotal - discount;
    if (afterDiscount < 0) {
      afterDiscount = 0;
    }

    //calculate total price
    const taxVal = (afterDiscount * tax) / 100;
    const total = afterDiscount + taxVal + dellivery;

    if (isCart.length) {
      await Cart.updateOne(
        { _id: mongoose.Types.ObjectId(isCart[0]._id) },
        { $set: { tax: tax, subTotal: subtotal, Total: total, 'coupon.id': coupon[0]?._id } }
      );
    }

    const data = {
      subtotal,
      discount,
      afterDiscount,
      total,
      taxVal,
      dellivery,
    };

    return data;
  } catch (error) {
    throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, error);
  }
};

const addProdCart = async (req) => {
  try {
    let cartId = req.body.cartId;
    delete req.body.cartId;
    const userid = await tokenService.getUserIdFromToken(req);

    let Qt;
    let isCart;

    if (userid && userid !== '') {
      req.body.userId = userid;
      isCart = await getCartByUserid(userid);
    } else {
      isCart = await getCart(cartId);
    }


    console.log(req.body)

    let cart;

    if (req.body.items.variableProductId === '') {
      delete req.body.items.variableProductId;
    }

    if (isCart && isCart !== null) {
      const productInCart = await checkProductInCart(
        isCart._id,
        req.body.items.productId,
        req.body.items.variableProductId,
        userid
      );
      if (productInCart.length !== 0) {
        let qt = productInCart[0].items.quantity + req.body.items.quantity;
        cart = await Cart.updateOne(
          { _id: productInCart[0]._id, 'items._id': productInCart[0].items._id },
          { $set: { 'items.$.quantity': qt } }
        );
      } else {
        cart = await Cart.updateOne({ _id: isCart._id }, { $push: { items: req.body.items } });
      }
      Qt = await totalQtLocal(isCart._id);
    } else {
      cart = await Cart.create(req.body);
      Qt = await totalQtLocal(cart._id);
    }

    const data = {
      cart,
      Qt,
    };

    return data;
  } catch (error) {
    throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, error);
  }
};

const removeCartById = async (id) => {
  try {
    await Cart.findByIdAndDelete(id);
    return;
  } catch (error) {
    throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, error);
  }
};

const getCart = async (id) => {
  return Cart.findOne({ _id: id });
};

const getCartByUserid = async (id) => {
  return Cart.findOne({ userId: id });
};

const getCartbyId = async (req) => {
  const userid = await tokenService.getUserIdFromToken(req);
  let aggreQuery = [];
  if (userid) {
    aggreQuery.push({
      $match: {
        userId: mongoose.Types.ObjectId(userid),
      },
    });
  } else {
    aggreQuery.push({
      $match: {
        _id: mongoose.Types.ObjectId(req.body.cartId),
      },
    });
  }

  aggreQuery.push(
    {
      $lookup: {
        from: 'products',
        let: {
          pid: '$items.productId',
        },
        pipeline: [
          {
            $match: {
              $expr: {
                $in: ['$_id', '$$pid'],
              },
            },
          },
          {
            $project: {
              title: 1,
              short_desc: 1,
              price: 1,
              product_image: 1,
              slug:1,
            },
          },
        ],
        as: 'product',
      },
    },
    {
      $addFields: {
        items: {
          $map: {
            input: '$items',
            in: {
              $let: {
                vars: {
                  m: {
                    $arrayElemAt: [
                      {
                        $filter: {
                          input: '$product',
                          cond: {
                            $eq: ['$$mb._id', '$$this.productId'],
                          },
                          as: 'mb',
                        },
                      },
                      0,
                    ],
                  },
                },
                in: {
                  $mergeObjects: [
                    '$$this',
                    {
                      title: '$$m.title',
                      price: '$$m.price',
                      product_image: '$$m.product_image',
                      short_desc: '$$m.short_desc',
                      slug: '$$m.slug',
                    },
                  ],
                },
              },
            },
          },
        },
      },
    },
    {
      $lookup: {
        from: 'products',
        let: {
          pid: '$items.productId',
          vid: '$items.variableProductId',
        },
        pipeline: [
          {
            $match: {
              $expr: {
                $in: ['$_id', '$$pid'],
              },
            },
          },
          {
            $unwind: {
              path: '$variation',
              preserveNullAndEmptyArrays: true,
            },
          },
          {
            $project: {
              title: 1,
              variation: 1,
            },
          },
          {
            $match: {
              $expr: {
                $in: ['$variation._id', '$$vid'],
              },
            },
          },
        ],
        as: 'variation',
      },
    },
    {
      $addFields: {
        items: {
          $map: {
            input: '$items',
            in: {
              $let: {
                vars: {
                  m: {
                    $arrayElemAt: [
                      {
                        $filter: {
                          input: '$variation',
                          cond: {
                            $eq: ['$$mb.variation._id', '$$this.variableProductId'],
                          },
                          as: 'mb',
                        },
                      },
                      0,
                    ],
                  },
                },
                in: {
                  $mergeObjects: [
                    '$$this',
                    {
                      var_id: '$$m.variation._id',
                      price: '$$m.variation.price',
                      attribute: '$$m.variation.attribute',
                      product_image: '$$m.variation.product_image',
                      short_desc: '$$m.variation.variation_description',
                    },
                  ],
                },
              },
            },
          },
        },
      },
    }
  );

  const cartdata = Cart.aggregate(aggreQuery);

  if (!cartdata) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Cart Not Found');
  }

  return cartdata;
};

const updateCartQt = async (body) => {
  try {
    let cart;
    if (body.qt == 0) {
      cart = await removeItem(body.cartId, body.id);
    } else {
      cart = await Cart.updateOne(
        { _id: mongoose.Types.ObjectId(body.cartId), 'items._id': mongoose.Types.ObjectId(body.id) },
        { $set: { 'items.$.quantity': body.qt } }
      );
    }

    const Qt = await totalQtLocal(body.cartId);

    const data = {
      cart,
      Qt,
    };

    return data;
  } catch (error) {
    throw new ApiError(httpStatus.NOT_FOUND, error);
  }
};

const removeItem = async (cartId, itemId) => {
  try {
    await Cart.updateOne({ _id: cartId }, { $pull: { items: { _id: itemId } } });
    const cart = await getCart(cartId);
    if (cart.items.length) {
      return await totalQtLocal(cartId);
    } else {
      return removeCartById(cartId);
    }
  } catch (error) {
    console.log(error);
  }
};

const assignCoupon = async (req) => {
  try {
    const userid = await tokenService.getUserIdFromToken(req);

    let cart;

    if (userid) {
      console.log('executed user');
      cart = await Cart.updateOne({ userId: userid }, { $set: { 'coupon.couponCode': req.body.couponcode } });
    } else {
      console.log('executed else');
      cart = await Cart.updateOne({ _id: req.body.cartId }, { $set: { 'coupon.couponCode': req.body.couponcode } });
    }
    return cart;
  } catch (error) {
    console.log(error);
    throw new ApiError(httpStatus.NOT_FOUND, error);
  }
};

const deleteCouponFromCart = async (req) => {
  try {
    const userid = await tokenService.getUserIdFromToken(req);

    let cart;

    if (userid) {
      cart = await Cart.updateOne({ userId: userid }, { $unset: { coupon: '' } });
    } else {
      cart = await Cart.updateOne({ _id: req.body.cartId }, { $unset: { coupon: '' } });
    }

    return cart;
  } catch (error) {
    console.log(error);
    throw new ApiError(httpStatus.NOT_FOUND, error);
  }
};

const totalQt = async (req) => {
  try {
    const cartid = req.body.cartId;
    const userid = await tokenService.getUserIdFromToken(req);

    const aggreQuery = [];

    if (userid) {
      aggreQuery.push({
        $match: {
          userId: mongoose.Types.ObjectId(userid),
        },
      });
    } else {
      aggreQuery.push({
        $match: {
          _id: mongoose.Types.ObjectId(cartid),
        },
      });
    }
    aggreQuery.push(
      {
        $unwind: {
          path: '$items',
          includeArrayIndex: 'string',
          preserveNullAndEmptyArrays: false,
        },
      },
      {
        $group: {
          _id: 'dsfdsfdsf',
          totalQt: {
            $sum: '$items.quantity',
          },
        },
      }
    );

    return Cart.aggregate(aggreQuery);
  } catch (error) {
    throw new ApiError(httpStatus.NOT_FOUND, error);
  }
};

const totalQtLocal = async (id) => {
  try {
    const aggreQuery = [];
    aggreQuery.push(
      {
        $match: {
          _id: mongoose.Types.ObjectId(id),
        },
      },
      {
        $unwind: {
          path: '$items',
          includeArrayIndex: 'string',
          preserveNullAndEmptyArrays: false,
        },
      },
      {
        $group: {
          _id: 'dsfdsfdsf',
          totalQt: {
            $sum: '$items.quantity',
          },
        },
      }
    );

    return Cart.aggregate(aggreQuery);
  } catch (error) {
    throw new ApiError(httpStatus.NOT_FOUND, error);
  }
};

module.exports = {
  getCartbyId,
  removeCartById,
  addProdCart,
  assignUser,
  removeItem,
  updateCartQt,
  makeCalculations,
  assignCoupon,
  deleteCouponFromCart,
  totalQt,
  totalQtLocal,
};
