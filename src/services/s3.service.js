const httpStatus = require('http-status');
const fs = require('fs');
const S3 = require('aws-sdk/clients/s3');
const config = require('../config/config');
const ApiError = require('../utils/ApiError');

const bucketName = config.s3.S3_BUCKET_PATH;
const region = config.s3.S3_BUCKET_REGION;
const accessKeyId = config.s3.S3_ACCESS_KEY;
const secretAccessKey = config.s3.S3_SECRET_KEY;
const s3 = new S3({
  region,
  accessKeyId,
  secretAccessKey,
});

// uploads a file to s3
function uploadFile(file, path) {
  const fileStream = fs.createReadStream(file.path);
  const uploadParams = {
    Bucket: bucketName + path,
    Body: fileStream,
    Key: `${new Date().toISOString()}-${file.originalname}`,
  };
  return s3.upload(uploadParams).promise();
}
// downloads a file from s3
function getFileStream(fileKey) {
  const downloadParams = {
    Key: fileKey,
    Bucket: bucketName,
  };

  return s3.getObject(downloadParams).createReadStream();
}
function deleteFile(file) {
  const params1 = {
    Bucket: bucketName,
    Key: file,
  };

  s3.deleteObject(params1, (error, data) => {
    if (error) {
      console.log(error);
      throw new ApiError(httpStatus.NOT_FOUND, 'error');
    }
  });
  return true;
}

const checkImage = async(body, folderPath)=>{
  for (let key of Object.keys(body)) {
      if (body[key] instanceof Array) {
          await Promise.all(body[key].map(async(element,index) => {
              await checkImage(element, folderPath);
          }));
      } else {
          
          if (key == folderPath) {
            
            await deleteFileByUrl(body[key], folderPath);
          }
      }
  } 
}

const deleteFileByUrl = async(url, folderPath) =>{
  let folderName = folderPath.split('_')[0];
  let key = url.split(folderName+'/');
  const params1 = {
    Bucket: bucketName,
    Key: folderName+'/'+decodeURIComponent(key[1]),
  };
  console.log(params1);

  s3.deleteObject(params1, (error, data) => {
    if (error) {
      console.log(error);
    }
  });
  return true;
}
exports.getFileStream = module.exports = {
  uploadFile,
  getFileStream,
  deleteFile,
  deleteFileByUrl,
  checkImage
};
