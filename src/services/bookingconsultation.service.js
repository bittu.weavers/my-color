const httpStatus = require('http-status');
const mongoose = require('mongoose');
const { bookingconsultation, bookingavailability } = require('../models');
const ApiError = require('../utils/ApiError');
const { userService } = require('./index');

const ITEMS_PER_PAGE = 5;
// Add Booking Consultation
const bookConsultationAdd = async(userBody) => {
    const bookConsultation = await bookingconsultation.create(userBody);
    return bookConsultation;
};

// Get Booking Slots By Date - Admin
const getBookingSlotsByDate = async(date) => {
    return bookingavailability.find({ date });
};

// Get Booking Slots Date Only
const getBookingSlotsDate = async() => {
    return bookingavailability.find();
};

// Add / Update  Booking Slots Data
const addBookingSlotData = async(bookingSlotsBody) => {
    const query = { userId: bookingSlotsBody.userId };
    const update = { $set: bookingSlotsBody};
    const options = { upsert: true };
    return bookingavailability.updateOne(query, update, options);
    /*const result = await getBookingSlotsByDate(bookingSlotsBody.date);
    if (!result[0]) {
        const createdData = await bookingavailability.create(bookingSlotsBody);
        return {
            results: createdData,
            message: 'Booking Slots Added Successfully',
        };
    }
    Object.assign(result[0], bookingSlotsBody);
    await result[0].save();
    return {
        results: result[0],
        message: 'Booking Slots Updated Successfully',
    }; */
};

// Get Booking Consultation By ID
const getBookingConsultationById = async(id) => {
    return bookingconsultation.findById(id);
};

// Get Availability Slots By ID
const getAvailabilitySlotsById = async(id) => {
    return bookingavailability.findById(id);
};

// Get Booking Consultation Past List
const bookConsultationPastList = async (user_id) => {
    return bookingconsultation.find({ userId: user_id, datetime: { $lt: new Date() } }).sort({ datetime: -1 }).populate('colouristId','fname lname email');
};
// Get Booking Consultation Future List
const bookConsultationFutureList = async(user_id) => {
    return bookingconsultation.find({ userId: user_id, datetime: { $gte: new Date() } }).sort({ datetime: -1 }).populate('colouristId','fname lname email');
};

// Get Booking Slots
const getBookingSlots = async(userId) => {
    return bookingavailability.findOne({ userId });
};

// Booking Consultation Details
const bookConsultationDetails = async(id) => {
    const aggrQry = [{
            $match: {
                _id: mongoose.Types.ObjectId(id),
            },
        },
        {
            $lookup: {
                from: 'transactions',
                localField: 'transaction_id',
                foreignField: '_id',
                as: 'trans',
            },
        },
        { $unwind: '$trans' },
        {
            $lookup: {
                from: 'users',
                localField: 'colouristId',
                foreignField: '_id',
                as: 'colourist',
            },
        },
        { $unwind: '$colourist' },
        {
            $lookup: {
                from: 'users',
                localField: 'userId',
                foreignField: '_id',
                as: 'user',
            },
        },
        { $unwind: '$user' },
        // define which fields are you want to fetch
        {
            $project: {
                _id: '$trans._id',
                amount: '$trans.amount',
                note: '$trans.note',
                type: '$trans.type',
                status: '$trans.status',
                paymentdate: '$trans.created_at',
                colourist: {  
                    fname: "$colourist.fname",
                    lname: "$colourist.lname",
                    email: "$colourist.email"
                },
                user: {   
                    fname: "$user.fname",           
                    lname: "$user.lname",
                    email: "$user.email"
                },
                datetime: 1,
                joinlink: 1,
                bookingstatus: 1,
            },
        },
        {
            $project: {
                _id: '$trans._id',
                amount: {
                    $ifNull: ['$amount', ''],
                },
                note: {
                    $ifNull: ['$note', ''],
                },
                type: {
                    $ifNull: ['$type', ''],
                },
                status: {
                    $ifNull: ['$status', ''],
                },
                colourist:{
                    $ifNull: ['$colourist', '']
                },
                user:{
                    $ifNull: ['$user', '']
                },
                paymentdate: {
                    $ifNull: ['$paymentdate', ''],
                },
                datetime: {
                    $ifNull: ['$datetime', ''],
                },
                joinlink: {
                    $ifNull: ['$joinlink', ''],
                },
                bookingstatus: {
                    $ifNull: ['$bookingstatus', ''],
                },
            },
        },
    ];
    const result = await bookingconsultation.aggregate(aggrQry);
    if (result) {
        return result[0];
    }
    throw new ApiError(httpStatus.NOT_FOUND, 'Booking consultation not found');
};

// Delete Booking Consultation
const bookConsultationDelete = async(id) => {
    const result = await getBookingConsultationById(id);
    if (!result) {
        throw new ApiError(httpStatus.NOT_FOUND, 'Booking consultation not found');
    }
    await result.remove();
    return result;
};

// Delete Availability Slots
const availabilityslotsDelete = async(id) => {
    const result = await getAvailabilitySlotsById(id);
    if (!result) {
        throw new ApiError(httpStatus.NOT_FOUND, 'Availability slots not found');
    }
    await result.remove();
    return result;
};

// Update Booking Consultation By ID
const bookConsultationUpdate = async(id, updateBody) => {
    const result = await getBookingConsultationById(id);
    if (!result) {
        throw new ApiError(httpStatus.NOT_FOUND, 'Booking consultation not found');
    }
    Object.assign(result, updateBody);
    await result.save();
    return result;
};
// Status Update Booking Consultation By ID
const bookConsultationStatusUpdate = async(id, updateBody) => {
    const result = await getBookingConsultationById(id);
    if (!result) {
        throw new ApiError(httpStatus.NOT_FOUND, 'Booking consultation not found');
    }
    Object.assign(result, updateBody);
    await result.save();
    return result;
};
//  Booking Consultation 
const getAllBookConsultation = async(currentPage) => {
    try {
        const totalItems = await bookingconsultation.find().countDocuments();
        const booking = await bookingconsultation
            .find()
            .sort({ datetime: -1 })
            .populate('userId', 'fname lname email')
            .populate('colouristId', 'fname lname')
            .skip((currentPage - 1) * ITEMS_PER_PAGE)
            .limit(ITEMS_PER_PAGE);
        const bookingList = {
            booking,
            page: currentPage,
            limit: ITEMS_PER_PAGE,
            totalPages: Math.ceil(totalItems / ITEMS_PER_PAGE),
            totalResults: totalItems,
        };
        return bookingList;
    } catch (err) {
        if (err) {
            return new ApiError(httpStatus.NOT_FOUND, 'Booking not found');
        }
    }
};

//  Booking Consultation By Colourist
const getAllColouristBookConsultation = async(currentPage,colouristId) => {
    try {
        const totalItems = await bookingconsultation.find().countDocuments();
        const booking = await bookingconsultation
            .find({colouristId})
            .sort({ datetime: -1 })
            .populate('userId', 'fname lname email')
            .populate('colouristId', 'fname lname')
            .skip((currentPage - 1) * ITEMS_PER_PAGE)
            .limit(ITEMS_PER_PAGE);
        const bookingList = {
            booking,
            page: currentPage,
            limit: ITEMS_PER_PAGE,
            totalPages: Math.ceil(totalItems / ITEMS_PER_PAGE),
            totalResults: totalItems,
        };
        return bookingList;
    } catch (err) {
        if (err) {
            return new ApiError(httpStatus.NOT_FOUND, 'Booking not found');
        }
    }
};
// Available Days For Booking
/* const getAvailabilityDays = async() => {
    const aggrQry = [
        {
          $lookup: {
            from: 'users',
            localField: 'userId',
            foreignField: '_id',
            as: 'user'
          },
        },
        { $unwind: '$user' },
        {
            $project: {
                name : { '$concat': ['$user.fname', ' ', '$user.lname'] },
                profileimage:'$user.profileimage',
                userId:1,
                availability: {
                    $filter: {
                        input: '$availability',
                        cond: {$eq: ['$$this.status', true]}
                    }
                },
            }
        },
        {
            $project: {
                name:1,
                profileimage: {
                    "$cond": {
                      "if": {
                        $eq: [
                          {
                            "$ifNull": [
                              "$profileimage",
                              ""
                            ]
                          },
                          ""
                        ]
                      },
                      "then": "https://i.stack.imgur.com/l60Hf.png",
                      "else": "$profileimage"
                    }
                },
                userId:1,
                availability:1
            }
        },
        { $unwind: '$availability' },
        { 
            $group : {
                _id: {
                    daynum: "$availability.daynum",
                },
                user: { $push: {image:"$profileimage",thumbImage:"$profileimage",title:"$name",userId:"$userId"} },
            }   
        },
        { 
            $group : {
                _id: "null",
                result:{
                    $push: {
                        daynum: "$_id.daynum",
                        user: "$user",
                    }
                }
            }
        }

    ];
    return  bookingavailability.aggregate(aggrQry);
} */

const getAvailabilityDays = async() => {
    const aggrQry = [
        {
          $lookup: {
            from: 'users',
            localField: 'userId',
            foreignField: '_id',
            as: 'user'
          },
        },
        { $unwind: '$user' },
        {
            $project: {
                name : { '$concat': ['$user.fname', ' ', '$user.lname'] },
                profileimage:'$user.profileimage',
                userId:1,
                availability: {
                    $filter: {
                        input: '$availability',
                        cond: {$eq: ['$$this.status', true]}
                    }
                },
            }
        },
        {
            $project: {
                name:1,
                profileimage: {
                    "$cond": {
                      "if": {
                        $eq: [
                          {
                            "$ifNull": [
                              "$profileimage",
                              ""
                            ]
                          },
                          ""
                        ]
                      },
                      "then": "https://i.stack.imgur.com/l60Hf.png",
                      "else": "$profileimage"
                    }
                },
                userId:1,
                availability:1
            }
        },
        { $unwind: '$availability' },
        { 
            $group : {
                _id: {
                    daynum: "$availability.daynum",
                },
                user: { $push: {image:"$profileimage",thumbImage:"$profileimage",title:"$name",userId:"$userId",timeslots:"$availability.timeslots",buffertime:"$availability.buffertime"} },
            }   
        },
        { 
            $sort : {
                "_id.daynum":1,
                
            }   
        },
        { 
            $group : {
                _id: "null",
                result:{
                    $push: {
                        daynum: "$_id.daynum",
                        user: "$user",
                    }
                }
            }
        }

    ];
    return  bookingavailability.aggregate(aggrQry);
}


// Available time slot of a colourist
const getAvailabilityTimeColourist = async(colouristId,dayNum) => {
    return bookingavailability.find({  userId:mongoose.Types.ObjectId(colouristId)},{
        availability: {
            $filter: {
                input: '$availability',
                cond: {$eq: ['$$this.daynum',dayNum]}
            }
        },
      })
};

module.exports = {
    bookConsultationAdd,
    bookConsultationDelete,
    bookConsultationPastList,
    bookConsultationFutureList,
    bookConsultationUpdate,
    bookConsultationDetails,
    bookConsultationStatusUpdate,
    getAllBookConsultation,
    addBookingSlotData,
    getBookingSlots,
    availabilityslotsDelete,
    getBookingSlotsDate,
    getBookingSlotsByDate,
    getAvailabilityDays,
    getAvailabilityTimeColourist,
    getAllColouristBookConsultation
};
