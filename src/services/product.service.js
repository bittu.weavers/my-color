const httpStatus = require('http-status');
const mongoose = require('mongoose');
const Product = require('../models/product.model');
const Review = require('../models/productreview.model');
const ApiError = require('../utils/ApiError');
const Category = require('../models/productcategory.model');
const userService = require('./user.service');
const tokenService = require('./token.service');
const atob=require('atob');
const ITEMS_PER_PAGE = 10;
const ITEMS_PER_PAGE_REVIEW = 5;
// Add Product
const addProductData = async (productBody) => {
  const productdata = await Product.create(productBody);
  return productdata;
};

// Add Product Review
const addReviewData = async (req) => {
  let productBody = req.body;
  const newReviewData = { ...productBody, user_id: req.user.id };
  const reviewdata = await Review.create(newReviewData);
  return reviewdata;
};
const checkUserCanSubmitReview = async (req) => {
  let user_id = req.user.id;
  let product = req.body.product_id;
  const reviewdata = await Review.find({ user_id, product_id: product });
  return reviewdata.length ? true : false;
};
// List Product
const getProductListData = async (currentPage) => {
  try {
    const totalItems = await Product.find().countDocuments();
    const products = await Product.find()
      .sort({ created_at: -1 })
      .populate('Product', 'title')
      .skip((currentPage - 1) * 8)
      .limit(8);

    const productList = {
      products,
      page: currentPage,
      limit: 8,
      totalPages: Math.ceil(totalItems / 8),
      totalResults: totalItems,
    };
    return productList;
  } catch (err) {
    if (err) {
      return new ApiError(httpStatus.NOT_FOUND, 'Product not found');
    }
  }
};

// List Product Review List
const getReviewListData = async (pid, currentPage) => {
  try {
    const totalItems = await Review.find({ product_id: pid }).countDocuments();
    const reviews = await Review.find({ product_id: pid })
      .sort({ created_at: -1 })
      .populate('Review', 'review_desc')
      .skip((currentPage - 1) * ITEMS_PER_PAGE)
      .limit(ITEMS_PER_PAGE);

    const reviewList = {
      reviews,
      page: currentPage,
      limit: ITEMS_PER_PAGE,
      totalPages: Math.ceil(totalItems / ITEMS_PER_PAGE),
      totalResults: totalItems,
    };
    return reviewList;
  } catch (err) {
    if (err) {
      return new ApiError(httpStatus.NOT_FOUND, 'Product review not found');
    }
  }
};

// List Product
const getAllProductListData = async () => {
  try {
    return Product.find({}, { title: 1 });
  } catch (err) {
    if (err) {
      return new ApiError(httpStatus.NOT_FOUND, 'Product not found');
    }
  }
};
// Get Product By ID
const getProductById = async (id) => {
  const product = Product.findById(id);
  if (!product) {
    return new ApiError(httpStatus.NOT_FOUND, 'Product not found');
  }
  return product;
};



// Review Detail By ID
const getReviewDetailsData = async (id) => {
  const aggrQry = [{
    $match: {
      _id: mongoose.Types.ObjectId(id),
    },
  },
  // Join with products table
  {
    $lookup: {
      from: 'products',
      localField: 'product_id',
      foreignField: '_id',
      as: 'products',
    },
  },
  { $unwind: '$products' },
  // Join with users table
  {
    $lookup: {
      from: 'users',
      localField: 'user_id',
      foreignField: '_id',
      as: 'users',
    },
  },
  { $unwind: '$users' },
  // define which fields are you want to fetch
  {
    $project: {
      _id: 1,
      review_desc: 1,
      rating_count: 1,
      is_approve: 1,
      created_at: 1,
      updated_at: 1,
      title: '$products.title',
      fname: '$users.fname',
      lname: '$users.lname',
    },
  },
  {
    $project: {
      _id: 1,
      review_desc: {
        $ifNull: ['$review_desc', ''],
      },
      rating_count: {
        $ifNull: ['$rating_count', ''],
      },
      is_approve: {
        $ifNull: ['$is_approve', ''],
      },
      created_at: {
        $ifNull: ['$created_at', ''],
      },
      updated_at: {
        $ifNull: ['$updated_at', ''],
      },
      title: {
        $ifNull: ['$title', ''],
      },
      fname: {
        $ifNull: ['$fname', ''],
      },
      lname: {
        $ifNull: ['$lname', ''],
      },
    },
  },
  ];
  const result = await Review.aggregate(aggrQry);
  if (result) {
    return result[0];
  }
  throw new ApiError(httpStatus.NOT_FOUND, 'Product review not found');
};

// Delete Product
const deleteProduct = async (id) => {
  const result = await getProductById(id);
  if (!result) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Product not found');
  }
  await result.remove();
  await Review.deleteMany({ product_id: id });
  return result;
};
const groupBy = (array, key) => {
  // Return the end result
  let groupData = array.reduce((result, currentValue) => {

    // If an array already present for key, push it to the array. Else create an array and push the object
    (
      result['variation.attribute.' + currentValue[key]] =
      result['variation.attribute.' + currentValue[key]] || []).push(
        currentValue.id
      );
    // Return the current iteration `result` value, this will be taken as next iteration `result` value and accumulate
    return result;
  }, {}); // empty object is the initial value for result object
  let blank = {}
  for (let key of Object.keys(groupData)) {
    blank[key] = { $in: groupData[key] }
  }
  console.log(blank);
  return blank;
};
// Product filter Dat
const getProductFilterData = async (currentPage, req) => {
  try {
    let body = req.body
    let userId= await tokenService.getUserIdFromToken(req)

    let filterobject = { "is_publish": "publish" };
    if (body.category && body.category.length) {
      let categoryobjectIds = body.category.map(x => {
        return mongoose.Types.ObjectId(x)
      })
      filterobject = { ...filterobject, 'categoryid.category_id': { $in: categoryobjectIds } };
    }
    if (body.attribute && body.attribute.length) {
      let groupData = groupBy(body.attribute, 'slug');
      for (let key of Object.keys(groupData)) {
        filterobject[key] = groupData[key];
      }
    }
    if (body.search && body.search != '') {
      filterobject = { ...filterobject, title: { $regex: body.search, $options: 'i' } };
    }
    if (body.maxPrice && body.maxPrice != '') {
      filterobject = { ...filterobject, price: { $gte: body.minPrice, $lte: body.maxPrice } };
    }
    let filterAggre = [{
      $match: filterobject
    },
    {
      $sort: {
        created_at:-1
      }
    },
    {
      $lookup: {
        from: "users",
        let: {
          userId: (userId !='') ? mongoose.Types.ObjectId(userId) : ''
        },
        pipeline: [{
          $match: {
            $expr: {
              $eq: [
                "$_id",
                "$$userId"
              ]
            }
          }
        },
        {
          $project: {
            wishlist: 1,
            productId: "$wishlist.productId"
          }
        },
        {
          $project: {
            productId: 1
          }
        },
        {
          $unwind: "$productId"
        },
        {
          "$group": {
            "_id": null,
            "productid": {
              "$push": "$productId"
            }
          }
        },
        {
          $unwind: "$productid"
        },
        ],
        as: "userWishlist"
      }
    }, {
      $addFields: {
        "wishlistproductIds": {
          "$map": {
            "input": "$userWishlist",
            "as": "val",
            "in": {
              $cond: [{
                $in: [
                  "$$val.label",
                  []
                ]
              },
              {
                label: "$$val.productid",
                value: {
                  $toDouble: "$$val.productid"
                }
              },
                "$$val.productid"
              ]
            }
          }
        }
      }
    },
    {
      $lookup: {
        from: 'reviews',
        pipeline: [
          { $match: { is_approve: true } }
        ],
        localField: '_id',
        foreignField: 'product_id',
        as: 'review'
      }
    }, {
      $project: {
        _id: 1,
        title: 1,
        slug: 1,
        short_desc: 1,
        price: 1,
        details: 1,
        ingredients: 1,
        weight: 1,
        type: 1,
        categoryid: 1,
        faq: 1,
        review: 1,
        product_image: 1,
        parrentAttrs: 1,
        min_price: 1,
        max_price: 1,
        variation: 1,
        reviewCount: { $size: "$review" },
        reviewAvgRaw: { $ceil: { $avg: "$review.rating_count" } },
        'all_childattr': {
          '$concatArrays': [
            '$variation.attribute'
          ]
        },
        isWishList: {
          $in: ["$_id", "$wishlistproductIds"]
        }
      }
    }, {
      $project: {
        _id: 1,
        title: 1,
        slug: 1,
        short_desc: 1,
        price: 1,
        details: 1,
        ingredients: 1,
        weight: 1,
        type: 1,
        categoryid: 1,
        faq: 1,
        review: 1,
        product_image: 1,
        parrentAttrs: 1,
        min_price: 1,
        max_price: 1,
        variation: 1,
        reviewCount: { $size: "$review" },
        reviewAvgRaw: { $ceil: { $avg: "$review.rating_count" } },
        convertObjectToarray: {
          "$map": {
            "input": "$all_childattr",
            "as": "allchildAttr",
            "in": {
              id: { $objectToArray: "$$allchildAttr" },
            }
          }
        },
        isWishList: 1
      }
    }, {
      $project: {
        _id: 1,
        title: 1,
        slug: 1,
        short_desc: 1,
        price: 1,
        details: 1,
        ingredients: 1,
        weight: 1,
        type: 1,
        categoryid: 1,
        faq: 1,
        review: 1,
        product_image: 1,
        parrentAttrs: 1,
        min_price: 1,
        max_price: 1,
        variation: 1,
        reviewCount: { $size: "$review" },
        reviewAvgRaw: { $ceil: { $avg: "$review.rating_count" } },
        allchildAttrids: {
          $reduce: {
            input: "$convertObjectToarray",
            initialValue: [],
            in: { $concatArrays: ["$$value", "$$this.id.v"] }
          }
        },
        isWishList: 1
      }
    }, {
      $addFields: {
        "childattrObjIds": {
          "$map": {
            "input": "$allchildAttrids",
            "as": "allchildAttr",
            "in": {
              childId: { $convert: { input: "$$allchildAttr", to: "objectId" } },
            }
          }
        }
      }
    }, {
      $lookup: {
        from: 'attributes',
        let: { "parrentAttrIds": "$childattrObjIds" },
        "pipeline": [
          {
            "$unwind": "$value"
          },

          { "$match": { "$expr": { "$in": ["$value._id", "$$parrentAttrIds.childId"] } } },
          {
            "$project": {
              _id: 1,
              name: 1,
              slug: 1,
              value: { $sum: 1 }
            }
          },
          {
            "$group": {
              _id: "$slug",
              total: { $sum: "$value" }
            }
          }
        ],
        "as": "attrGroup"
      }
    },
    {
      $facet: {
        data: [{
          $skip: (currentPage - 1) * 9
        },
        {
          $limit: 9
        }
        ],
        pageInfo: [{
          $group: {
            _id: null,
            count: {
              $sum: 1
            }
          }
        }]
      }

    },
    {
      $unwind: {
        path: '$pageInfo',
      }
    }
    ];
    //  const agg = Product.aggregate([{ $match: { 'categoryid.category_id': { $in: body.category } } }]);
    //const totalItems = await Product.find(filterobject).countDocuments();
    const products = await Product.aggregate(filterAggre);
    /* .sort({ created_at: -1 })
    .skip((currentPage - 1) * ITEMS_PER_PAGE)
    .limit(ITEMS_PER_PAGE); */
    let count = (products.length) ? products[0].pageInfo.count : 0;
    let totalResults = (products.length) ? products[0].pageInfo.count : 0;
    const productList = {
      filterobject,
      products: (products.length) ? products[0].data : [],
      page: currentPage,
      limit: 9,
      totalPages: Math.ceil(count / 9),
      totalResults: totalResults,
    };
    return productList;
  } catch (err) {
    if (err) {
      return new ApiError(httpStatus.NOT_FOUND, 'Product not found');
    }
  }
};
const productFilterlist = async () => {
  try {
    let getCategory = [{
      $lookup: {
        from: 'productcategories',
        localField: 'categoryid.category_id',
        foreignField: '_id',
        pipeline: [
          {
            $match: {
              is_active: true
            }
          },
          {
            "$project": {
              _id: 1,
              category_name: 1,
            }
          }
        ],
        as: 'category'
      }
    }, {
      $unwind: {
        path: "$category",
        preserveNullAndEmptyArrays: false
      }
    }, {
      $group: {
        _id: { id: "$category._id", category_name: "$category.category_name" },
      }
    }];
    const category = await Product.aggregate(getCategory);
    const minQuery = await Product.find({}, { price: 1 }).sort({ price: 1 }).limit(1);
    const maxQuery = await Product.find({}, { price: 1 }).sort({ price: -1 }).limit(1);
    let getAttrQuery = [{
      $project: {
        _id: 1,
        variation: 1,
        'all_childattr': {
          '$concatArrays': [
            '$variation.attribute'
          ]
        },

      }
    }, {
      $unwind: {
        path: "$all_childattr",
        preserveNullAndEmptyArrays: false
      }
    }, {
      $project: {
        spec: {
          $objectToArray: "$all_childattr"
        }
      }
    }, {
      $addFields: {
        "allttr": {
          "$map": {
            "input": "$spec",
            "as": "allchildAttr",
            "in": {
              id: { $convert: { input: "$$allchildAttr.v", to: "objectId" } },
            }
          }
        }
      }
    }, {
      $lookup: {
        from: 'attributes',
        let: { "parrentAttrIds": "$allttr" },
        "pipeline": [
          {
            "$unwind": "$value"
          },

          { "$match": { "$expr": { "$in": ["$value._id", "$$parrentAttrIds.id"] } } },
          {
            "$project": {
              _id: 1,
              slug: 1,
              value: 1
            }
          },
          {
            "$group": {
              _id: { attrName: "$slug", label: "$value.label", id: "$value._id" }
            }
          }
        ],
        "as": "attrGroup"
      }
    }, {
      $unwind: {
        path: "$attrGroup",
        preserveNullAndEmptyArrays: false
      }
    }, {
      $group: {
        _id: "$attrGroup._id.attrName",
        value: { $addToSet: { id: "$attrGroup._id.id", name: "$attrGroup._id.label" } }
      }
    }];
    const attrData = await Product.aggregate(getAttrQuery);
    return { category, minPrice: minQuery[0].price, maxPrice: maxQuery[0].price, attrData };
  } catch (err) {
    if (err) {
      return new ApiError(httpStatus.NOT_FOUND, 'Product not found');
    }
  }
};

const getReviewById = async (id) => {
  return Review.findById(id);
};

// Delete Product Review
const deleteReview = async (id) => {
  const result = await getReviewById(id);
  if (!result) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Product review not found');
  }
  await result.remove();
  return result;
};

// Review Status Update

const reviewStatusUpdate = async (id, updateBody) => {
  const result = await getReviewById(id);
  if (!result) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Product review not found');
  }
  Object.assign(result, updateBody);
  await result.save();
  return result;
};
const findProductBySlug = async (slug,req) => {
  try {
    let userId= await tokenService.getUserIdFromToken(req)
    const aggrQry = [{
      $match: {
        slug: slug,
        is_publish: "publish"
      }
    }, {
      $lookup: {
        from: 'reviews',
        pipeline: [{ $match: { is_approve: true } }],
        localField: '_id',
        foreignField: 'product_id',
        as: 'review',
      }
    },
    {
      $lookup: {
        from: "users",
        let: {
          userId: (userId !='') ? mongoose.Types.ObjectId(userId) : ''
        },
        pipeline: [{
          $match: {
            $expr: {
              $eq: [
                "$_id",
                "$$userId"
              ]
            }
          }
        },
        {
          $project: {
            wishlist: 1,
            productId: "$wishlist.productId"
          }
        },
        {
          $project: {
            productId: 1
          }
        },
        {
          $unwind: "$productId"
        },
        {
          "$group": {
            "_id": null,
            "productid": {
              "$push": "$productId"
            }
          }
        },
        {
          $unwind: "$productid"
        },
        ],
        as: "userWishlist"
      }
    }, {
      $addFields: {
        "wishlistproductIds": {
          "$map": {
            "input": "$userWishlist",
            "as": "val",
            "in": {
              $cond: [{
                $in: [
                  "$$val.label",
                  []
                ]
              },
              {
                label: "$$val.productid",
                value: {
                  $toDouble: "$$val.productid"
                }
              },
                "$$val.productid"
              ]
            }
          }
        }
      }
    }, {
      $project: {
        _id: 1,
        title: 1,
        slug: 1,
        short_desc: 1,
        price: 1,
        details: 1,
        ingredients: 1,
        weight: 1,
        type: 1,
        categoryid: 1,
        faqs: 1,
        review: 1,
        product_image: 1,
        parrentAttrs: 1,
        min_price: 1,
        max_price: 1,
        variation: 1,
        galleryImage: 1,
        reviewCount: { $size: "$review" },
        reviewAvgRaw: { $ceil: { $avg: "$review.rating_count" } },
        all_childattr: {
          '$concatArrays': [
            '$variation.attribute'
          ]
        },
        isWishList: {
          $in: ["$_id", "$wishlistproductIds"]
        }
      }
    }, {
      $addFields: {
        convertObjectToarray: {
          "$map": {
            "input": "$all_childattr",
            "as": "allchildAttr",
            "in": {
              id: { $objectToArray: "$$allchildAttr" },
            }
          }
        },
      }
    }, {
      $addFields: {
        allchildAttrids: {
          $reduce: {
            input: "$convertObjectToarray",
            initialValue: [],
            in: { $concatArrays: ["$$value", "$$this.id.v"] }
          }
        }
      }
    }, {
      $addFields: {
        "childattrObjIds": {
          "$map": {
            "input": "$allchildAttrids",
            "as": "allchildAttr",
            "in": {
              childId: { $convert: { input: "$$allchildAttr", to: "objectId" } },
            }
          }
        }
      }
    }, {
      $lookup: {
        from: 'attributes',
        let: { "parrentAttrIds": "$childattrObjIds" },
        "pipeline": [{
          "$unwind": "$value"
        },

        { "$match": { "$expr": { "$in": ["$value._id", "$$parrentAttrIds.childId"] } } },
        {
          "$project": {
            _id: 1,
            slug: 1,
            value: 1
          }
        },
        {
          $group: {
            _id: "$slug",
            value: { $push: "$value" }
          }
        }

        ],
        "as": "attrGroup"
      }
    }];
    const result = await Product.aggregate(aggrQry);
    return (result.length) ? result[0] : {};
  } catch (err) {
    if (err) {
      return new ApiError(httpStatus.NOT_FOUND, 'Team not found');
    }
  }
};
// List Product Review List
const getReviewListDataUser = async (pid, currentPage) => {
  try {
    const totalItems = await Review.find({ product_id: pid, is_approve: true }).countDocuments();
    const reviews = await Review.find({ product_id: pid, is_approve: true })
      .populate('user_id', 'fname lname image')
      .sort({ created_at: -1 })
      .skip((currentPage - 1) * ITEMS_PER_PAGE_REVIEW)
      .limit(ITEMS_PER_PAGE_REVIEW);
    const aggrQry = [{
      $match: {
        product_id: mongoose.Types.ObjectId(pid),
        is_approve: true,
      },
    },

    // define which fields are you want to fetch
    {
      $project: {
        _id: 1,
        rating_count: 1,
        user_id: 1,
        is_approve: 1,
      },
    },
    {
      $group: {
        _id: null,
        star_5_ratings: {
          $sum: {
            $cond: [{ $eq: ['$rating_count', 5] }, 1, 0],
          },
        },
        star_4_ratings: {
          $sum: {
            $cond: [{ $eq: ['$rating_count', 4] }, 1, 0],
          },
        },
        star_3_ratings: {
          $sum: {
            $cond: [{ $eq: ['$rating_count', 3] }, 1, 0],
          },
        },
        star_2_ratings: {
          $sum: {
            $cond: [{ $eq: ['$rating_count', 2] }, 1, 0],
          },
        },
        star_1_ratings: {
          $sum: {
            $cond: [{ $eq: ['$rating_count', 1] }, 1, 0],
          },
        },
        totalReview: {
          $sum: {
            $cond: [{ $eq: ['$product_id', mongoose.Types.ObjectId(pid)] }, 0, 1],
          },
        },
        reviewAvggroup: { $avg: '$rating_count' },
      },
    },
    {
      $project: {
        _id: 1,
        totalReview: 1,
        is_approve: 1,
        reviewAvg: { $ceil: '$reviewAvggroup' },
        star_5_ratings: 1,
        star_4_ratings: 1,
        star_3_ratings: 1,
        star_2_ratings: 1,
        star_1_ratings: 1,
      },
    },
    ];
    const reviewgroup = await Review.aggregate(aggrQry);
    const newgroupdata = { ...reviewgroup[0], reviewAvg: reviewgroup.length ? Math.ceil(reviewgroup[0]['reviewAvg']) : 0 };
    const reviewList = {
      reviewgroup,
      reviews,
      page: currentPage,
      limit: ITEMS_PER_PAGE_REVIEW,
      totalPages: Math.ceil(totalItems / ITEMS_PER_PAGE_REVIEW),
      totalResults: totalItems,
    };
    return reviewList;
  } catch (err) {
    if (err) {
      return new ApiError(httpStatus.NOT_FOUND, 'Product review not found');
    }
  }
};

const updateProductById = async (pid, updateBody) => {
  const result = await getProductById(pid);
  if (!result) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Product not found');
  }
  Object.assign(result, updateBody);
  await result.save();
  return result;
};


//arnab gupta

//get all product details by id
const getProductByArrayOfId = async (product) => {
  let updatedProdId = []
  product.map(val=>{
    updatedProdId.push(mongoose.Types.ObjectId(val))
  })
  const result = await Product.aggregate([
    {$match:{
      _id:{
        $in:updatedProdId
      }
    }}
  ])
  if (!result) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Cart Data Not Fount');
  }
  return result;
}

const getProductByArrayId = async (id) => {
  let allId = []
  id.forEach(val=>{
    allId.push(mongoose.Types.ObjectId(val.productId))
  })
  try {
    return await Product.aggregate([
      {
        $match:{
          _id:{
            $in:allId
          }
        }
      }
    ])
  } catch (error) {
    throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR,error)
  }
};

//get Products list for header search
const productListForHeaderSearch = async (req) => {
  const searchData = req.body.search;

  const productData = await Product.find(
    { title: { $regex: '.*' + searchData + '.*'} }).select("title slug").limit(15)
  return productData;
}
module.exports = {
  addProductData,
  getProductListData,
  getProductById,
  deleteProduct,
  getReviewListData,
  addReviewData,
  getReviewDetailsData,
  reviewStatusUpdate,
  deleteReview,
  getProductFilterData,
  productFilterlist,
  findProductBySlug,
  getReviewListDataUser,
  updateProductById,
  checkUserCanSubmitReview,
  getProductByArrayOfId,
  getProductByArrayId,
  getAllProductListData,
  productListForHeaderSearch
};
