const httpStatus = require('http-status');
const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const { User } = require('../models');
const ApiError = require('../utils/ApiError');
const { Token } = require('../models');

const ITEMS_PER_PAGE = 10;
/**
 * Create a user
 * @param {Object} userBody
 * @returns {Promise<User>}
 */
const createUser = async (userBody) => {
  if (await User.isEmailTaken(userBody.email)) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Email already taken');
  }
  console.log(userBody)
  const user = await User.create(userBody);
  return user;
};

/**
 * Query for users
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const queryUsers = async (filter, options) => {
  const users = await User.paginate(filter, options);
  return users;
};
/**
 * Get user by id
 * @param {ObjectId} id
 * @returns {Promise<User>}
 */
const getUserById = async (id) => {
  return User.findById(id);
};

/**
 * Get user by email
 * @param {string} email
 * @returns {Promise<User>}
 */
const getUserByEmail = async (email) => {
  return User.findOne({ email });
  // User.aggregate([{ $match: { email } }]);
};

/**
 * Get user by userid and role
 * @param {string} id
 *  @param {string} role
 * @returns {Promise<User>}
 */
const checkUserByIdAndRole = async (id, role) => {
  return User.findOne({ _id: id, role });
};

/**
 * Update user by id
 * @param {ObjectId} userId
 * @param {Object} updateBody
 * @returns {Promise<User>}
 */
const updateUserById = async (userId, updateBody) => {
  console.log(userId);
  console.log(updateBody);
  const user = await getUserById(userId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  if (updateBody.email && (await User.isEmailTaken(updateBody.email, userId))) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Email already taken');
  }
  Object.assign(user, updateBody);
  await user.save();
  return user;
};

/**
 * Delete user by id
 * @param {ObjectId} userId
 * @returns {Promise<User>}
 */
const deleteUserById = async (userId) => {
  const user = await getUserById(userId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  await user.remove();
  return user;
};
/**
 * change password
 * @param {string} email
 * @param {string} oldPassword
 * @param {string} newPassword
 * @returns {Promise}
 */
const changePassword = async (email, oldPassword, newPassword) => {
  const user = await getUserByEmail(email);
  if (user) {
    const isPassMatch = await bcrypt.compare(oldPassword, user.password);
    const samePassword = await bcrypt.compare(newPassword, user.password);
    if (isPassMatch) {
      if (!samePassword) {
        const changeUpdateUser = await updateUserById(user._id, {
          password: newPassword,
        });
        return changeUpdateUser;
      }
      throw new ApiError(httpStatus.FORBIDDEN, 'Your current password should not same as new password');
    }
    throw new ApiError(httpStatus.FORBIDDEN, 'Old password is incorrect, please try again');
  }
  throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
};

const addAddressById = async (userId, addressData) => {
  const user = await getUserById(userId);
  if (user) {
    return User.update({ _id: userId }, { $push: { address: addressData } });
  }
  throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
};

const getAddressByAddressUserID = async (userId, addressId) => {
  const user = await getUserById(userId);
  if (user) {
    const aggrQuery = [
      {
        $match: {
          _id: mongoose.Types.ObjectId(userId),
        },
      },
      {
        $unwind: '$address',
      },
      {
        $match: {
          'address._id': mongoose.Types.ObjectId(addressId),
        },
      },
      {
        $replaceRoot: {
          newRoot: '$address',
        },
      },
    ];
    const addressDetail = await User.aggregate(aggrQuery);
    return addressDetail[0];
  }
  throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
};

const updateAddressByUserId = async (userId, addressBody) => {
  const addressData = addressBody;
  const user = await getUserById(userId);
  const { addressId } = addressData;
  delete addressData.addressId;
  // delete addressData._id;
  if (user) {
    const userAddress = await getAddressByAddressUserID(userId, addressId);
    if (userAddress) {
      const obj2 = { _id: addressId };
      const newAddressData = { ...addressData, ...obj2 };
      return User.update({ _id: userId, 'address._id': addressId }, { $set: { 'address.$': newAddressData } });
    }
    throw new ApiError(httpStatus.NOT_FOUND, 'Address not found');
  }
  throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
};

const deleteAddressByUserId = async (userId, addressId) => {
  const user = await getUserById(userId);
  if (user) {
    const userAddress = await getAddressByAddressUserID(userId, addressId);
    if (userAddress) {
      return User.update({ _id: userId }, { $pull: { address: { _id: addressId } } });
    }
    throw new ApiError(httpStatus.NOT_FOUND, 'Address not found');
  }
  throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
};

const changeStatusAddressByUserId = async (userId, addressData) => {
  const user = await getUserById(userId);
  if (user) {
    const userAddress = await getAddressByAddressUserID(userId, addressData.addressId);
    if (userAddress) {
      await User.update({ _id: userId }, { $set: { 'address.$[].primaryStatus': false } }, { multi: true });
      return User.update(
        { _id: userId, 'address._id': addressData.addressId },
        { $set: { 'address.$.primaryStatus': addressData.primaryStatus } }
      );
    }
    throw new ApiError(httpStatus.NOT_FOUND, 'Address not found');
  }
  throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
};

const getUserBySocialId = async (socialId, userType) => {
  let qry = {};
  if (userType === 'facebook') {
    qry = {
      facebookId: socialId,
    };
  }
  if (userType === 'google') {
    qry = {
      googleId: socialId,
    };
  }
  return User.findOne(qry);
};

// Delete Social Account
const deleteSocialUserAccount = async (socialId, userType) => {
  const result = await getUserBySocialId(socialId, userType);
  if (!result) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Social Account not found');
  }
  await result.remove();
  return result;
};
/**
 * Get user by token
 * @param {string} token
 * @returns {Promise<Token>}
 */
const getUserByToken = async (token) => {
  const tokenDoc = await Token.findOne({ token });
  if (!tokenDoc) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Invaild Token');
  }
  return tokenDoc.user;
};
const getUserObjByToken = async (token) => {
  const tokenDoc = await Token.findOne({ token });
  if (!tokenDoc) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Invaild Token');
  }
  const userObj = await getUserById(tokenDoc.user);
  return userObj;
};
// Get Users List
const getAllUsersList = async (currentPage, searchData) => {
  try {
    const filterObj = {};
    if (searchData.role !== '') {
      filterObj.role = searchData.role;
    }
    if (searchData.search !== '') {
      filterObj.$or = [
        { fname: { $regex: searchData.search, $options: 'i' } },
        { lname: { $regex: searchData.search, $options: 'i' } },
        { email: { $regex: searchData.search, $options: 'i' } },
      ];
    }
    const totalItems = await User.find(filterObj).countDocuments();
    const users = await User.find(filterObj)
      .sort({ createdAt: -1 })
      .skip((currentPage - 1) * ITEMS_PER_PAGE)
      .limit(ITEMS_PER_PAGE);
    const usersList = {
      users,
      page: currentPage,
      limit: ITEMS_PER_PAGE,
      totalPages: Math.ceil(totalItems / ITEMS_PER_PAGE),
      totalResults: totalItems,
    };
    return usersList;
  } catch (err) {
    if (err) {
      return new ApiError(httpStatus.NOT_FOUND, 'Users not found');
    }
  }
};

module.exports = {
  createUser,
  queryUsers,
  getUserById,
  getUserByEmail,
  updateUserById,
  deleteUserById,
  changePassword,
  addAddressById,
  getAddressByAddressUserID,
  updateAddressByUserId,
  deleteAddressByUserId,
  changeStatusAddressByUserId,
  getUserBySocialId,
  checkUserByIdAndRole,
  deleteSocialUserAccount,
  getUserByToken,
  getUserObjByToken,
  getAllUsersList,
};
