const httpStatus = require('http-status');
const Page = require('../models/page.model');
const ApiError = require('../utils/ApiError');
const { userService } = require('./index');
const S3Service=require('./s3.service');
const mongoose = require('mongoose');
const ITEMS_PER_PAGE = 50;
/**
 * Create a Page
 * @param {Object} pageReq
 * @returns {Promise<Blog>}
 */
const createPage = async (pageReq, token) => {
  const userId = await userService.getUserByToken(token);
  const pageBody = pageReq.body;
  const finelObj = { ...pageBody, user: userId };
  const page = await Page.create(finelObj);
  return page;
};
const getPages = async (currentPage) => {
  try {
    const totalItems = await Page.find().countDocuments();
    const pages = await Page.find()
      .sort({ created_at: -1 })
      .populate('user', 'name')
      .skip((currentPage - 1) * ITEMS_PER_PAGE)
      .limit(ITEMS_PER_PAGE);

    return {
      pages,
      page: currentPage,
      limit: ITEMS_PER_PAGE,
      totalPages: Math.ceil(totalItems / ITEMS_PER_PAGE),
      totalResults: totalItems,
    };
  } catch (err) {
    if (err) {
      throw err;
    }
  }
};
const findPageBySlug = async (slug) => {
  const page = await Page.findOne({ slug }).populate('user', 'name');
  if (!page) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Page not found');
  }
  return page;
};
const findPageById = async (id) => {
  const page = await Page.findById(id).populate('user', 'name');
  if (!page) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Page not found');
  }
  return page;
};
const editPage = async (id) => {
  const page = await findPageById(id);
  if (!page) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Page not found');
  }
  return page;
};
const deletePage = async (id) => {
  const page = await findPageById(id);
  page.remove();
  return page;
};
const updatePage = async (id, token, request) => {
  const query = { _id: id };
  const valueUpdate = { $set: request.body };
  await Page.updateOne(query, valueUpdate, async function (err, result) {
    if (err) {
      throw new ApiError(httpStatus.BAD_REQUEST, 'Unable to process this request');
    }
  });
  const page = await findPageById(id);
  return page;
};
const findBySectionId = async (sectionId) => {
  const section = await Page.findOne({ 'content.sectionId': sectionId }, { 'content.$': 1 });
  if (!section) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Section not found');
  }
  return section.content[0];
};
const removeImage = async (sectionId,body) => {

  await S3Service.deleteFileByUrl(body.imageUrl,'imageUrl');

  const section = await Page.te({ 'content.sectionId':sectionId },
  {
    $set: {
      "content.$.imageUrl":''
    }
  }
  );
  if (!section) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Section not found');
  }
  return section.content;
};
const removeBlockImage = async (blockId,pageId,body) => {

  await S3Service.deleteFileByUrl(body.imageUrl,'imageUrl');
  const section = await Page.update({ _id:mongoose.Types.ObjectId(pageId) },
  {
    $set: {
      "content.$[].block.$[e].imageUrl":''
    }
  },
  {
    arrayFilters:[{"e._id":mongoose.Types.ObjectId(blockId)}]
  }
  );
  //const section =await Page.findOne({_id:mongoose.Types.ObjectId("6298a238b83967363a480076"),'content.block._id':sectionId})
  if (!section) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Section not found');
  }
  return section;
};
const removeBlock = async (pageId,blockId,body) => {

await S3Service.deleteFileByUrl(body.imageUrl,'imageUrl');
 const section = await Page.update({ _id:mongoose.Types.ObjectId(pageId) },
 { $pull: { "content.$[].block": { "_id": mongoose.Types.ObjectId(blockId)} } },
  { new: true }
 );
 //{ $pull: { likedTours: { _id: docs._id } } }
  if (!section) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Block  not found');
  }
  return section.content;
};
module.exports = {
  createPage,
  getPages,
  findPageBySlug,
  deletePage,
  findPageById,
  editPage,
  updatePage,
  findBySectionId,
  removeImage,
  removeBlock,
  removeBlockImage
};
