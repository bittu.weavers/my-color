const httpStatus = require('http-status');
const ApiError = require('../utils/ApiError');
const stripeService = require('./stripe.service');

const createCard = async(token,customerId) => {
    const card = await stripeService.createCard(token,customerId);
    return card;
}

const getCardListData = async(customerId) => {
    const cardList = await stripeService.getCardListStripe(customerId);
    const customer = await stripeService.getCustomerStripe(customerId);
    cardList['default_source'] = customer.default_source;
    return cardList;
}

const deleteCardData = async(cardId,customerId) => {
    return stripeService.deleteCardtStripe(customerId,cardId);
}

const defaultCardData = async(cardId,customerId) => {
    return stripeService.defaultCardtStripe(customerId,cardId);
}

module.exports = {
    createCard,
    getCardListData,
    deleteCardData,
    defaultCardData
};