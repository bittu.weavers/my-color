const httpStatus = require('http-status');
const ApiError = require('../utils/ApiError');
const config = require('../config/config');
const stripe = require('stripe')(config.stripe_secret_key);

const getCustomerStripe = async (customerId) => {
  try {
    return stripe.customers.retrieve(customerId);
  } catch (err) {
    if (err) {
      throw new ApiError(httpStatus['207_MESSAGE'], err);
    }
  }
};

const create_refund = async (data) => {
  try {
    if (data.amount) {
      return stripe.refunds.create({
        charge: data.charge,
        amount: data.amount,
      });
    } else {
      return stripe.refunds.create({
        charge: data.charge,
      });
    }
  } catch (error) {
    throw new ApiError(httpStatus['500_MESSAGE'], err);
  }
};

// Add plan
const createCustomerStripe = async (customerData) => {
  try {
    const customer = await stripe.customers.create({
      email: customerData.email,
      name: customerData.fname + ' ' + customerData.lname,
    });
    return customer.id;
  } catch (err) {
    if (err) {
      throw new ApiError(httpStatus['207_MESSAGE'], err);
    }
  }
};

const create_charge = async (data) => {
  try {
    return stripe.charges.create(data);
  } catch (err) {
    if (err) {
      throw new ApiError(httpStatus['207_MESSAGE'], err);
    }
  }
};

// Create Card
const createCard = async (token, cusomerId) => {
  try {
    const card = await stripe.customers.createSource(cusomerId, { source: token });
    return card.id;
  } catch (err) {
    if (err) {
      throw new ApiError(httpStatus['207_MESSAGE'], err);
    }
  }
};

// Card List
const getCardListStripe = async (customerId) => {
  try {
    return stripe.customers.listSources(customerId, { object: 'card' });
  } catch (err) {
    if (err) {
      throw new ApiError(httpStatus['207_MESSAGE'], err);
    }
  }
};
// Card Delete
const deleteCardtStripe = async (customerId, cardId) => {
  try {
    return stripe.customers.deleteSource(customerId, cardId);
  } catch (err) {
    if (err) {
      throw new ApiError(httpStatus['207_MESSAGE'], err);
    }
  }
};
// Default Card Set
const defaultCardtStripe = async (customerId, cardId) => {
  try {
    return stripe.customers.update(customerId, { default_source: cardId });
  } catch (err) {
    if (err) {
      throw new ApiError(httpStatus['207_MESSAGE'], err);
    }
  }
};

// Subscription Detail
const subscriptionDetailStripe = async (subId) => {
  try {
    return stripe.subscriptions.retrieve(subId);
  } catch (err) {
    if (err) {
      throw new ApiError(httpStatus['207_MESSAGE'], err);
    }
  }
};

module.exports.create_charge = create_charge;
module.exports.createCustomerStripe = createCustomerStripe;
module.exports.createCard = createCard;
module.exports.getCardListStripe = getCardListStripe;
module.exports.deleteCardtStripe = deleteCardtStripe;
module.exports.defaultCardtStripe = defaultCardtStripe;
module.exports.getCustomerStripe = getCustomerStripe;
module.exports.subscriptionDetailStripe = subscriptionDetailStripe;
module.exports.create_refund = create_refund;
