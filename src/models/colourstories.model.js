const mongoose = require('mongoose');
const { toJSON, paginate } = require('./plugins');

const colourStoriesSchema = mongoose.Schema({
    image: {
        type: String,
    },
    description: {
        type: String,
    },
    rating_count: {
        type: Number,
    },
    name: {
        type: String,
    },
    since: {
        type: String,
    },
}, { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } });



colourStoriesSchema.plugin(toJSON);
colourStoriesSchema.plugin(paginate);


const colour = mongoose.model('colourstories', colourStoriesSchema);

module.exports = colour;
