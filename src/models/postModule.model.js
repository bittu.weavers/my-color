const mongoose = require('mongoose');
const slug = require('mongoose-slug-generator');
const { toJSON, paginate } = require('./plugins');

mongoose.plugin(slug);
const { Schema } = mongoose;
const postSchema = mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
      trim: true,
      default: '',
    },
    slug: {
      type: String,
      slug: 'title',
      unique: true,
    },
    description: {
      type: String,
      trim: true,
      default: '',
    },
    sub_title: {
      type: String,
      trim: true,
      default: '',
    },
    short_description: {
      type: String,
      trim: true,
      default: '',
    },
    image: {
      type: String,
      trim: true,
      default: '',
    },
    post_type: {
      type: String,
      required: true,
      trim: true,
      default: '',
    },
    is_feature: {
      type: Boolean,
      enum: [true, false],
      default: false,
    },
    is_active: {
      type: Boolean,
      enum: [true, false],
      default: true,
    },
    user: {
      type: Schema.Types.ObjectId,
      ref: 'User',
      required: true,
    },
  },
  { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
);

// add plugin that converts mongoose to json
postSchema.plugin(toJSON);
postSchema.plugin(paginate);

/**
 * @typedef Videopost
 */
const Videopost = mongoose.model('Post', postSchema);

module.exports = Videopost;
