const mongoose = require('mongoose');
const { toJSON, paginate } = require('./plugins');

const productcategorySchema = mongoose.Schema(
  {
    category_name: {
      type: String,
      required: true,
      trim: true,
      default: '',
    },
    product_count: {
      type: Number,
      required: true,
      default: 0,
    },
    is_active: {
      type: Boolean,
      enum: [true, false],
      default: true,
    },
  },
  { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
);

// add plugin that converts mongoose to json
productcategorySchema.plugin(toJSON);
productcategorySchema.plugin(paginate);

/**
 * @typedef Productcategory
 */
const Productcategory = mongoose.model('Productcategory', productcategorySchema);

module.exports = Productcategory;
