const mongoose = require('mongoose');
const { toJSON, paginate } = require('./plugins');

const videopostSchema = mongoose.Schema(
  {
    video_title: {
      type: String,
      required: true,
      trim: true,
      default: '',
    },
    video_description: {
      type: String,
      trim: true,
      default: '',
    },
    video_image: {
      type: String,
      required: true,
      trim: true,
      default: '',
    },
    video_url: {
      type: String,
      required: true,
      trim: true,
      default: '',
    },
    is_feature: {
      type: Boolean,
      enum: [true, false],
      default: false,
    },
    is_active: {
      type: Boolean,
      enum: [true, false],
      default: true,
    },
  },
  {
    timestamps: true,
  }
);

// add plugin that converts mongoose to json
videopostSchema.plugin(toJSON);
videopostSchema.plugin(paginate);

/**
 * @typedef Videopost
 */
const Videopost = mongoose.model('Videopost', videopostSchema);

module.exports = Videopost;
