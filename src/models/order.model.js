const mongoose = require('mongoose');
const { toJSON, paginate } = require('./plugins');
const orderSchema = mongoose.Schema(
    {
        userId: {
            type: mongoose.SchemaTypes.ObjectId,
            ref: 'User',
            required: false,
        },
        transaction_id:mongoose.SchemaTypes.ObjectId,
        orderId:String,
        addressType:mongoose.SchemaTypes.ObjectId,
        orderStatus:String,
        product: [
          {
            title:String,
            attr:String,
            quantity: Number,
            short_desc:String,
            image:String,
            price:Number
          }
        ],
        payment:{
          totalPrice:Number,
          discount:Number,
          subtotal:Number,
          tax:Number,
          delivery:Number,
          finalamt:Number
        },
        totalAmount: Number,
        refundAmount:Number
    },
    {
        timestamps: true,
    }
);

// add plugin that converts mongoose to json
orderSchema.plugin(toJSON);
orderSchema.plugin(paginate);

const Order = mongoose.model('Order', orderSchema);

module.exports = Order;
