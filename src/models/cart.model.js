const mongoose = require('mongoose');
const { toJSON, paginate } = require('./plugins');
const cartSchema = mongoose.Schema(
    {
        userId: {
            type: mongoose.SchemaTypes.ObjectId,
            ref: 'User',
            required: false,
        },
        items: [
          {
            productId: {
              type: mongoose.SchemaTypes.ObjectId,
              ref: 'Product',
              required: true,
            },
            variableProductId: {
              type: mongoose.SchemaTypes.ObjectId,
              ref: 'Product.variation',
              required: false,
            },
            quantity: Number
          }
        ],
        shipping: {
          shippingType: String,
          amount:Number
        },
        coupon: {
          couponCode: String,
          id:mongoose.SchemaTypes.ObjectId,
          discountType: String,
          discountAmount: Number
        },
        tax: Number,
        subTotal: Number,
        Total: Number
    },
    {
        timestamps: true,
    }
);

// add plugin that converts mongoose to json
cartSchema.plugin(toJSON);
cartSchema.plugin(paginate);
/**
 * @typedef Product
 */
 cartSchema.methods.addToCart = async function(product) {
  // 'this' keyword still refers to the schema


  const cartProductIndex = this.items.findIndex(cp => {
      return cp.productId.toString() === product._id.toString();
  });
  let newQuantity = 1;
  const updatedCartItems = [...this.items];

  if (cartProductIndex >= 0) {
      newQuantity = this.items[cartProductIndex].quantity + 1;
      updatedCartItems[cartProductIndex].quantity = newQuantity;
  } else {
      updatedCartItems.push({
          productId: product._id,
          quantity: newQuantity
      });
  }
   this.subTotal=this.subTotal+product.price;
  this.Total=this.Total+product.price;
  this.tax=0;
  this.coupon= {
    couponCode: "",
    discountType: "",
    discountAmount: 0
  }
  this.shipping= {
    shippingType: "",
    amount:0
  }
  this.items = updatedCartItems;
  return this.save();
};

cartSchema.methods.removeFromCart =async function(productId) {
  const updatedCartItems = this.cart.items.filter(item => {
      return item.productId.toString() !== productId.toString();
  });
  this.cart.items = updatedCartItems;
  return this.save();
};

cartSchema.methods.clearCart =async function() {
  this.cart = { items: [] };
  return this.save();
};
const Cart = mongoose.model('Cart', cartSchema);

module.exports = Cart;
