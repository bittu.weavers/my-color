const mongoose = require('mongoose');
const { toJSON, paginate } = require('./plugins');

const wishlistSchema = mongoose.Schema(
  {
    userId: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: 'User',
      required: true,
    },

    productId: {
      type: mongoose.SchemaTypes.ObjectId,
      // ref: 'Product',
      ref: 'Productcategory',
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

// add plugin that converts mongoose to json //
wishlistSchema.plugin(toJSON);
wishlistSchema.plugin(paginate);
/**
 * @typedef Tag
 */
const wishlist = mongoose.model('wishlist', wishlistSchema);

module.exports = wishlist;
