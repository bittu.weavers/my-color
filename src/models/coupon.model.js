const mongoose = require('mongoose');
const { toJSON, paginate } = require('./plugins');

const couponSchema = mongoose.Schema(
  {
    coupon_code: {
      type: String,
      required: true,
      unique:true,
      trim: true,
    },
    coupon_start_date: {
      type: Date,
      required: true,
    },
    coupon_expiry_date: {
      type: Date,
    },
    coupon_amount: {
      type: Number,
      default: 0,
    },
    coupon_percentage_value: {
      type: Number,
      default: 0,
    },
    is_percentage: {
      type: Boolean,
      enum: [true, false],
      default: false,
    },
    coupon_limit: {
      type: Number,
      default: 0,
    },
    product_for: {
      type: [mongoose.Types.ObjectId],
      ref: 'Product',
    },
  },
  {
    timestamps: true,
  }
);

// add plugin that converts mongoose to json
couponSchema.plugin(toJSON);
couponSchema.plugin(paginate);

/**
 * @typedef Productreview
 */
const coupons = mongoose.model('coupon', couponSchema);

module.exports = coupons;
