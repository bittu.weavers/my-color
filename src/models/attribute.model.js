const mongoose = require('mongoose');
const slug = require('mongoose-slug-generator');
const { deleteFileByUrl } = require('../services/s3.service');
const { toJSON, paginate } = require('./plugins');

const attrsValue = mongoose.Schema({
  label: {
    type: String,
  },
  
  unit_value: {
    type: String,
  },
  attrimage: {
    type: String,
    default: '',
  },
});

const AttributeSchema = mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      trim: true,
    },
    slug: {
      type: String,
      slug: 'name',
      unique: true,
    },
    value: [attrsValue],
  },
  { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
);

// add plugin that converts mongoose to json
AttributeSchema.plugin(toJSON);
AttributeSchema.plugin(paginate);
/**
 * @typedef Attribute
 */
 
AttributeSchema.post('remove', function (doc) {
  try {
    doc['value'].map(async(element,index) => {
      if(element.attrimage){
        deleteFileByUrl(element.attrimage,'attrimage');
      }
    });
  } catch(e) {
    console.error(`[error] ${e}`);
    throw Error('Error occurred while deleting Attribute');
  }
});
AttributeSchema.post('deleteOne', function (doc) {
  try {
    doc['value'].map(async(element,index) => {
      if(element.attrimage){
        deleteFileByUrl(element.attrimage,'attrimage');
      }
    });
  } catch(e) {
    console.error(`[error] ${e}`);
    throw Error('Error occurred while deleting Attribute');
  }
});
  
const Attribute = mongoose.model('attribute', AttributeSchema);

module.exports = Attribute;
