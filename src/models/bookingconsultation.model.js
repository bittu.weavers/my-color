const mongoose = require('mongoose');
const validator = require('validator');
const { toJSON, paginate } = require('./plugins');
const { bookingStatus } = require('../config/constant');
const userService = require("../services/user.service");
const config = require("../config/config");
const mailer = require('../utils/mailer');



const bookingconsultationSchema = mongoose.Schema(
  {
    userId: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: 'User',
      required: true,
    },
    colouristId: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: 'User',
      required: true,
    },
    bookingstatus: {
      type: String,
      enum: bookingStatus,
      default: 'Pending',
    },
    datetime: {
      type: Date,
      required: true,
    },
    joinlink: {
      type: String,
      trim: true,
    },
    transaction_id: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: 'transactions',
      trim: true,
    },
  },
  {
    timestamps: true,
  }
);


const makeDate = (datetime) => {
  const str = new Date(datetime).toLocaleString('en-US', {
    hour: 'numeric',
    minute: 'numeric',
    hour12: true
  })
  const allMonth = ['Jan','Feb','Mar','Apr',"May",'June',"July",'Agu','Sep','Oct','Nov','Dec']
  const day = new Date(datetime).getDate();
  const month = allMonth[new Date(datetime).getMonth()]
  const year = new Date(datetime).getFullYear();
  const fullDateAndTime = `${day} ${month} ${year}, ${str}`
  return fullDateAndTime
}
// add plugin that converts mongoose to json //
bookingconsultationSchema.plugin(toJSON);
bookingconsultationSchema.plugin(paginate);

bookingconsultationSchema.pre('save', async function (next) {
  const bookingconsultation = this;
  const datetosend = makeDate(bookingconsultation.datetime)
  try {
    const user = await userService.getUserById(bookingconsultation.userId)
    if (bookingconsultation.isModified('joinlink')) {
      const welcomeMailDetails = {
        to: user.email,
        from: config.email.from,
        subject: "Booking Details",
        templateId: 'd-75967a3beac44798865be6d0a5186720',
        dynamicTemplateData: {
          name: `${user.fname} ${user.lname}`,
          datetime: datetosend,
          link: bookingconsultation.joinlink
        },
      };
      await mailer.sendEmail(welcomeMailDetails);
    } else {
      next();
    }
  } catch (error) {
    console.log(error)
  }
});
/**
 * @typedef Tag
 */
const bookingconsultation = mongoose.model('bookingconsultation', bookingconsultationSchema);

module.exports = bookingconsultation;

