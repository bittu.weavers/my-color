const mongoose = require('mongoose');
const validator = require('validator');
const bcrypt = require('bcryptjs');
const { toJSON, paginate } = require('./plugins');
const { roles } = require('../config/roles');
const { userType, addressType } = require('../config/constant');
const wishlist = mongoose.Schema({
  productId:{
    type: mongoose.SchemaTypes.ObjectId,
     ref: 'Product',
    required: true,
  }
},
{ timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
);
const userSchema = mongoose.Schema(
  {
    fname: {
      type: String,
      required: true,
      trim: true,
    },
    lname: {
      type: String,
      required: true,
      trim: true,
    },
    email: {
      type: String,
      required: true,
      unique: true,
      trim: true,
      lowercase: true,
      validate(value) {
        if (!validator.isEmail(value)) {
          throw new Error('Invalid email');
        }
      },
    },
    stripe_id: {
      type: String,
    },
    password: {
      type: String,
      required: true,
      trim: true,
      minlength: 8,
      validate(value) {
        if (!value.match(/\d/) || !value.match(/[a-zA-Z]/)) {
          throw new Error('Password must contain at least one letter and one number');
        }
      },
      private: true, // used by the toJSON plugin
    },
    role: {
      type: String,
      enum: roles,
      default: 'user',
    },
    usertype: {
      type: String,
      enum: userType,
      default: 'email',
    },
    facebookId: {
      type: String,
      default: '',
    },
    googleId: {
      type: String,
      default: '',
    },
    profileimage: {
      type: String,
      default: '',
    },
    isLoggedIn: {
      type: Boolean,
      enum: [true, false],
      default: false,
    },
    address: {
      type: [
        {
          firstname: {
            type: String,
          },
          lastname: {
            type: String,
          },
          addressline1: {
            type: String,
          },
          addressline2: {
            type: String,
          },
          city: {
            type: String,
          },
          zipcode: {
            type: String,
          },
          state: {
            type: String,
          },
          country: {
            type: String,
          },
          countrycode: {
            type: String,
          },
          phonecode: {
            type: String,
          },
          phone: {
            type: String,
          },
          addEmail: {
            type: String,
          },
          addType: {
            type: String,
            enum: addressType,
            default: 'home',
          },
          primaryStatus: {
            type: Boolean,
            enum: [true, false],
            default: false,
          },
        },
      ],
      default: [],
    },
    wishlist:[wishlist]
  },
  {
    timestamps: true,
  }
);

// add plugin that converts mongoose to json
userSchema.plugin(toJSON);
userSchema.plugin(paginate);

/**
 * Check if email is taken
 * @param {string} email - The user's email
 * @param {ObjectId} [excludeUserId] - The id of the user to be excluded
 * @returns {Promise<boolean>}
 */
userSchema.statics.isEmailTaken = async function (email, excludeUserId) {
  const user = await this.findOne({ email, _id: { $ne: excludeUserId } });
  return !!user;
};

/**
 * Check if password matches the user's password
 * @param {string} password
 * @returns {Promise<boolean>}
 */
userSchema.methods.isPasswordMatch = async function (password) {
  const user = this;
  return bcrypt.compare(password, user.password);
};

userSchema.pre('save', async function (next) {
  const user = this;
  if (user.isModified('password')) {
    user.password = await bcrypt.hash(user.password, 8);
  }
  next();
});

/**
 * @typedef User
 */
const User = mongoose.model('User', userSchema);

module.exports = User;
