const mongoose = require('mongoose');
const slug = require('mongoose-slug-generator');
const { deleteFileByUrl,checkImage } = require('../services/s3.service');
const { toJSON, paginate } = require('./plugins');
const { productStatusType, productType } = require('../config/constant');

mongoose.plugin(slug);
const { Schema } = mongoose;

const image = mongoose.Schema({
  product_image: {
    type: String,
  },
});


const faq = mongoose.Schema({
  question: {
    type: String,
  },
  answer: {
    type: String,
  },
});

const variationData = mongoose.Schema({
  regular_price: {
    type: Number,
    default: 0,
  },
  sell_price: {
    type: Number,
     default: 0,
  },
  price: {
    type: Number,
    default: 0,
  },
  sku: {
    type: String,
    default: '',
  },
  stock: {
    type: Number,
    default: '',
  },
  remaining_stock: {
    type: Number,
    default: '',
  },
  product_image: {
    type: String,
    default: '',
  },
  var_shipping_weight: {
    type: String,
  },
  var_shipping_length: {
    type: String,
  },
  var_shipping_width: {
    type: String,
  },
  var_shipping_height: {
    type: String,
  },
  variation_description: {
    type: String,
  },
  galleryImage:[image],
  attribute: {},
});

const ProductSchema = mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
      trim: true,
    },
    slug: {
      type: String,
      slug: 'title',
      unique: true,
    },
    short_desc: {
      type: String,
      default: '',
    },
    regular_price: {
      type: Number,
       default: 0,
    },
    sell_price: {
      type: Number,
       default: 0,
    },
    price: {
      type: Number,
      default: 0,
    },
    min_price: {
      type: Number,
      default: 0,
    },
    max_price: {
      type: Number,
      default: 0,
    },
    details: {
      type: String,
      default: '',
    },
    ingredients: {
      type: String,
      default: '',
    },
    sku: {
      type: String,
      default: '',
    },
    stock: {
      type: Number,
      default: '',
    },
    remaining_stock: {
      type: Number,
      default: '',
    },
    is_publish: {
      type: String,
      enum: productStatusType,
      default: 'publish',
    },
    type: {
      type: String,
      enum: productType,
      default: 'simple',
    },
    product_image: {
      type: String,
      default: '',
    },
    galleryImage:[image],
    categoryid: [
      {
        category_id: {
          type: Schema.Types.ObjectId,
          ref: 'Productcategory',
        },
      },
    ],
    shipping: {
      weight: {
        type: String,
      },
      length: {
        type: String,
      },
      width: {
        type: String,
      },
      height: {
        type: String,
      },
    },
    faqs: [faq],
    parrentAttrIds: [],
    variation: [variationData],
  },
  { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
);




// add plugin that converts mongoose to json
ProductSchema.plugin(toJSON);
ProductSchema.plugin(paginate);
/**
 * @typedef Product
 */

ProductSchema.pre('save', async function (next) {
  const Product = this;
  let variation = Product.variation
  if (variation.length) {
    for (var i = 0; i < variation.length; i++) {
      if (variation[i].sell_price) {
          variation[i].price = variation[i].sell_price
      } else {
          variation[i].price = variation[i].regular_price
      }
    }
    var minPrice = Math.min(...variation.map(item => item.price));
    if (minPrice) {
      Product.min_price = minPrice;
    }
    var maxPrice = Math.max(...variation.map(item => item.price));
    if (maxPrice) {
      Product.max_price = maxPrice;
      Product.price = maxPrice;
    }
  }
  next();
});

ProductSchema.post('remove', async function (doc) {
  await checkImage(doc.toJSON(),'product_image');
});
ProductSchema.post('deleteOne', async function (doc) {
  await checkImage(doc.toJSON(),'product_image');
});
const Product = mongoose.model('Product', ProductSchema);

module.exports = Product;
