const mongoose = require('mongoose');
const { toJSON, paginate } = require('./plugins');

const availabilitySlots = mongoose.Schema({
  date: {
    type: Date,
    required: true,
  },
  start_time: {
    type: String,
    required: true,
  },
  end_time: {
    type: String,
    required: true,
  },
  buffer_time: {
    type: String,
    required: true,
  },
});
const settingSchema = mongoose.Schema(
  {
    availability_slots: [availabilitySlots],
  },
  { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
);
// add plugin that converts mongoose to json
settingSchema.plugin(toJSON);
settingSchema.plugin(paginate);
/**
 * @typedef Setting
 */
const Settings = mongoose.model('Setting', settingSchema);

module.exports = Settings;
