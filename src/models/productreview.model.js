const mongoose = require('mongoose');
const { toJSON, paginate } = require('./plugins');

const { Schema } = mongoose;

const ReviewSchema = mongoose.Schema({
    user_id: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    rating_count: {
        type: Number,
        required: true,
    },
    review_desc: {
        type: String,
    },
    is_approve: {
        type: Boolean,
        enum: [true, false],
        default: false,
    },
    product_id: {
        type: Schema.Types.ObjectId,
        ref: 'Product',
        required: true,
    },
}, { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } });

// add plugin that converts mongoose to json
ReviewSchema.plugin(toJSON);
ReviewSchema.plugin(paginate);
/**
 * @typedef Review
 */
const Review = mongoose.model('Review', ReviewSchema);

module.exports = Review;
