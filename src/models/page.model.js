const mongoose = require('mongoose');
const slug = require('mongoose-slug-generator');
const validator = require('validator');
const bcrypt = require('bcryptjs');
const { toJSON, paginate } = require('./plugins');

mongoose.plugin(slug);
const { Schema } = mongoose;
const string = {
  type: String,
};
const blockContent = mongoose.Schema({
  heading: string,
  headingTwo: string,
  description: string,
  imageUrl: string,
  buttonText: string,
  buttonUrl: string,
});
const content = mongoose.Schema({
  headingOne: string,
  headingTwo: string,
  subHeading: string,
  description: string,
  buttonText: string,
  buttonUrl: string,
  imageUrl: string,
  block: [blockContent],
  sectionId: string,
});
const PageSchema = mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
      trim: true,
    },
    slug: {
      type: String,
      slug: 'title',
      unique: true,
    },
    content: [content],
    user: {
      type: Schema.Types.ObjectId,
      ref: 'User',
      required: true,
    },
  },
  { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
);
// add plugin that converts mongoose to json
PageSchema.plugin(toJSON);
PageSchema.plugin(paginate);
/**
 * @typedef Page
 */

PageSchema.pre('updateOne', async function (next) {
  console.log(JSON.stringify(this._update));
});
const Page = mongoose.model('Page', PageSchema);

module.exports = Page;
