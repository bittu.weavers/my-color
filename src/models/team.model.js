const mongoose = require('mongoose');
const { toJSON, paginate } = require('./plugins');

const { Schema } = mongoose;
const string = {
  type: String,
};
const socialBlock = mongoose.Schema({
  socailUrl: string,
  socailType: string,
});
const TeamSchema = mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      trim: true,
    },
    teamImage: {
        type: String,
        default: '',
      },
    social: [socialBlock],
    user: {
      type: Schema.Types.ObjectId,
      ref: 'User',
      required: true,
    },
  },
  { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
);
// add plugin that converts mongoose to json
TeamSchema.plugin(toJSON);
TeamSchema.plugin(paginate);
/**
 * @typedef Team
 */
const Team = mongoose.model('Team', TeamSchema);

module.exports = Team;
