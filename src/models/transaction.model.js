const mongoose = require('mongoose');
const { toJSON, paginate } = require('./plugins');
const { transactionType } = require('../config/constant');

const transactionSchema = mongoose.Schema(
  {
    userId: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: 'User',
      required: true,
    },
    amount: {
      type: Number,
      required: true,
      trim: true,
    },
    note: {
      type: String,
      trim: true,
    },
    type: {
      type: String,
      required: true,
      enum: transactionType,
      default: ['Order'],
    },
    status: {
      type: String,
      required: true,
    },
    refundstatus: {
      type: Boolean,
      default:false
    },
    refundamount: {
      type: Number,
    },
    transobject: {
      type: String,
    },
    charge:{
      type:String
    },
    refund:{
      type:String
    }
  },
  { timestamps: { createdAt: 'created_at' } }
);

// add plugin that converts mongoose to json //
transactionSchema.plugin(toJSON);
transactionSchema.plugin(paginate);

/**
 * @typedef Tag
 */
const transaction = mongoose.model('transaction', transactionSchema);

module.exports = transaction;
