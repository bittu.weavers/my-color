const mongoose = require('mongoose');
const { toJSON, paginate } = require('./plugins');

const bookingAvailabilitySchema = mongoose.Schema({
    userId: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'User',
        required: true,
    },
    availability:[
        {
            dayname:{type:String},
            daynum:{type:Number},
            status:{type:Boolean},
            buffertime:{type:Number},
            timeslots:[
                    {
                        starttime : {type:Number},
                        endtime : {type:Number},
                        slot:{type:Number}
                    }
                ],
        }
    ]
}, { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } });
// add plugin that converts mongoose to json
bookingAvailabilitySchema.plugin(toJSON);
bookingAvailabilitySchema.plugin(paginate);
/**
 * @typedef bookingavailability
 */
const bookingavailability = mongoose.model('bookingavailability', bookingAvailabilitySchema);

module.exports = bookingavailability;
