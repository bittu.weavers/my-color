const mongoose = require('mongoose');
const { toJSON, paginate } = require('./plugins');
const siteSettingsSchema = mongoose.Schema(
  {
    header_logo: {
      type: String,
    },
    footer_logo: {
      type: String,
    },
    social: [
      {
        id:{type:mongoose.Types.ObjectId},
        type:{type:String},
        link:{type:String},
      }
     ],
    footer_text: {
      type: String,
    },
    copyright_text: {
      type: String,
    },
    header_text:{
      type: String,
    },
    insta_followlink:{
      type: String,
    },
  },
  {
    timestamps: true,
  }
);

// add plugin that converts mongoose to json
siteSettingsSchema.plugin(toJSON);
siteSettingsSchema.plugin(paginate);

const SiteSettings = mongoose.model('SiteSettings', siteSettingsSchema);

module.exports = SiteSettings;
