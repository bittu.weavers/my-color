const mongoose = require('mongoose');
const { toJSON, paginate } = require('./plugins');

const taxSchema = mongoose.Schema({
    country: {
        type: String,
    },
    state: {
        type: String,
    },
    tax: {
        type: Number,
    },
}, { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } });



taxSchema.plugin(toJSON);
taxSchema.plugin(paginate);


const tax = mongoose.model('tax', taxSchema);

module.exports = tax;
