const userType = ['facebook', 'google', 'email'];
const bookingStatus = ['Booked', 'Inprogress', 'Completed', 'Pending', 'Cancelled'];
const addressType = ['home', 'office', 'others'];
const currency = ['USD'];
const currencySymbol = ['$'];
const transactionType = ['Booking', 'Order'];
const productStatusType = ['publish', 'draft', 'private'];
const productType = ['simple', 'variable'];

module.exports = {
  userType,
  currency,
  currencySymbol,
  addressType,
  bookingStatus,
  transactionType,
  productStatusType,
  productType,
};
