const roles = ['user', 'admin','colourist'];
const roleRights = new Map();
roleRights.set(roles[0], ['manageImage','manageUsers','manageProfile']);
roleRights.set(roles[1], ['manageAdmin','manageProfile','manageImage']);
roleRights.set(roles[2], ['manageColourist','manageProfile','manageImage']);
module.exports = {
  roles,
  roleRights,
};
