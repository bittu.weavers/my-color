const express = require('express');
const validate = require('../../../middlewares/validate');
const colourValidator = require("../../../validations/colourstories.validation");
const colourstoriesController = require("../../../controllers/colourstory.controller");

const router = express.Router();

router.get('/storyList', colourstoriesController.storyList);

module.exports = router;
