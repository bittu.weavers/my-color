const express = require('express');
const postController = require('../../../controllers/post.controller');

const router = express.Router();
router.get('/get-post-list/:post_type/:current_page?', postController.getPostListFrontEnd);
router.get('/:post_type/:slug', postController.getPostBySlug);
router.get('/related-post/:post_type/:pexslug', postController.getRelatedPost);
module.exports = router;

/**
 * @swagger
 * paths:
 *  /post/get-post-list/:post_type/:current_page?:
 *    get:
 *      summary: Get Post
 *      tags: [Post Front-end]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: false
 *      responses:
 *        "200":
 *          description: Post Fetch Successfully
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/MessageType'
 *              example:
 *               code: 200
 *               message: Post Fetch Successfully
 */
/**
 * @swagger
 * paths:
 *  /post/:post_type/:slug/:
 *    get:
 *      summary: Get Post By slug and post type
 *      tags: [Post Front-end]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: false
 *      responses:
 *        "200":
 *          description: Post Fetch
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/MessageType'
 *              example:
 *               code: 200
 *               message: Post Fetch Successfully
 */
