const express = require('express');
const useragent = require('express-useragent');
const validate = require('../../../middlewares/validate');
const authValidation = require('../../../validations/auth.validation');
const authController = require('../../../controllers/auth.controller');
const auth = require('../../../middlewares/auth');

const router = express.Router();

router.use(useragent.express());
router.post('/register', validate(authValidation.register), authController.register);
router.post('/login', validate(authValidation.login), authController.login);
router.post('/logout', validate(authValidation.logout), authController.logout);
router.post('/forgot-password-otp', validate(authValidation.forgotPassword), authController.forgotPasswordOTP);
router.post('/otp-verification', validate(authValidation.otpVerification), authController.otpVerification);
router.post('/reset-password', validate(authValidation.resetPassword), authController.resetPassword);
router.post('/social-login', validate(authValidation.socialLogin), authController.socialLogin);
router.post('/refreshToken', validate(authValidation.refreshToken), authController.refreshToken);

module.exports = router;

/**
 * @swagger
 * paths:
 *  /auth/register:
 *    post:
 *      summary: Register as user
 *      tags: [Auth]
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - fname
 *                - lname
 *                - email
 *                - password
 *              properties:
 *                fname:
 *                  type: string
 *                lname:
 *                  type: string
 *                email:
 *                  type: string
 *                  format: email
 *                  description: must be unique
 *                password:
 *                  type: string
 *                  format: password
 *                  minLength: 8
 *                  description: At least one number and one letter
 *              example:
 *                fname: First fake
 *                lname: Last fake
 *                email: fake@example.com
 *                password: Password1@
 *      responses:
 *        "200":
 *          description: Created
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  user:
 *                    $ref: '#/components/schemas/User'
 *                  tokens:
 *                    $ref: '#/components/schemas/AuthTokens'
 *        "400":
 *          $ref: '#/components/responses/DuplicateEmail'
 */

/**
 * @swagger
 * paths:
 *  /auth/social-login:
 *    post:
 *      summary: Social login account creation
 *      tags: [Auth]
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - socialId
 *                - fname
 *                - email
 *                - usertype
 *              properties:
 *                socialId:
 *                  type: string
 *                fname:
 *                  type: string
 *                email:
 *                  type: string
 *                  format: email
 *                  description: must be unique
 *                usertype:
 *                  type: string
 *                  enum: [facebook, google]
 *              example:
 *                socialId: "12345"
 *                fname: First fake
 *                email: fake@example.com
 *                usertype: facebook
 *      responses:
 *        "200":
 *          description: Created
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  user:
 *                    $ref: '#/components/schemas/User'
 *                  tokens:
 *                    $ref: '#/components/schemas/AuthTokens'
 *        "400":
 *          $ref: '#/components/responses/DuplicateEmail'
 */

/**
 * @swagger
 * paths:
 *  /auth/login:
 *    post:
 *      summary: Login
 *      tags: [Auth]
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - email
 *                - password
 *              properties:
 *                email:
 *                  type: string
 *                  format: email
 *                password:
 *                  type: string
 *                  format: password
 *              example:
 *                email: fake@example.com
 *                password: Password1@
 *      responses:
 *        "200":
 *          description: OK
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  user:
 *                    $ref: '#/components/schemas/User'
 *                  tokens:
 *                    $ref: '#/components/schemas/AuthTokens'
 *        "401":
 *          description: Invalid email or password
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Error'
 *              example:
 *                code: 401
 *                message: Invalid email or password
 */

/**
 * @swagger
 * paths:
 *  /auth/logout:
 *    post:
 *      summary: Logout
 *      tags: [Auth]
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - refreshToken
 *              properties:
 *                refreshToken:
 *                  type: string
 *              example:
 *                refreshToken: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI1ZWJhYzUzNDk1NGI1NDEzOTgwNmMxMTIiLCJpYXQiOjE1ODkyOTg0ODQsImV4cCI6MTU4OTMwMDI4NH0.m1U63blB0MLej_WfB7yC2FTMnCziif9X8yzwDEfJXAg
 *      responses:
 *        "200":
 *          description: ok
 *          content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/MessageType'
 *             example:
 *               code: 200
 *               message: You have been successfully logged out!
 *        "404":
 *          $ref: '#/components/responses/NotFound'
 */

/**
 * @swagger
 * paths:
 *  /auth/forgot-password-otp:
 *    post:
 *      summary: Forgot password with otp
 *      description: An email will be sent to reset password otp.
 *      tags: [Auth]
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - email
 *              properties:
 *                email:
 *                  type: string
 *                  format: email
 *              example:
 *                email: fake@example.com
 *      responses:
 *        "200":
 *          description: ok
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/MessageType'
 *              example:
 *                code: 200
 *                message: Verification code has been sent to your email address.
 *        '404':
 *          description: User Not Found
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/MessageType'
 *              example:
 *                code: 404
 *                message: No user found with this email.
 */

/**
 * @swagger
 * paths:
 *  /auth/otp-verification:
 *    post:
 *      summary: Otp verification by otp and email
 *      description: An otp is sent to your email id.
 *      tags: [Auth]
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - email
 *                - otp
 *              properties:
 *                email:
 *                  type: string
 *                  format: email
 *                otp:
 *                  type: string
 *              example:
 *                email: fake@example.com
 *                otp: "89788"
 *      responses:
 *        "200":
 *          description: ok
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  code:
 *                    type: integer
 *                  message:
 *                    type: string
 *                  isVerificationSuccess:
 *                    type: boolean
 *                example:
 *                   code: 200
 *                   message: Success.
 *                   isVerificationSuccess: true
 *        '404':
 *          description: User Not Found
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/MessageType'
 *              example:
 *                code: 404
 *                message: No data found with this email.
 */

/**
 * @swagger
 * paths:
 *  /auth/reset-password:
 *    post:
 *      summary: Reset password
 *      tags: [Auth]
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - password
 *                - email
 *                - otp
 *              properties:
 *                password:
 *                  type: string
 *                  format: password
 *                  minLength: 8
 *                  description: At least one number and one letter
 *                email:
 *                  type: string
 *                  format: email
 *                otp:
 *                  type: string
 *              example:
 *                password: password1
 *                email: fake@example.com
 *                otp: "89788"
 *      responses:
 *        "200":
 *          description: ok
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/MessageType'
 *              example:
 *                code: 200
 *                message: Your password has been reset successfully.
 *        "400":
 *          description: User not found/OTP dose not match
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/MessageType'
 *              example:
 *                code: 400
 *                message: User not found/OTP dose not match
 */
