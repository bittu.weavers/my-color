const express = require('express');
const validate = require('../../../middlewares/validate');
const productreviewValidation = require('../../../validations/productreview.validation');
const productreviewController = require('../../../controllers/productreview.controller');
const auth = require('../../../middlewares/auth');

const router = express.Router();
router.use(auth('manageProfile'))

router.post(
  '/add-product-review',
  validate(productreviewValidation.productReviewAdd),
  productreviewController.productReviewAdd
);
router.post(
  '/product-review-status',
  validate(productreviewValidation.productreviewstatus),
  productreviewController.productreviewstatus
);
router.delete(
  '/delete-review',
  validate(productreviewValidation.deleteproductreview),
  productreviewController.deleteproductreview
);
module.exports = router;

/**
 * @swagger
 * paths:
 *  /productreview/add-product-review:
 *    post:
 *      summary: Add Product Review
 *      tags: [Product Review]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - productid
 *                - userid
 *                - rating_count
 *                - review_desc
 *                - orderid
 *              properties:
 *                productid:
 *                  type: string
 *                userid:
 *                  type: string
 *                rating_count:
 *                  type: number
 *                review_desc:
 *                  type: string
 *                orderid:
 *                  type: string
 *              example:
 *               productid: "61bb575a9de9ff155023e7d0"
 *               userid: "61bb505a6de9ff155023e7d5"
 *               rating_count: 1
 *               review_desc: "Product is good and recomendable"
 *               orderid: "61bb575a9de9ff155023e7d2"
 *      responses:
 *        "200":
 *          description: Add Product Review
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/MessageType'
 *              example:
 *               code: 200
 *               message: Product review submitted successfully
 */
/**
 * @swagger
 * paths:
 *  /productreview/delete-review:
 *    delete:
 *      summary: Delete Product Review
 *      tags: [Product Review]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - id
 *              properties:
 *                id:
 *                type: string
 *              example:
 *                id: "61bb505a6de9ff155023e7d5"
 *      responses:
 *        "200":
 *          description: Delete Product Review
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/MessageType'
 *              example:
 *               code: 200
 *               message: Product review deleted successfully
 */
/**
 * @swagger
 * paths:
 *  /productreview/product-review-status:
 *    post:
 *      summary: Product Review Status Change
 *      tags: [Product Review]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - id
 *                - review_status
 *              properties:
 *                id:
 *                 type: string
 *                review_status:
 *                 type: boolean
 *              example:
 *                id: "123456"
 *                review_status: true
 *      responses:
 *        "200":
 *          description: Product Review Status Change
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/MessageType'
 *              example:
 *               code: 200
 *               message: Product review status chnaged successfully
 */
