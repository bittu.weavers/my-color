const express = require('express');
const validate = require('../../../middlewares/validate');
const auth = require('../../../middlewares/auth');
const cardValidation = require('../../../validations/card.validation');
const cardController = require('../../../controllers/card.controller');

const router = express.Router();
router.use(auth('manageProfile'));
router.post('/add-card', validate(cardValidation.addCard), cardController.addCard);
router.get('/default-card/:cardId', validate(cardValidation.defaultCard), cardController.defaultCard);
router.get('/all-card-list/', cardController.getCardList);
router.delete('/delete-card/:cardId',validate(cardValidation.defaultCard), cardController.deleteCard);

module.exports = router;