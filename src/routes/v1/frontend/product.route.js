const express = require('express');
const validate = require('../../../middlewares/validate');
const productController = require('../../../controllers/product.controller');
const productValidation = require('../../../validations');
const auth = require('../../../middlewares/auth');
const router = express.Router();
router.get('/filter/list', productController.productFilterlist);
router.post('/filter/:current_page?', validate(productValidation.productListAndFilter), productController.productFilter);

router.get('/:slug', productController.getproductByslug);
// Product Review
router.post('/review/add',auth('manageProfile'), validate(productValidation.addReview), productController.addReview);
router.get('/review/:pid/:current_page?', productController.getProductReviewByIduser);
router.get('/reviewdetails/:rid',auth('manageProfile'), productController.getProductReviewDetails);
router.patch('/reviewstatus',auth('manageProfile'), validate(productValidation.reviewStatusUpdate), productController.reviewStatusUpdate);
router.delete('/reviewdelete/:id',auth('manageProfile'), productController.deleteReview);

//Product List for header search
router.post('/search/headerSearch', productController.getProductListForHeader);

module.exports = router;

