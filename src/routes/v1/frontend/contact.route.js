const express = require('express');
const validate = require('../../../middlewares/validate');
const contactController = require('../../../controllers/contact.controller');
const contactvalidation = require('../../../validations/contact.validation');

const router = express.Router();
router.post('/send', validate(contactvalidation.sendMail), contactController.sendMail);
module.exports = router;
