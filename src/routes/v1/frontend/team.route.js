const express = require('express');
const validate = require('../../../middlewares/validate');
const teamController = require('../../../controllers/team.controller');
const router = express.Router();
router.get('/get/:current_page?', teamController.getTeamListFrontEnd);
module.exports = router;

/**
 * @swagger
 * paths:
 *  /team/get-post-list/current_page?:
 *    get:
 *      summary: Get Teams
 *      tags: [Team Front-end]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: false
 *      responses:
 *        "200":
 *          description: Team Fetch Successfully
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/MessageType'
 *              example:
 *               code: 200
 *               message: Team Fetch Successfully
 */
