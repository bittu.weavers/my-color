const express = require('express');
const validate = require('../../../middlewares/validate');
const orderValidation = require('../../../validations/order.validation');
const orderController = require('../../../controllers/order.controller');
const auth = require('../../../middlewares/auth');

const router = express.Router()

router.use(auth('manageProfile'));

//orders/order-details/:orderid
router.get('/order-details/:orderid',validate(orderValidation.details),orderController.getOrder);
router.get('/all-orders/',orderController.getAllOrders);
router.post('/cancel/',validate(orderValidation.cancellOrder),orderController.cancellOrder);

module.exports = router;