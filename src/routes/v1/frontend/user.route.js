const express = require('express');
const validate = require('../../../middlewares/validate');
const userValidation = require('../../../validations/user.validation');
const userController = require('../../../controllers/user.controller');
const uploadFile = require('../../../middlewares/imageUpload.middleware');
const auth = require('../../../middlewares/auth');

const router = express.Router();
router.use(auth('manageProfile'))
router
  .post('/get-user-detail', validate(userValidation.getUserDetail), userController.getUserDetail)
  .patch('/update-user-image',uploadFile('profileimage'), userController.updateUser)
  .patch('/update-user', validate(userValidation.updateUser), userController.updateUser)
  .post('/change-password', validate(userValidation.changePassword), userController.changePassword)
  .delete('/delete-social-user', validate(userValidation.deleteSocialUser), userController.deleteSocialUser);
module.exports = router;

/**
 * @swagger
 * paths:
 *  /users/get-user-detail:
 *    post:
 *      summary: Get a user
 *      description: Logged in users can fetch only their own user information.
 *      tags: [Users]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - userId
 *              properties:
 *                userId:
 *                  type: string
 *              example:
 *                userId: 61bc9d3f8bf7db49c0056dc6
 *      responses:
 *        "200":
 *          description: OK
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/User'
 *        "401":
 *          $ref: '#/components/responses/Unauthorized'
 *        "403":
 *          $ref: '#/components/responses/Forbidden'
 *        "404":
 *          $ref: '#/components/responses/NotFound'
 */
/**
 * @swagger
 * paths:
 *  /users/update-user/:
 *    patch:
 *      summary: Update a user
 *      description: Logged in users can only update their own information.
 *      tags: [Users]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - userId
 *                - fname
 *                - lname
 *                - email
 *              properties:
 *                userId:
 *                  type: string
 *                fname:
 *                  type: string
 *                lname:
 *                  type: string
 *                email:
 *                  type: string
 *                  format: email
 *                  description: must be unique
 *              example:
 *                userId: 61bc9d3f8bf7db49c0056dc6
 *                fname: First fake name
 *                lname: First fake name
 *                email: fake@example.com
 *      responses:
 *        "200":
 *          description: OK
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/User'
 *        "400":
 *          $ref: '#/components/responses/DuplicateEmail'
 *        "401":
 *          $ref: '#/components/responses/Unauthorized'
 *        "403":
 *          $ref: '#/components/responses/Forbidden'
 *        "404":
 *          $ref: '#/components/responses/NotFound'
 */
/**
 * @swagger
 * paths:
 *  /users/change-password/:
 *    post:
 *      summary: change password for user
 *      description: Logged in users can only change password their own information.
 *      tags: [Users]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - email
 *                - oldPassword
 *                - newPassword
 *              properties:
 *                email:
 *                  type: string
 *                  format: email
 *                  description: must be unique
 *                oldPassword:
 *                   type: string
 *                newPassword:
 *                  type: string
 *                  format: password
 *                  minLength: 8
 *                  description: At least one number and one letter
 *              example:
 *                email: fake@example.com
 *                oldPassword: Password1@
 *                newPassword: NewPassword1@
 *      responses:
 *       '200':
 *         description: ok
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/MessageType'
 *             example:
 *               code: 200
 *               message: Your password has been changed successfully
 *       "401":
 *          $ref: '#/components/responses/Unauthorized'
 *       '403':
 *         description: Old Password dose not match
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/MessageType'
 *             example:
 *               code: 403
 *               message: Old Password dose not match
 *       '404':
 *         description: User Not Found
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/MessageType'
 *             example:
 *               code: 404
 *               message: User not found
 */
/**
 * @swagger
 * paths:
 *  /users/delete-social-user:
 *    delete:
 *      summary: Delete Social Registered User
 *      description: Functionality for delete social registered user i.e google/facebook
 *      tags: [Users]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - socialId
 *                - usertype
 *              properties:
 *                socialId:
 *                  type: string
 *                usertype:
 *                  type: string
 *              example:
 *                socialId: 61bc9d3f8bf7db49c0056dc6
 *                usertype: facebook/google
 *      responses:
 *        "200":
 *          description: OK
 *          content:
 *            application/json:
 *              schema:
 *               $ref: '#/components/schemas/MessageType'
 *              example:
 *               code: 200
 *               message: User unlink successfully
 *        "401":
 *          $ref: '#/components/responses/Unauthorized'
 *        "403":
 *          $ref: '#/components/responses/Forbidden'
 *        "404":
 *          $ref: '#/components/responses/NotFound'
 */
