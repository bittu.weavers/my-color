const express = require('express');
const cartController = require('../../../controllers/cart.controller');
const addToCartValidation=require('../../../validations/cart.validation');
const validate = require('../../../middlewares/validate');
const router = express.Router();

router.put('/add-to-cart',validate(addToCartValidation.addToCart) ,cartController.addToCart);
router.post('/updatecart',validate(addToCartValidation.updatecart) ,cartController.updatecart);
router.post('/remove-item',validate(addToCartValidation.removal),cartController.removeItem);
router.post('/cart-items',validate(addToCartValidation.getCart),cartController.getCart);
router.post('/assign-userid',validate(addToCartValidation.assignId),cartController.assignUserId);
router.post('/calculate',validate(addToCartValidation.calculate),cartController.calculateCart);
router.post('/add-coupon',validate(addToCartValidation.coupon),cartController.assignCoupon);
router.post('/remove-coupon',validate(addToCartValidation.coupon),cartController.removeCoupon);
router.post('/total-items',validate(addToCartValidation.cartQt),cartController.getQuantity);
module.exports = router;



