const express = require('express');
const validate = require('../../../middlewares/validate');
const wishlistValidation = require('../../../validations/wishlist.validation');
const wishlistController = require('../../../controllers/wishlist.controller');
const auth = require('../../../middlewares/auth');

const router = express.Router();
router.use(auth('manageProfile'))

router.post('/add-to-wishlist', validate(wishlistValidation.addproducttowishlist), wishlistController.addproducttowishlist);
router.get('/get-wishlist/:current_page?', wishlistController.getWishlist);
router.patch(
  '/delete-wishlist',
  validate(wishlistValidation.deleteproductwishlist),
  wishlistController.deleteproductwishlist
);

module.exports = router;

/**
 * @swagger
 * paths:
 *  /wishlist/add-to-wishlist:
 *    post:
 *      summary: Add Product To Wishlist
 *      tags: [Product Wishlist]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - userId
 *                - productId
 *              properties:
 *                userId:
 *                  type: string
 *                productId:
 *                  type: string
 *              example:
 *                userId: "61bb505a6de9ff155023e7d5"
 *                productId: "61bb575a9de9ff155023e7d5"
 *      responses:
 *        "200":
 *          description: Product wishlist
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/MessageType'
 *              example:
 *               code: 200
 *               message: Product added to wishlist
 */

/**
 * @swagger
 * paths:
 *  /wishlist/delete-wishlist:
 *    delete:
 *      summary: Remove Product From Wishlist
 *      tags: [Product Wishlist]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - id
 *              properties:
 *                id:
 *                type: string
 *              example:
 *                id: "61bb505a6de9ff155023e7d5"
 *      responses:
 *        "200":
 *          description: Remove Product From Wishlist
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/MessageType'
 *              example:
 *               code: 200
 *               message: Product removed from wishlist.
 */
