const express = require('express');
const validate = require('../../../middlewares/validate');
const paymentvalidate = require('../../../validations/payment.validation');
const paymentcontroller = require('../../../controllers/payment.controller');
const auth = require('../../../middlewares/auth');

const router = express.Router()

router.use(auth('manageProfile'));
router.post('/pay',validate(paymentvalidate.payme),paymentcontroller.payme);

module.exports = router;