const express = require('express');
const pageController = require('../../../controllers/page.controller');
const router = express.Router();

router.post('/get', pageController.getPages);
router.get('/:slug', pageController.getPageBySlug);
router.get('/section/:id', pageController.getSectionById);
module.exports = router;
