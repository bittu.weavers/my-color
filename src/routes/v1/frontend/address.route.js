const express = require('express');
const validate = require('../../../middlewares/validate');
const addressValidation = require('../../../validations/address.validation');
const addressController = require('../../../controllers/address.controller');
const auth = require('../../../middlewares/auth');

const router = express.Router();
router.use(auth('manageProfile'))
router
  .post('/add-address', validate(addressValidation.addAddress), addressController.addAddress)
  .post('/address-list', validate(addressValidation.getAddressList), addressController.getAddressList)
  .post('/address-detail', validate(addressValidation.addressDetail), addressController.addressDetail)
  .patch('/update-address', validate(addressValidation.updateAddress), addressController.updateAddress)
  .delete('/delete-address', validate(addressValidation.deleteAddress), addressController.deleteAddress)
  .post('/change-status-address', validate(addressValidation.changeStatusAddress), addressController.changeStatusAddress);

module.exports = router;

/**
 * @swagger
 * paths:
 *  /address/add-address:
 *    post:
 *      summary: add address to the user
 *      description: Logged in users can only add their address and addType must be ['home', 'office', 'others'].
 *      tags: [Address]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - userId
 *                - firstname
 *                - lastname
 *                - addressline1
 *                - city
 *                - zipcode
 *                - state
 *                - country
 *                - countrycode
 *                - phonecode
 *                - phone
 *                - addEmail
 *                - addType
 *              properties:
 *                userId:
 *                  type: string
 *                  description: must be User Id
 *                firstname:
 *                  type: string
 *                  description: Enter the First Name
 *                lastname:
 *                  type: string
 *                  description: Enter the Last Name
 *                addressline1:
 *                   type: string
 *                   description: Enter the Address Line1
 *                addressline2:
 *                   type: string
 *                   description: Enter the Address Line2
 *                city:
 *                   type: string
 *                   description: Enter the City
 *                zipcode:
 *                   type: string
 *                   description: Enter the Zipcode
 *                state:
 *                   type: string
 *                   description: Enter the State
 *                country:
 *                   type: string
 *                   description: Enter the Country
 *                countrycode:
 *                   type: string
 *                   description: Enter the Country Code
 *                phonecode:
 *                   type: string
 *                   description: Enter the Phonecode Code
 *                phone:
 *                   type: string
 *                   description: Enter the Phonecode Code
 *                addEmail:
 *                  type: string
 *                  format: email
 *                  description: Enter the Email Address
 *                addType:
 *                  type: string
 *                  enum: ['home', 'office', 'others']
 *              example:
 *                userId: 61bc9d3f8bf7db49c0056dc6
 *                firstname: Fake firstname
 *                lastname: Fake lastname
 *                addressline1: 59 s m lane
 *                addressline2: ""
 *                city: Howrah
 *                zipcode: "711101"
 *                state: west bengal
 *                country: india
 *                countrycode: IN
 *                phonecode: "+91"
 *                phone: "9876543210"
 *                addEmail: Fake@gmail.com
 *                addType: 'home'
 *      responses:
 *       '200':
 *         description: ok
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/MessageType'
 *             example:
 *               code: 200
 *               message: Address added successfully
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       '404':
 *         description: User Not Found
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/MessageType'
 *             example:
 *               code: 404
 *               message: User not found
 */
/**
 * @swagger
 * paths:
 *  /address/address-list:
 *    post:
 *      summary: address list of user
 *      description: Logged in users can only access their address.
 *      tags: [Address]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - userId
 *              properties:
 *                userId:
 *                  type: string
 *                  description: must be User Id
 *              example:
 *                userId: 61bc9d3f8bf7db49c0056dc6
 *      responses:
 *       '200':
 *         description: ok
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 addressData:
 *                    type: array
 *                    items:
 *                      $ref: '#/components/schemas/Address'
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       '404':
 *         description: User Not Found
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/MessageType'
 *             example:
 *               code: 404
 *               message: User not found
 */
/**
 * @swagger
 * paths:
 *  /address/address-detail:
 *    post:
 *      summary: address list of user
 *      description: Logged in users can only access their address detail.
 *      tags: [Address]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - userId
 *                - addressId
 *              properties:
 *                userId:
 *                  type: string
 *                  description: must be User Id
 *                addressId:
 *                  type: string
 *                  description: must be Address Id
 *              example:
 *                userId: 61bc9d3f8bf7db49c0056dc6
 *                addressId: 61c31738af5ba306acfbd03f
 *      responses:
 *       '200':
 *         description: ok
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 addressData:
 *                    type: object
 *                    $ref: '#/components/schemas/Address'
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       '404':
 *         description: User Not Found
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/MessageType'
 *             example:
 *               code: 404
 *               message: User not found/Address not found
 */
/**
 * @swagger
 * paths:
 *  /address/update-address:
 *    patch:
 *      summary: add address to the user
 *      description: Logged in users can only update their address and addType must be ['home', 'office', 'others'].
 *      tags: [Address]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *             type: object
 *             required:
 *               - userId
 *               - addressId
 *               - firstname
 *               - lastname
 *               - addressline1
 *               - city
 *               - zipcode
 *               - state
 *               - country
 *               - countrycode
 *               - phonecode
 *               - phone
 *               - addEmail
 *               - addType
 *             properties:
 *               userId:
 *                 type: string
 *                 description: must be User Id
 *               addressId:
 *                 type: string
 *                 description: must be User Id
 *               firstname:
 *                 type: string
 *                 description: Enter the First Name
 *               lastname:
 *                 type: string
 *                 description: Enter the Last Name
 *               addressline1:
 *                 type: string
 *                 description: Enter the Address Line1
 *               addressline2:
 *                 type: string
 *                 description: Enter the Address Line2
 *               city:
 *                 type: string
 *                 description: Enter the City
 *               zipcode:
 *                 type: string
 *                 description: Enter the Zipcode
 *               state:
 *                 type: string
 *                 description: Enter the State
 *               country:
 *                 type: string
 *                 description: Enter the Country
 *               countrycode:
 *                 type: string
 *                 description: Enter the Country Code
 *               phonecode:
 *                 type: string
 *                 description: Enter the Phonecode Code
 *               phone:
 *                 type: string
 *                 description: Enter the Phonecode Code
 *               addEmail:
 *                 type: string
 *                 format: email
 *                 description: Enter the Email Address
 *               addType:
 *                 type: string
 *                 enum: ['home', 'office', 'others']
 *             example:
 *               userId: 61bc9d3f8bf7db49c0056dc6
 *               addressId: 61c31738af5ba306acfbd03f
 *               firstname: Fake firstname
 *               lastname: Fake lastname
 *               addressline1: 59 s m lane
 *               addressline2: ""
 *               city: Howrah
 *               zipcode: "711101"
 *               state: west bengal
 *               country: india
 *               countrycode: IN
 *               phonecode: "+91"
 *               phone: "9876543210"
 *               addEmail: Fake@gmail.com
 *               addType: 'home'
 *      responses:
 *       '200':
 *         description: ok
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/MessageType'
 *             example:
 *               code: 200
 *               message: Address updated successfully
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       '404':
 *         description: User Not Found
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/MessageType'
 *             example:
 *               code: 404
 *               message: User not found/Address not found
 */
/**
 * @swagger
 * paths:
 *  /address/delete-address:
 *    delete:
 *      summary: Delete address to the user
 *      description: Logged in users can only delete their address.
 *      tags: [Address]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - userId
 *                - addressId
 *              properties:
 *                userId:
 *                  type: string
 *                  description: must be User Id
 *                addressId:
 *                  type: string
 *                  description: must be Address Id
 *              example:
 *                userId: 61bc9d3f8bf7db49c0056dc6
 *                addressId: 61c31738af5ba306acfbd03f
 *      responses:
 *       '200':
 *         description: ok
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/MessageType'
 *             example:
 *               code: 200
 *               message: Address deleted successfully
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       '404':
 *         description: User Not Found
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/MessageType'
 *             example:
 *               code: 404
 *               message: User not found/Address not found
 */
/**
 * @swagger
 * paths:
 *  /address/change-status-address:
 *    post:
 *      summary: change address status to the user
 *      description: Logged in users can only change their address status.
 *      tags: [Address]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *             type: object
 *             required:
 *               - userId
 *               - addressId
 *               - primaryStatus
 *             properties:
 *               userId:
 *                 type: string
 *                 description: must be User Id
 *               addressId:
 *                 type: string
 *                 description: must be User Id
 *               primaryStatus:
 *                 type: boolean
 *                 description: Enter the primaryStatus true/false
 *             example:
 *               userId: 61bc9d3f8bf7db49c0056dc6
 *               addressId: 61c31738af5ba306acfbd03f
 *               primaryStatus: true
 *      responses:
 *       '200':
 *         description: ok
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/MessageType'
 *             example:
 *               code: 200
 *               message: Address status change successfully
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       '404':
 *         description: User Not Found
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/MessageType'
 *             example:
 *               code: 404
 *               message: User not found/Address not found
 */
