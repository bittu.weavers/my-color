const express = require('express');
const validate = require('../../../middlewares/validate');
const bookingconsultationValidation = require('../../../validations/bookingconsultation.validation');
const bookingconsultationController = require('../../../controllers/bookingconsultation.controller');
const auth = require('../../../middlewares/auth');

const router = express.Router();


router.post(
    '/bookingconsultation-add',
    validate(bookingconsultationValidation.bookingconsultationadd),
    bookingconsultationController.bookingconsultationadd
);
router.delete(
    '/bookingconsultation-delete',
    validate(bookingconsultationValidation.bookingconsultationdelete),
    bookingconsultationController.bookingconsultationdelete
);
router.get('/bookingconsultation-list', auth('manageUsers'), bookingconsultationController.bookingconsultationlist);
router.patch(
    '/bookingconsultation-update',
    validate(bookingconsultationValidation.bookingconsultationupdate),
    bookingconsultationController.bookingconsultationupdate
);
router.post(
    '/bookingconsultation-statuschange',
    validate(bookingconsultationValidation.bookingconsultationstatuschange),
    bookingconsultationController.bookingconsultationstatuschange
);
router.post(
    '/bookingconsultation-details',
    validate(bookingconsultationValidation.bookingconsultationdetails),
    bookingconsultationController.bookingconsultationdetails
);
router.post(
    '/bookingconsultation-stripe-payment',
    validate(bookingconsultationValidation.consultationpayment),auth('manageProfile'),
    bookingconsultationController.consultationpayment
);
router.post('/get-availability-days', bookingconsultationController.getAvailabilityDays);
router.post('/get-availability-slots-date', bookingconsultationController.getBookingSlotsDate);
router.post('/get-availability-details', bookingconsultationController.getBookingSlotsDetails);

router.post('/get-availability-time-slots', bookingconsultationController.getAvailableTimeSlot);
module.exports = router;

/**
 * @swagger
 * paths:
 *  /bookingconsultation/bookingconsultation-add:
 *    post:
 *      summary: Add Booking Consultation
 *      tags: [Booking Consultation]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - userId
 *                - fname
 *                - lname
 *                - email
 *                - bdate
 *                - btime
 *                - joinlink
 *              properties:
 *                userId:
 *                  type: string
 *                fname:
 *                  type: string
 *                lname:
 *                  type: string
 *                email:
 *                  type: string
 *                bdate:
 *                  type: Date
 *                joinlink:
 *                  type: string
 *              example:
 *               userid: "61bb505a6de9ff155023e7d5"
 *               fname: "Rahul"
 *               lname: "Mandal"
 *               email: "abc@gmail.com"
 *               bdate: "2022-05-10"
 *               joinlink: ""
 *      responses:
 *        "200":
 *          description: Consultation booking is successful, please proceed with the payment!
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/MessageType'
 *              example:
 *               code: 200
 *               message: Consultation booking is successful, please proceed with the payment!
 */
/**
 * @swagger
 * paths:
 *  /bookingconsultation/bookingconsultation-list:
 *    get:
 *      summary: List Booking Consultation
 *      tags: [Booking Consultation]
 *      security:
 *        - bearerAuth: []
 *      responses:
 *        "200":
 *          description: Sucess ( Data will be fetch by user token)
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/MessageType'
 *              example:
 *               code: 200
 *               message: Sucess
 */
/**
 * @swagger
 * paths:
 *  /bookingconsultation/bookingconsultation-delete:
 *    delete:
 *      summary: Delete Booking Consultation
 *      tags: [Booking Consultation]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - id
 *              properties:
 *                id:
 *                  type: string
 *              example:
 *               id: "61bb505a6de9ff155023e7d5"
 *      responses:
 *        "200":
 *          description: Consultation booking is deleted successfully
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/MessageType'
 *              example:
 *               code: 200
 *               message: Consultation booking is deleted successfully
 */
/**
 * @swagger
 * paths:
 *  /bookingconsultation/bookingconsultation-details:
 *    post:
 *      summary: Details Booking Consultation
 *      tags: [Booking Consultation]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - id
 *              properties:
 *                id:
 *                  type: string
 *              example:
 *               id: "61bb505a6de9ff155023e7d5"
 *      responses:
 *        "200":
 *          description: Sucess
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/MessageType'
 *              example:
 *               code: 200
 *               message: Sucess
 */
/**
 * @swagger
 * paths:
 *  /bookingconsultation/bookingconsultation-statuschange:
 *    post:
 *      summary: Booking Consultation Status Change
 *      tags: [Booking Consultation]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - id
 *                - bookingstatus
 *              properties:
 *                id:
 *                  type: string
 *              example:
 *               id: "61bb505a6de9ff155023e7d5"
 *               bookingstatus: "Booked"
 *      responses:
 *        "200":
 *          description: Consultation booking status updated successfully
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/MessageType'
 *              example:
 *               code: 200
 *               message: Consultation booking status updated successfully
 */
