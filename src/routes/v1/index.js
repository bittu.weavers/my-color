const express = require('express');
const authRoute = require('./frontend/auth.route');
const userRoute = require('./frontend/user.route');
const addressRoute = require('./frontend/address.route');
const wishlistRoute = require('./frontend/wishlist.route');
const productreviewRoute = require('./frontend/productreview.route');
const bookingconsultationRoute = require('./frontend/bookingconsultation.route');
const profileRoute = require('./profile.route');
const docsRoute = require('./docs.route');
const uploadRoute = require('./upload.route');
const postFrontEnd = require('./frontend/post.route');
const pageFronEnd = require('./frontend/page.route');
const teamRoute = require('./frontend/team.route');
const contact = require('./frontend/contact.route');
const productFronend = require('./frontend/product.route');
const cardRoute = require('./frontend/card.route');
const colourStoriesRoute = require('./frontend/colourstories.route');

/* Admin Route */
const productRoute = require('./admin/product.route');
const attributeRoute = require('./admin/attribute.route');
const productcategoryRoute = require('./admin/productcategory.route');
const admincouponRoute = require('./admin/coupon.route');
const videopostRoute = require('./admin/videopost.route');
const postRoute = require('./admin/post.route');
const pageRoute = require('./admin/page.route');
const imageRoute = require('./admin/image.route');
const bookingconsultationAdmin = require('./admin/bookingconsultation.route');
const teamAmin = require('./admin/team.route');
const settingsRoute = require('./admin/setting.route');
const orderpayment = require('./frontend/payment.route');
const orders = require('./frontend/order.route');
const adminOrder = require("./admin/ordermanager.route")
const adminColour = require("./admin/colourstories.route");
// const bookingconsultationAdmin = require('./admin/bookingconsultation.route');
const usersAdmin = require('./admin/user.route');
const tax = require("./admin/tax.route")

const usersColourist = require('./colourist/manage.route');
const cart = require('./frontend/cart.route');
const sitesettings = require('./admin/sitesettings.route');
// console.log(usersColourist);

const router = express.Router();
router.use('/card', cardRoute);
router.use('/docs', docsRoute);
router.use('/docs', docsRoute);
router.use('/profile', profileRoute);
router.use('/auth', authRoute);
router.use('/users', userRoute);
router.use('/address', addressRoute);
router.use('/wishlist', wishlistRoute);
router.use('/productreview', productreviewRoute);
router.use('/bookingconsultation', bookingconsultationRoute);
router.use('/page', pageFronEnd);
/* Admin Route */
router.use('/admin/product', productRoute);
router.use('/admin/attribute', attributeRoute);
router.use('/admin/productcategory', productcategoryRoute);
router.use('/admin/coupon', admincouponRoute);
router.use('/admin/videopost', videopostRoute);
router.use('/admin/post', postRoute);
router.use('/admin/page', pageRoute);
router.use('/admin/setting', settingsRoute);
/**
 * Image Upload
 */
router.use('/admin/image', imageRoute);
/**
 * Front End Post Route
 */
router.use('/post', postFrontEnd);
/**
 * bookingconsultation Admin Route
 */
router.use('/admin/bookingconsultation', bookingconsultationAdmin);
router.use('/post', postFrontEnd);
/**
 * bookingconsultation Admin Route
 */
// router.use('/admin/bookingconsultation', bookingconsultationAdmin);
/**
 * User Admin API
 */
/**
 * Team Route
 */
router.use('/team', teamRoute);
router.use('/admin/team', teamAmin);
/**
 * Team Route ends
 */
router.use('/admin/user', usersAdmin);
router.use('/admin/tax', tax);
router.use('/admin/order', adminOrder);
router.use('/admin/colour', adminColour);
router.use('/admin/settings', sitesettings);
/**
 * Contact Route
 */
router.use('/contact', contact);
/**
 * Product Frontend
 */
router.use('/product', productFronend);

/* Colourist */
router.use('/colourist', usersColourist);
router.use('/file', uploadRoute);
/**
 * Cart
 */
router.use('/cart',cart)

//cart payments
router.use('/orderpayment',orderpayment)
router.use('/orders',orders)

//colour list
router.use('/colourStories',colourStoriesRoute)
module.exports = router;
