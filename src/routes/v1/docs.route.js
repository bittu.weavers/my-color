const express = require('express');
const swaggerJsdoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');
const swaggerDefinition = require('../../docs/swaggerDef');

const router = express.Router();

const specs1 = swaggerJsdoc({
  swaggerDefinition,
  apis: ['./src/docs/*.yml', './src/routes/v1/frontend/*.js'],
});

const specs2 = swaggerJsdoc({
  swaggerDefinition,
  apis: ['./src/docs/*.yml', './src/routes/v1/admin/*.js'],
});

const options = {
  customCss: '.swagger-ui .topbar { display: none }',
  customSiteTitle: 'My Color',
  customfavIcon: '/assets/favicon.ico',
  explorer: true,
};

const swaggerHtmlV1 = swaggerUi.generateHTML(specs1, options);
const swaggerHtmlV2 = swaggerUi.generateHTML(specs2, options);

router.use('/frontend', swaggerUi.serveFiles(specs1, options));
router.get('/frontend', (req, res) => {
  res.send(swaggerHtmlV1);
});

router.use('/admin', swaggerUi.serveFiles(specs2, options));
router.get('/admin', (req, res) => {
  res.send(swaggerHtmlV2);
});

module.exports = router;
