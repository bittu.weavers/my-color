const express = require('express');
const auth = require('../../../middlewares/auth');
const validate = require('../../../middlewares/validate');
const bookingconsultationValidation = require('../../../validations/bookingconsultation.validation');
const bookingconsultationController = require('../../../controllers/bookingconsultation.controller');

const router = express.Router();
router.use(auth('manageColourist'))
router.post('/save-availability-slots',bookingconsultationController.addBookingSlots);
router.get('/get-availability-slots',bookingconsultationController.getColouristBookingSlots);
router.get('/bookingconsultation-details/:id', bookingconsultationController.bookingconsultationdetailsAdmin);
router.get('/bookingconsultation-list/:pageNumber?', bookingconsultationController.getColouristAllBooking);
router.patch(
  '/bookingconsultation-update/:id',
  validate(bookingconsultationValidation.bookingconsultationupdateAdmin),
  bookingconsultationController.bookingconsultationupdateAdmin
);

module.exports = router;