const express = require('express');
const auth = require('../../middlewares/auth');
const validate = require('../../middlewares/validate');
const uploadFile = require('../../middlewares/imageUpload.middleware');
const userController = require('../../controllers/user.controller');
const userValidation = require('../../validations/user.validation');

const router = express.Router();
router.use(auth('manageProfile'));
router.get('/edit-profile', userController.editProfile);
router.patch(
  '/update-profile',
  validate(userValidation.updateProfile),
  uploadFile('profileimage'),
  userController.updateProfile
);
// router.route('/update-image').patch(auth('manageProfile'),uploadFile('profile_image'), userController.editProfile);
module.exports = router;
