const express = require('express');
const multer = require('multer');
const imageController = require('../../controllers/image.controller');
const validate = require('../../middlewares/validate');
const auth = require('../../middlewares/auth');
const imageValidation = require('../../validations/image.validation');

const router = express.Router();
const useragent = require('express-useragent');

router.use(useragent.express());
const imageStorage = multer.diskStorage({
    // Destination to store image
    destination: 'public/images',
    filename: (req, file, cb) => {
        cb(null, `${Date.now()}_${file.originalname}`);
        // file.fieldname is name of the field (image)
        // path.extname get the uploaded file extension
    },
});
const imageUpload = multer({
    storage: imageStorage,
    fileFilter(req, file, cb) {
        /*  if (!file.originalname.match(/\.(png|jpg|svg)$/)) {
           // upload only png and jpg format
           return cb(new Error('Please upload a Image'));
         } */
        cb(undefined, true);
    },
});
router.route('/upload').post(auth('manageImage'), imageUpload.single('image'), imageController.uploadImage);
// router.post('/get/images',imageController.getImages);
router.route('/upload').delete(auth('manageImage'), validate(imageValidation.deleteImage), imageController.deleteImage);
module.exports = router;
/**
 * @swagger
 * paths:
 *  /admin/image/upload:
 *    post:
 *      summary: Upload Image
 *      tags: [Image]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - image
 *                - path
 *              properties:
 *                image:
 *                  type: object
 *                path:
 *                  type: string
 *              example:
 *                path: "image/page"
 *      responses:
 *        "200":
 *          description: Image Uploaded Successfully
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/MessageType'
 *              example:
 *               code: 200
 *               message: Image Uploaded Successfully
 */
/**
 * @swagger
 * paths:
 *  /admin/image/delete:
 *    delete:
 *      summary: Delete Image
 *      tags: [Image]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - path
 *                - type
 *                - id
 *              properties:
 *                path:
 *                  type: string
 *                type:
 *                  type: string
 *                id:
 *                  type: string
 *              example:
 *                path: "image/page"
 *      responses:
 *        "200":
 *          description: Image Deleted Successfully
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/MessageType'
 *              example:
 *               code: 200
 *               message: Image Deleted Successfully
 */
