const express = require('express');
const validate = require('../../../middlewares/validate');
const attributeController = require('../../../controllers/attribute.controller');
const attributeValidation = require('../../../validations');
const auth = require('../../../middlewares/auth');
const uploadFile = require('../../../middlewares/imageUpload.middleware');

const router = express.Router();
router.use(auth('manageAdmin'));

router.post('/add', validate(attributeValidation.addAttribute), uploadFile('attrimage'), attributeController.addAttribute);
router.get('/list/', attributeController.getAttributeList);
router.get('/edit/:id', attributeController.editAttribute);
router.delete('/delete/:id', attributeController.deleteAttribute);
router.patch('/update/:id',uploadFile('attrimage'), attributeController.updateAttribute);
router.post('/get-attrs/', attributeController.getAttrsListById);
module.exports = router;
