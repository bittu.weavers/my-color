const express = require('express');
const validate = require('../../../middlewares/validate');
const videopostValidation = require('../../../validations/videopost.validation');
const videopostController = require('../../../controllers/videopost.controller');
const auth = require('../../../middlewares/auth');

const router = express.Router();
router.use(auth('manageAdmin'));
router.post('/add-video', validate(videopostValidation.addVideo), videopostController.addVideo);
router.post('/get-video-list', videopostController.getVideoList);
router.post('/edit-video', validate(videopostValidation.editVideo), videopostController.editVideo);
router.patch('/update-video', validate(videopostValidation.updateVideo), videopostController.updateVideo);
router.delete('/delete-video', validate(videopostValidation.deleteVideo), videopostController.deleteVideo);
module.exports = router;

/**
 * @swagger
 * paths:
 *  /admin/videopost/add-video:
 *    post:
 *      summary: Add Video
 *      tags: [Video]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - video_title
 *                - video_description
 *                - video_image
 *                - video_url
 *                - is_active
 *              properties:
 *                video_title:
 *                  type: string
 *                video_description:
 *                  type: string
 *                video_image:
 *                  type: string
 *                video_url:
 *                  type: string
 *                is_active:
 *                  type: boolean
 *              example:
 *                video_title: "Test Video"
 *                video_description: "Something!"
 *                video_image: ""
 *                video_url: ""
 *                is_active: true/false
 *      responses:
 *        "200":
 *          description: Video Added Successfully
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/MessageType'
 *              example:
 *               code: 200
 *               message: Video Added Successfully
 */
/**
 * @swagger
 * paths:
 *  /admin/videopost/get-video-list:
 *    post:
 *      summary: Video List
 *      tags: [Video]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *      responses:
 *        "200":
 *          description: Success
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/MessageType'
 *              example:
 *               code: 200
 *               message: Success
 */

/**
 * @swagger
 * paths:
 *  /admin/videopost/edit-video:
 *    post:
 *      summary: Edit Video
 *      tags: [Video]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - id
 *              properties:
 *                id:
 *                type: string
 *              example:
 *                id: "123456"
 *      responses:
 *        "200":
 *          description: Video Detail By ID
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/MessageType'
 *              example:
 *               code: 200
 *               message: Success
 */
/**
 * @swagger
 * paths:
 *  /admin/videopost/update-video:
 *    patch:
 *      summary: Update Video
 *      tags: [Video]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - id
 *                - video_title
 *                - video_description
 *                - video_image
 *                - video_url
 *                - is_active
 *              properties:
 *                id:
 *                  type: string
 *                video_title:
 *                  type: string
 *                video_description:
 *                  type: string
 *                video_image:
 *                  type: string
 *                video_url:
 *                  type: string
 *                is_active:
 *                  type: boolean
 *              example:
 *                id: "123456789"
 *                video_title: "Test Video"
 *                video_description: "Something!"
 *                video_image: ""
 *                video_url: ""
 *                is_active: true/false
 *      responses:
 *        "200":
 *          description: Video Updated Successfully
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/MessageType'
 *              example:
 *               code: 200
 *               message: Video Updated Successfully
 */
/**
 * @swagger
 * paths:
 *  /admin/videopost/delete-video:
 *    delete:
 *      summary: Delete Video
 *      tags: [Video]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - id
 *              properties:
 *                id:
 *                type: string
 *              example:
 *                id: "123456"
 *      responses:
 *        "200":
 *          description: Video deleted
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/MessageType'
 *              example:
 *               code: 200
 *               message: Video deleted successfully
 */
