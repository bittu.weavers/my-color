const express = require('express');
const validate = require('../../../middlewares/validate');
const bookingconsultationValidation = require('../../../validations/bookingconsultation.validation');
const bookingconsultationController = require('../../../controllers/bookingconsultation.controller');
const auth = require('../../../middlewares/auth');

const router = express.Router();
router.use(auth('manageAdmin'));

router.delete(
  '/bookingconsultation-delete',
  validate(bookingconsultationValidation.bookingconsultationdelete),
  bookingconsultationController.bookingconsultationdelete
);
router.get('/bookingconsultation-list/:pageNumber?', bookingconsultationController.getAllBooking);
router.patch(
  '/bookingconsultation-update/:id',
  validate(bookingconsultationValidation.bookingconsultationupdateAdmin),
  bookingconsultationController.bookingconsultationupdateAdmin
);
router.post(
  '/bookingconsultation-statuschange',
  validate(bookingconsultationValidation.bookingconsultationstatuschange),
  bookingconsultationController.bookingconsultationstatuschange
);
router.get('/bookingconsultation-details/:id', bookingconsultationController.bookingconsultationdetailsAdmin);
router.delete('/bookingconsultation-delete/:id', bookingconsultationController.bookingconsultationdeleteAdmin);

router.post('/save-availability-slots', bookingconsultationController.addBookingSlots);
router.get('/get-availability-slots/:userId', bookingconsultationController.getBookingSlots);
//router.delete('/delete-availability-slots/:id', bookingconsultationController.deleteAvailabilitySlots);

module.exports = router;
/**
 * @swagger
 * paths:
 *  /bookingconsultation/bookingconsultation-list:
 *    get:
 *      summary: List Booking Consultation
 *      tags: [Booking Consultation Admin]
 *      security:
 *        - bearerAuth: []
 *      responses:
 *        "200":
 *          description: Sucess ( Data will be fetch by user token)
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/MessageType'
 *              example:
 *               code: 200
 *               message: Sucess
 */
/**
 * @swagger
 * paths:
 *  /bookingconsultation/bookingconsultation-delete:
 *    delete:
 *      summary: Delete Booking Consultation
 *      tags: [Booking Consultation Admin]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - id
 *              properties:
 *                id:
 *                  type: string
 *              example:
 *               id: "61bb505a6de9ff155023e7d5"
 *      responses:
 *        "200":
 *          description: Consultation booking is deleted successfully
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/MessageType'
 *              example:
 *               code: 200
 *               message: Consultation booking is deleted successfully
 */
/**
 * @swagger
 * paths:
 *  /bookingconsultation/bookingconsultation-details:
 *    post:
 *      summary: Details Booking Consultation
 *      tags: [Booking Consultation Admin]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - id
 *              properties:
 *                id:
 *                  type: string
 *              example:
 *               id: "61bb505a6de9ff155023e7d5"
 *      responses:
 *        "200":
 *          description: Sucess
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/MessageType'
 *              example:
 *               code: 200
 *               message: Sucess
 */
/**
 * @swagger
 * paths:
 *  /bookingconsultation/bookingconsultation-statuschange:
 *    post:
 *      summary: Booking Consultation Status Change
 *      tags: [Booking Consultation Admin]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - id
 *                - bookingstatus
 *              properties:
 *                id:
 *                  type: string
 *              example:
 *               id: "61bb505a6de9ff155023e7d5"
 *               bookingstatus: "Booked"
 *      responses:
 *        "200":
 *          description: Consultation booking status updated successfully
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/MessageType'
 *              example:
 *               code: 200
 *               message: Consultation booking status updated successfully
 */
