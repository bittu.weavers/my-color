const express = require('express');
const validate = require('../../../middlewares/validate');
const orderValidation = require('../../../validations/order.validation');
const orderController = require('../../../controllers/order.controller');
const auth = require('../../../middlewares/auth');

const router = express.Router()

router.use(auth('manageAdmin'));
router.patch('/order-status',validate(orderValidation.orderstatus),orderController.setOrderStatus);
router.get('/all-orders/:pageno/:orderId?',validate(orderValidation.filter),orderController.getAllOrdersAdmin);
router.post('/refund',validate(orderValidation.refund),orderController.refund);
router.get('/order-details/:id',validate(orderValidation.getorderid),orderController.orderDetails);

module.exports = router;


