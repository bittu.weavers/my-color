const express = require('express');
const taxController = require('../../../controllers/tax.controller');
const taxValidator = require('../../../validations/tax.validation');
const validate = require('../../../middlewares/validate');
const auth = require('../../../middlewares/auth');
const router = express.Router();

router.put('/set-tax', auth('manageAdmin'),validate(taxValidator.tax),taxController.addSetting);
router.put('/new-tax',auth('manageAdmin'), validate(taxValidator.add),taxController.addNewTax);
router.get('/get-tax/:id',auth('manageAdmin'), validate(taxValidator.gettax),taxController.getTax);
router.delete('/del-tax/:id',auth('manageAdmin'), validate(taxValidator.gettax),taxController.deleteTax);
router.get('/get-all-tax',taxController.getAlltax);
router.post('/filter-tax',validate(taxValidator.filter),taxController.filterTax);

module.exports = router;
