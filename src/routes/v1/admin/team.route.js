const express = require('express');
const validate = require('../../../middlewares/validate');
const teamController = require('../../../controllers/team.controller');
const teamvalidation = require('../../../validations/team.validation');
const auth = require('../../../middlewares/auth');
const uploadFile = require('../../../middlewares/imageUpload.middleware');

const router = express.Router();
router.use(auth('manageAdmin'));
router.post('/add', validate(teamvalidation.addTeam), uploadFile('teamImage'), teamController.createTeam);
router.get('/edit/:id', teamController.editTeam);
router.put('/update/:id', validate(teamvalidation.editTeam), uploadFile('teamImage'), teamController.updateTeam);
router.delete('/delete/:id', teamController.deleteTeam);

module.exports = router;
