const express = require('express');
const validate = require('../../../middlewares/validate');
const auth = require('../../../middlewares/auth');
const settingsValidator = require("../../../validations/sitesettings.validation");
const settingscontroller = require("../../../controllers/sitesettings.controller");
const uploadFile = require('../../../middlewares/imageUpload.middleware');

const router = express.Router();

router.get('/getsitedata', settingscontroller.getLogo);
router.post('/addheaderlogo',auth('manageAdmin'), validate(settingsValidator.addLogo),uploadFile('logo'),settingscontroller.addSiteLogo);
router.post('/addfooterlogo',auth('manageAdmin'), validate(settingsValidator.addLogo),uploadFile('logo'),settingscontroller.addSiteFooterLogo);
router.post('/addsocial',auth('manageAdmin'), validate(settingsValidator.addSocial),settingscontroller.addSiteSocial);
router.put('/updatesocial',auth('manageAdmin'), validate(settingsValidator.addSocial),settingscontroller.updateSocial);
router.get('/getSocial/:objectId/:id',auth('manageAdmin'),settingscontroller.getSocial);
router.delete('/deleteSocial/:objectId/:id',auth('manageAdmin'),settingscontroller.deleteSocial);
router.put('/footer',auth('manageAdmin'),validate(settingsValidator.footer),settingscontroller.setfooter)
router.put('/header',auth('manageAdmin'),validate(settingsValidator.header),settingscontroller.setheader)


module.exports = router;
