const express = require('express');
const validate = require('../../../middlewares/validate');
const couponValidation = require('../../../validations/coupon.validation');
const couponController = require('../../../controllers/coupon.controller');
const auth = require('../../../middlewares/auth');

const router = express.Router();
router.use(auth('manageAdmin'));
router.post('/add-coupon', validate(couponValidation.addCouponCode), couponController.addCouponCode);
router.get('/get-coupon-list/:current_page?/:pagesize?', couponController.getCouponList);
router.get('/edit-coupon/:id', couponController.editCoupon);
router.patch('/update-coupon/:id', validate(couponValidation.updateCoupon), couponController.updateCoupon);
router.delete('/delete-coupon/:id',  couponController.deleteCoupon);
router.get('/generate-coupon-code',  couponController.generate_Coupon_code);
module.exports = router;

/**
 * @swagger
 * paths:
 *  /admin/coupon/add-coupon:
 *    post:
 *      summary: Add Coupon
 *      tags: [Coupon]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - coupon_code
 *                - coupon_start_date
 *                - coupon_expiry_date
 *                - coupon_amount
 *              properties:
 *                coupon_code:
 *                  type: string
 *                coupon_start_date:
 *                  type: date
 *                coupon_expiry_date:
 *                  type: date
 *                coupon_amount:
 *                  type: number
 *              example:
 *                coupon_code: "COUPON0100"
 *                coupon_start_date: "2022-01-19"
 *                coupon_expiry_date: "2022-01-30"
 *                coupon_amount: 500
 *      responses:
 *        "200":
 *          description: Coupon Added
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/MessageType'
 *              example:
 *               code: 200
 *               message: Coupon added successfully
 */

/**
 * @swagger
 * paths:
 *  /admin/coupon/get-coupon-list:
 *    post:
 *      summary: List Coupon
 *      tags: [Coupon]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *      responses:
 *        "200":
 *          description: Success
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/MessageType'
 *              example:
 *               code: 200
 *               message: Success
 */

/**
 * @swagger
 * paths:
 *  /admin/coupon/edit-coupon:
 *    post:
 *      summary: Edit Coupon
 *      tags: [Coupon]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - id
 *              properties:
 *                id:
 *                type: string
 *              example:
 *                id: "123456"
 *      responses:
 *        "200":
 *          description: Coupon Detail By ID
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/MessageType'
 *              example:
 *               code: 200
 *               message: Success
 */

/**
 * @swagger
 * paths:
 *  /admin/coupon/update-coupon:
 *    patch:
 *      summary: Update Coupon
 *      tags: [Coupon]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - id
 *                - coupon_code
 *                - coupon_start_date
 *                - coupon_expiry_date
 *                - coupon_amount
 *                - coupon_percentage_value
 *                - is_percentage
 *              properties:
 *                id:
 *                  type: string
 *                coupon_code:
 *                  type: string
 *                coupon_start_date:
 *                  type: date
 *                coupon_expiry_date:
 *                  type: date
 *                coupon_amount:
 *                  type: number
 *                coupon_percentage_value:
 *                  type: number
 *                is_percentage:
 *                  type: boolean
 *              example:
 *                id: "123456"
 *                coupon_code: "COUPON0100"
 *                coupon_start_date: "2022-01-19"
 *                coupon_expiry_date: "2022-01-30"
 *                coupon_amount: 500
 *                coupon_percentage_value: 10
 *                is_percentage: true
 *      responses:
 *        "200":
 *          description: Coupon updated
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/MessageType'
 *              example:
 *               code: 200
 *               message: Coupon updated successfully
 */

/**
 * @swagger
 * paths:
 *  /admin/coupon/delete-coupon:
 *    delete:
 *      summary: Delete Coupon
 *      tags: [Coupon]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - id
 *              properties:
 *                id:
 *                type: string
 *              example:
 *                id: "123456"
 *      responses:
 *        "200":
 *          description: Coupon deleted
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/MessageType'
 *              example:
 *               code: 200
 *               message: Coupon deleted successfully
 */
