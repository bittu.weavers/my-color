const express = require('express');
const validate = require('../../../middlewares/validate');
const productController = require('../../../controllers/product.controller');
const productValidation = require('../../../validations');
const auth = require('../../../middlewares/auth');
const uploadFile = require('../../../middlewares/imageUpload.middleware');

const router = express.Router();
router.use(auth('manageAdmin'));

// Product
router.post('/add', validate(productValidation.addProduct), uploadFile('product_image'), productController.addProduct);
router.patch('/update/:id', validate(productValidation.updateProduct), uploadFile('product_image'), productController.updateProduct);
router.get('/list/:current_page?', productController.getProductList);
router.get('/all-product-list', productController.getAllProductList);
router.get('/edit/:id', productController.editProduct);
router.delete('/delete/:id', productController.deleteProduct);

// Product Review
router.post('/review/add', validate(productValidation.addReview), productController.addReview);
router.get('/review/:pid/:current_page?', productController.getProductReviewById);
router.get('/reviewdetails/:rid', productController.getProductReviewDetails);
router.patch('/reviewstatus', validate(productValidation.reviewStatusUpdate), productController.reviewStatusUpdate);
router.delete('/reviewdelete/:id', productController.deleteReview);
module.exports = router;
