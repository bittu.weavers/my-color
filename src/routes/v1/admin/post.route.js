const express = require('express');
const validate = require('../../../middlewares/validate');
const postValidation = require('../../../validations/post.validation');
const postController = require('../../../controllers/post.controller');
const auth = require('../../../middlewares/auth');
const uploadFile = require('../../../middlewares/imageUpload.middleware');
const router = express.Router();
router.use(auth('manageAdmin'));
router.post('/add-post', validate(postValidation.addPost), uploadFile('image'), postController.addPost);
router.get('/get-post-list/:post_type/:current_page?', postController.getPostList);
router.get('/make-primary/:id/:post_type', postController.makePrimary);
router.get('/edit-post/:id', validate(postValidation.editPost), postController.editPost);
router.patch('/update-post/:id', validate(postValidation.updatePost), uploadFile('image'), postController.updatePost);
router.delete('/delete-post/:id', validate(postValidation.deletePost), postController.deletePost);
module.exports = router;

/**
 * @swagger
 * paths:
 *  /admin/post/add-post:
 *    post:
 *      summary: Add Post
 *      tags: [Post]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - title
 *                - description
 *                - image
 *                - url
 *                - is_active
 *                - post_type
 *              properties:
 *                title:
 *                  type: string
 *                description:
 *                  type: string
 *                image:
 *                  type: string
 *                url:
 *                  type: string
 *                is_active:
 *                  type: boolean
 *                post_type:
 *                  type: string
 *              example:
 *                title: "Test Video"
 *                description: "Something!"
 *                image: ""
 *                url: ""
 *                is_active: true/false
 *                post_type: "Video"
 *      responses:
 *        "200":
 *          description: Post Added Successfully
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/MessageType'
 *              example:
 *               code: 200
 *               message: Post Added Successfully
 */
/**
 * @swagger
 * paths:
 *  /admin/post/get-post-list/current_page?:
 *    get:
 *      summary: Post List
 *      tags: [Post]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *      responses:
 *        "200":
 *          description: Success
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/MessageType'
 *              example:
 *               code: 200
 *               message: Success
 */

/**
 * @swagger
 * paths:
 *  /admin/post/edit-post/:id:
 *    get:
 *      summary: Edit Post
 *      tags: [Post]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - id
 *              properties:
 *                id:
 *                type: string
 *              example:
 *                id: "123456"
 *      responses:
 *        "200":
 *          description: Post Detail By ID
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/MessageType'
 *              example:
 *               code: 200
 *               message: Success
 */
/**
 * @swagger
 * paths:
 *  /admin/post/update-post:
 *    patch:
 *      summary: Update Post
 *      tags: [Post]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - id
 *                - title
 *                - description
 *                - image
 *                - url
 *                - is_active
 *              properties:
 *                id:
 *                  type: string
 *                title:
 *                  type: string
 *                description:
 *                  type: string
 *                image:
 *                  type: string
 *                url:
 *                  type: string
 *                is_active:
 *                  type: boolean
 *              example:
 *                id: "123456789"
 *                title: "Test Video"
 *                description: "Something!"
 *                image: ""
 *                url: ""
 *                is_active: true/false
 *      responses:
 *        "200":
 *          description: Video Updated Successfully
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/MessageType'
 *              example:
 *               code: 200
 *               message: Video Updated Successfully
 */
/**
 * @swagger
 * paths:
 *  /admin/post/delete-post:
 *    delete:
 *      summary: Delete Video
 *      tags: [Post]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - id
 *              properties:
 *                id:
 *                type: string
 *              example:
 *                id: "123456"
 *      responses:
 *        "200":
 *          description: Post deleted
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/MessageType'
 *              example:
 *               code: 200
 *               message: Post deleted successfully
 */

/**
 * @swagger
 * paths:
 *  /admin/post/make-primary/id?:
 *    get:
 *      summary: Make a post feature
 *      tags: [Post]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: false
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *      responses:
 *        "200":
 *          description: Success
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/MessageType'
 *              example:
 *               code: 200
 *               message: Success
 */
