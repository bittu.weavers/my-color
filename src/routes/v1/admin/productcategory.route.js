const express = require('express');
const validate = require('../../../middlewares/validate');
const productcategoryValidation = require('../../../validations/productcategory.validation');
const productcategoryController = require('../../../controllers/productcategory.controller');
const auth = require('../../../middlewares/auth');

const router = express.Router();
router.use(auth('manageAdmin'));
router.post(
  '/add-productcategory',
  validate(productcategoryValidation.productcategoryAdd),
  productcategoryController.productcategoryAdd
);
router.get('/productcategory-list/:current_page?', productcategoryController.getCategoryListsData);
router.get('/getproductcategories/', productcategoryController.getCategoryList);
router.delete('/delete-productcategory/:id', productcategoryController.deleteProductcategory);
router.get('/edit-productcategory/:id', productcategoryController.editProductcategory);
router.patch(
  '/update-productcategory/:id',
  validate(productcategoryValidation.updateProductcategory),
  productcategoryController.updateProductcategory
);
router.get('/status-productcategory/:id', productcategoryController.statusProductcategory);
module.exports = router;

/**
 * @swagger
 * paths:
 *  /admin/productcategory/add-productcategory:
 *    post:
 *      summary: Add Product Category
 *      tags: [Product Category]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - category_name
 *              properties:
 *                category_name:
 *                  type: string
 *              example:
 *                category_name: First fake
 *      responses:
 *        "200":
 *          description: Category Added
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/MessageType'
 *              example:
 *               code: 200
 *               message: Product Category added successfully
 */

/**
 * @swagger
 * paths:
 *  /admin/productcategory/productcategory-list:
 *    post:
 *      summary: List Product Category
 *      tags: [Product Category]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *      responses:
 *        "200":
 *          description: Success
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/MessageType'
 *              example:
 *               code: 200
 *               message: Success
 */

/**
 * @swagger
 * paths:
 *  /admin/productcategory/edit-productcategory:
 *    post:
 *      summary: Edit Product Category
 *      tags: [Product Category]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - id
 *              properties:
 *                id:
 *                type: string
 *              example:
 *                id: "123456"
 *      responses:
 *        "200":
 *          description: Category List By ID
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/MessageType'
 *              example:
 *               code: 200
 *               message: Success
 */

/**
 * @swagger
 * paths:
 *  /admin/productcategory/update-productcategory:
 *    patch:
 *      summary: Update Product Category
 *      tags: [Product Category]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - id
 *                - category_name
 *              properties:
 *                id:
 *                type: string
 *                category_name:
 *                  type: string
 *              example:
 *                id: "123456"
 *                category_name: First fake
 *      responses:
 *        "200":
 *          description: Category updated
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/MessageType'
 *              example:
 *               code: 200
 *               message: Product Category updated successfully
 */

/**
 * @swagger
 * paths:
 *  /admin/productcategory/delete-productcategory:
 *    delete:
 *      summary: Delete Product Category
 *      tags: [Product Category]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - id
 *              properties:
 *                id:
 *                type: string
 *              example:
 *                id: "123456"
 *      responses:
 *        "200":
 *          description: Category deleted
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/MessageType'
 *              example:
 *               code: 200
 *               message: Product Category deleted successfully
 */

/**
 * @swagger
 * paths:
 *  /admin/productcategory/status-productcategory:
 *    post:
 *      summary: Product Category Status Change
 *      tags: [Product Category]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - id
 *                - is_active
 *              properties:
 *                id:
 *                 type: string
 *                is_active:
 *                 type: boolean
 *              example:
 *                id: "123456"
 *                is_active: true
 *      responses:
 *        "200":
 *          description: Category Status Chnage
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/MessageType'
 *              example:
 *               code: 200
 *               message: Product Category status has been changed successfully
 */
