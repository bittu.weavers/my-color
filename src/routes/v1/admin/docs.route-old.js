const express = require('express');
const swaggerJsdoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');
const swaggerDefinition = require('../../../docs/swaggerDef');

const router = express.Router();

const specs = swaggerJsdoc({
  swaggerDefinition,
  apis: ['./src/docs/*.yml', './src/routes/v1/admin/*.js'],
});

const options = {
  customCss: '.swagger-ui .topbar { display: none }',
  customSiteTitle: 'My Color',
  customfavIcon: '/assets/favicon.ico',
  explorer: true,
};

router.use('/', swaggerUi.serve);
router.get('/', swaggerUi.setup(specs, options));

module.exports = router;
