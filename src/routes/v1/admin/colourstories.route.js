const express = require('express');
const validate = require('../../../middlewares/validate');
const auth = require('../../../middlewares/auth');
const colourValidator = require("../../../validations/colourstories.validation");
const colourstoriesController = require("../../../controllers/colourstory.controller");
const uploadFile = require('../../../middlewares/imageUpload.middleware');

const router = express.Router();
router.use(auth('manageAdmin'));

router.post('/add', validate(colourValidator.addColourStory),uploadFile('image'),colourstoriesController.addStory);
router.get('/list/:current_page?', colourstoriesController.showList);
router.get('/story/:id', validate(colourValidator.story),colourstoriesController.getStory);
router.put('/edit', validate(colourValidator.updateStory),uploadFile('image'),colourstoriesController.updateStory);
router.delete('/delete/:id', validate(colourValidator.story),colourstoriesController.deleteStory);


module.exports = router;
