const express = require('express');
const validate = require('../../../middlewares/validate');
const userController = require('../../../controllers/user.controller');
const userValidation = require('../../../validations/user.validation');
const auth = require('../../../middlewares/auth');
const uploadFile = require('../../../middlewares/imageUpload.middleware');

const router = express.Router();
router.use(auth('manageAdmin'));
router.post('/get-users/:current_page',validate(userValidation.getUserList), userController.getUserList);
router.post('/add-user', validate(userValidation.addUser),uploadFile('profileimage'), userController.addUser);
router.get('/edit-user/:userId',userController.editUser);
router.patch('/update-user/:userId', validate(userValidation.updateAdminUser),uploadFile('profileimage'), userController.updateAdminUser);
router.delete('/delete-user/:userId',userController.deleteUser);
module.exports = router;
/**
 * @swagger
 * paths:
 *  /admin/user/get-users/current_page?:
 *    get:
 *      summary: User List
 *      tags: [User Admin]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *      responses:
 *        "200":
 *          description: Success
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/MessageType'
 *              example:
 *               code: 200
 *               message: Success
 */
