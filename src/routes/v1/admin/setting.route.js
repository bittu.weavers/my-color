const express = require('express');
const settingController = require('../../../controllers/setting.controller');

const router = express.Router();

router.post('/add', settingController.addSetting);

module.exports = router;
