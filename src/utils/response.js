class Response {
  constructor(data, statusCode = 200, msg = 'Success') {
    this.data = data;
    this.serverResponse = {
      code: statusCode,
      message: msg,
    };
  }

  view() {
    return {
      serverResponse: this.serverResponse,
      result: this.data,
    };
  }

  paginate() {
    return {
      serverResponse: this.serverResponse,
      totalPages: this.data.totalPages,
      totalRecords: this.data.totalResults,
      list: this.data.results,
    };
  }
}
module.exports = { Response };
