const sgMail = require('@sendgrid/mail');
const config = require('../config/config');
/**
 * Send an email via
 * @param {string} to
 * @param {string} subject
 * @param {string} text
 * @returns {Promise}
 */
const sendEmail = async (mailData) => {
  sgMail.setApiKey(config.email.sendGridApiKey);
  sgMail.send(mailData).then(
    () => {
      // eslint-disable-next-line no-console
      console.log('mail sent');
    },
    (error) => {
      if (error.response) {
        // eslint-disable-next-line no-console
        console.error(error.response.body);
      }
    }
  );
};
module.exports = {
  sendEmail,
};
