const Joi = require('joi');

const tax = {
  body: Joi.object().keys({
    id:Joi.string().required(),
    country: Joi.string().required(),
    state: Joi.string().optional().allow(""),
    tax: Joi.number().required(),
  }),
};

const add = {
  body: Joi.object().keys({
    country: Joi.string().required(),
    state: Joi.string().optional().allow(""),
    tax: Joi.number().required(),
  }),
};


const gettax = {
  params: Joi.object().keys({
    id: Joi.string().required()
  }),
};


const filter = {
  body: Joi.object().keys({
    country: Joi.string().required(),
    state: Joi.string().optional().allow("")
  }),
};




module.exports = {
    tax,
    gettax,
    add,
    filter
}