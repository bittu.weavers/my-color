const Joi = require('joi');
const { email } = require('./custom.validation');
// sendMail
const sendMail = {
  body: Joi.object().keys({
    first_name: Joi.string().required(),
    last_name: Joi.string().required(),
    email: Joi.string().required().custom(email),
    number: Joi.number().optional(),
    subject: Joi.string().required(),
    message: Joi.string().optional(),
  }),
};
module.exports = {
  sendMail,
};
