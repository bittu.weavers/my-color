const Joi = require('joi');
const { password, objectId, email } = require('./custom.validation');
// Get User Detail
const getUserDetail = {
  body: Joi.object().keys({
    userId: Joi.string().custom(objectId),
  }),
};

// Get User list
const getUserList = {
  body: Joi.object().keys({
    role:Joi.string().optional().allow(''),
    search:Joi.string().optional().allow(''),
  }),
};
// Update User
const updateUser = {
  body: Joi.object().keys({
    fname: Joi.string().required(),
    lname: Joi.string().required(),
    email: Joi.string().required().custom(email),
  }),
};
const updateUserImage={
  body: Joi.object().keys({
    profileimage: Joi.object().keys({
      filename: Joi.string().required(),
      file: Joi.string().required(),
      old_url: Joi.string().optional(),
    }),
  }),
}
// Change Password
const changePassword = {
  body: Joi.object().keys({
    email: Joi.string().required().messages({
      'string.email': 'Not a valid email address.',
      'string.empty': 'Email is required.',
    }),
    oldPassword: Joi.string().required(),
    newPassword: Joi.string().required().custom(password),
  }),
};
// Delete Social User
const deleteSocialUser = {
  body: Joi.object().keys({
    socialId: Joi.string().custom(objectId),
    usertype: Joi.string().required(),
  }),
};

const addUser = {
  body: Joi.object().keys({
    fname: Joi.string().required(),
    lname: Joi.string().required(),
    profileimage: Joi.optional(),
    email: Joi.string().required().custom(email),
    password: Joi.string().required().custom(password),
    role: Joi.string().required(),
  }),
};
const updateAdminUser = {
  body: Joi.object().keys({
    fname: Joi.string().required(),
    lname: Joi.string().required(),
    profileimage: Joi.optional(),
    password: Joi.string().optional(),
    role: Joi.string().required(),
  }),
};
const updateProfile = {
  body: Joi.object().keys({
    fname: Joi.string().required(),
    lname: Joi.string().required(),
    profileimage: Joi.optional(),
    password: Joi.string().optional(),
  }),
};

module.exports = {
  getUserDetail,
  addUser,
  updateUser,
  changePassword,
  deleteSocialUser,
  updateAdminUser,
  updateProfile
};
