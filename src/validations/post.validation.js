const Joi = require('joi');

// Add coupon Code
const addPost = {
  body: Joi.object().keys({
    title: Joi.string().required(),
    description: Joi.string().allow('').optional(),
    post_type: Joi.string().required(),
    image: Joi.allow('').optional(),
    is_active: Joi.boolean().required(),
    short_description:Joi.allow('').optional(),
    sub_title:Joi.allow('').optional()
  }),
};
// Delete Post
const deletePost = {
  params: Joi.object().keys({
    id: Joi.string().required(),
  }),
};
// Edit Post
const editPost = {
  params: Joi.object().keys({
    id: Joi.string().required(),
  }),
};
// Update Post
const updatePost = {
  params: Joi.object().keys({
    id: Joi.string().required(),
  }),
  body: Joi.object().keys({
    title: Joi.string().required(),
    description: Joi.string().allow('').optional(),
    post_type: Joi.string().required(),
    image: Joi.allow('').optional(),
    is_active: Joi.boolean().required(),
    short_description:Joi.allow('').optional(),
    sub_title:Joi.allow('').optional()
  }),
};
module.exports = {
  addPost,
  deletePost,
  editPost,
  updatePost,
};
