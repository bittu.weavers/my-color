const Joi = require("joi")

const payme = {
    body: Joi.object().keys({
        token: Joi.string().required(),
        orderData: Joi.object().required()
    }),
};

module.exports = {
    payme,
};
