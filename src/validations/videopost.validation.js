const Joi = require('joi');

// Add coupon Code
const addVideo = {
  body: Joi.object().keys({
    video_title: Joi.string().required(),
    video_description: Joi.string().allow('').optional(),
    video_image: Joi.string().required(),
    video_url: Joi.number().required(),
    is_active: Joi.boolean().required(),
  }),
};
// Delete Video
const deleteVideo = {
  body: Joi.object().keys({
    id: Joi.string().required(),
  }),
};
// Edit Video
const editVideo = {
  body: Joi.object().keys({
    id: Joi.string().required(),
  }),
};
// Update Video
const updateVideo = {
  body: Joi.object().keys({
    id: Joi.string().required(),
    video_title: Joi.string().required(),
    video_description: Joi.string().allow('').optional(),
    video_image: Joi.string().required(),
    video_url: Joi.number().required(),
    is_active: Joi.boolean().required(),
  }),
};
module.exports = {
  addVideo,
  deleteVideo,
  editVideo,
  updateVideo,
};
