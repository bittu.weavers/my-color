const Joi = require('joi');


const addColourStory = {
  body: Joi.object().keys({
    image: Joi.required(),
    description:Joi.string().required(),
    rating_count:Joi.number().required(),
    first_name:Joi.string().required(),
    last_name:Joi.string().required(),
    since:Joi.string().required()
  }),
};

const story = {
  params: Joi.object().keys({
    id: Joi.string().required()
  }),
};

const updateStory = {
  body: Joi.object().keys({
    id:Joi.string().required(),
    image: Joi.required(),
    description:Joi.string().required(),
    rating_count:Joi.number().required(),
    name:Joi.string().required(),
    since:Joi.string().required()
  }),
};


module.exports = {
    addColourStory,
    story,
    updateStory
};
