const Joi = require('joi');


// Register
const details = {
  params: Joi.object().keys({
    orderid: Joi.string().required()
  }),
};



const cancellOrder = {
  body: Joi.object().keys({
    orderId: Joi.string().required()
  })
}


const filter = {
  params: Joi.object().keys({
    orderId: Joi.string(),
    pageno:Joi.string().required(),
  })
}


const orderstatus = {
  body: Joi.object().keys({
    orderId: Joi.string().required(),
    orderStatus:Joi.string().required(),
  })
}


const refund = {
  body: Joi.object().keys({
    orderId: Joi.string().required(),
    amount:Joi.number(),
    email:Joi.required()
  })
}


const getorderid = {
  params: Joi.object().keys({
    id: Joi.string().required()
  })
}

module.exports = {
    details,
    cancellOrder,
    filter,
    orderstatus,
    refund,
    getorderid
}