const Joi = require('joi');

// Add Product Review
const productReviewAdd = {
  body: Joi.object().keys({
    productid: Joi.string().required(),
    userid: Joi.string().required(),
    rating_count: Joi.number().required(),
    review_desc: Joi.string().required(),
    orderid: Joi.string().required(),
  }),
};

// Change Product Review Status
const productreviewstatus = {
  body: Joi.object().keys({
    id: Joi.string().required(),
    review_status: Joi.boolean().required(),
  }),
};

// Delete Product Review
const deleteproductreview = {
  body: Joi.object().keys({
    id: Joi.string().required(),
  }),
};

module.exports = {
  productReviewAdd,
  productreviewstatus,
  deleteproductreview,
};
