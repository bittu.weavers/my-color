const Joi = require('joi');

// Add coupon Code
const addPage = {
    body: Joi.object().keys({
        title: Joi.string().required(),
        content: Joi.array().optional(),
    }),
};
// Delete Page
const deletePage = {
    params: Joi.object().keys({
        id: Joi.string().required(),
    }),
};
// Edit Page
const editPage = {
    params: Joi.object().keys({
        id: Joi.string().required(),
    }),
};
// Update Page
const updatePage = {
    body: Joi.object().keys({
        title: Joi.string().optional(),
        content: Joi.array().optional(),
    }),
};
module.exports = {
    addPage,
    deletePage,
    editPage,
    updatePage,
};
