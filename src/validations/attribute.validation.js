const Joi = require('joi');

// Add Attribute
const addAttribute = {
  body: Joi.object().keys({
    name: Joi.string().required(),
    value: Joi.array().optional(),
  }),
};

module.exports = { addAttribute };
