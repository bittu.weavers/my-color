const Joi = require('joi');

// Add coupon Code
const addCouponCode = {
  body: Joi.object().keys({
    coupon_code: Joi.string().required(),
    coupon_start_date: Joi.date().required(),
    coupon_expiry_date: Joi.date().optional(),
    coupon_amount: Joi.number().optional().allow(null),
    coupon_percentage_value: Joi.number().optional().allow(null),
    is_percentage: Joi.boolean().required(),
    coupon_limit: Joi.number().optional().allow(null),
    product_for: Joi.array().optional().allow(null),
  }),
};
// Delete coupon
const deleteCoupon = {
  body: Joi.object().keys({
    id: Joi.string().required(),
  }),
};
// Edit Coupon
const editCoupon = {
  body: Joi.object().keys({
    id: Joi.string().required(),
  }),
};
// Update Coupon
const updateCoupon = {
  body: Joi.object().keys({
    coupon_code: Joi.string().required(),
    coupon_start_date: Joi.date().required(),
    coupon_expiry_date: Joi.date().optional(),
    coupon_amount: Joi.number().optional().allow(null),
    coupon_percentage_value: Joi.number().optional().allow(null),
    is_percentage: Joi.boolean().required(),
    coupon_limit: Joi.number().optional().allow(null),
    product_for: Joi.array().optional().allow(null),
  }),
};
module.exports = {
  addCouponCode,
  deleteCoupon,
  editCoupon,
  updateCoupon,
};
