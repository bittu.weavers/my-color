const Joi = require('joi');
// addTeam
const addTeam = {
  body: Joi.object().keys({
    name: Joi.string().required(),
    teamImage: Joi.object().required(),
    social: Joi.array().optional(),
  }),
};
const editTeam = {
  body: Joi.object().keys({
    name: Joi.string().required(),
    teamImage: Joi.object().required(),
    social: Joi.array().optional(),
  }),
};
module.exports = {
  addTeam,
  editTeam,
};
