const Joi = require('joi');

const addLogo = {
    body: Joi.object().keys({
        logo:Joi.required(),
        id:Joi.string().required(),
    }),
};

const addSocial = {
    body: Joi.object().keys({
        social: Joi.object().required(),
        id:Joi.string().required()
    }),
};


const footer = {
    body: Joi.object().keys({
        footer_text : Joi.string().required(),
        copyright_text:Joi.string().required(),
        insta_followlink:Joi.string().required(),
        id:Joi.string().required(),
    }),
};



const header = {
    body: Joi.object().keys({
        header_text : Joi.string().required(),
        id:Joi.string().required(),
    }),
};

module.exports = {
    addLogo,
    addSocial,
    footer,
    header
}