const Joi = require('joi');
// sendMail
const addToCart = {
  body: Joi.object().keys({
    cartId:Joi.string().optional().allow(""),
    userId:Joi.string().optional().allow(""),
    items:Joi.object().required(),
  }),
};


const cartQt = {
  body: Joi.object().keys({
    cartId:Joi.string().optional().allow("")
  }),
};


const assignId = {
  body: Joi.object().keys({
    userId: Joi.string().required(),
    cartId: Joi.string().required()
  })
}




const getCart = {
  body: Joi.object().keys({
    cartId: Joi.string().optional().allow("")
  })
}

const updatecart = {
  body: Joi.object().keys({
    cartId: Joi.string().required(),
    id: Joi.string().required(),
    qt:Joi.number().required()
  })
}
const removal = {
  body: Joi.object().keys({
    cartId: Joi.string().required(),
    itemId: Joi.string().required()
  })
}

const calculate = {
  body: Joi.object().keys({
    cartId: Joi.string().required().allow(""),
    country: Joi.string().allow(""),
    state: Joi.string().allow(""),
    couponcode:Joi.string().allow("")
  })
}


const coupon = {
  body: Joi.object().keys({
    cartId: Joi.string().required().allow(""),
    couponcode:Joi.string().required().allow("")
  })
}



module.exports = {
  addToCart,
  getCart,
  assignId,
  updatecart,
  removal,
  calculate,
  coupon,
  cartQt
};
