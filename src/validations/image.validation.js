const Joi = require('joi');

// Delete Image
const deleteImage = {
  body: Joi.object().keys({
    path: Joi.string().required(),
    type: Joi.string().allow('').optional(),
    id: Joi.string().allow('').optional(),
  }),
};

module.exports = {
  deleteImage,
};
