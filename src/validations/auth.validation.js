const Joi = require('joi');
const { password, email } = require('./custom.validation');

// Register
const register = {
  body: Joi.object().keys({
    fname: Joi.string().required(),
    lname: Joi.string().required(),
    email: Joi.string().required().custom(email),
    password: Joi.string().required().custom(password),
  }),
};

// Otp Login
const login = {
  body: Joi.object().keys({
    email: Joi.string().required().custom(email),
    password: Joi.string().required(),
  }),
};

// Otp Logout
const logout = {
  body: Joi.object().keys({
    refreshToken: Joi.string().required(),
  }),
};

// Refresh Token
const refreshToken = {
  body: Joi.object().keys({
    refreshToken: Joi.string().required(),
  }),
};

// Otp Forgot Password
const forgotPassword = {
  body: Joi.object().keys({
    email: Joi.string().required().custom(email),
  }),
};

// Otp Verifiaction
const otpVerification = {
  body: Joi.object().keys({
    otp: Joi.string().required(),
    email: Joi.string().required().custom(email),
  }),
};

// Reset Password
const resetPassword = {
  body: Joi.object().keys({
    password: Joi.string().required().custom(password),
    otp: Joi.string().required(),
    email: Joi.string().required().custom(email),
  }),
};

// Social Login
const socialLogin = {
  body: Joi.object().keys({
    socialId: Joi.string().required(),
    fname: Joi.string().required(),
    lname: Joi.optional(),
    profileimage: Joi.string().optional(),
    social_image:Joi.string().optional(),
    email: Joi.optional(),
    usertype: Joi.string().required(),
  }),
};
module.exports = {
  register,
  login,
  logout,
  otpVerification,
  forgotPassword,
  resetPassword,
  socialLogin,
  refreshToken,
};
