const Joi = require('joi');

// Add Product category
const productcategoryAdd = {
    body: Joi.object().keys({
        category_name: Joi.string().required(),
    }),
};

// Delete Product Category
const deleteProductcategory = {
    body: Joi.object().keys({
        id: Joi.string().required(),
    }),
};

// Edit Product Category
const editProductcategory = {
    body: Joi.object().keys({
        id: Joi.string().required(),
    }),
};

// Update Product Category
const updateProductcategory = {
    body: Joi.object().keys({
        category_name: Joi.string().required(),
    }),
};

// Status Change Product Category
const statusProductcategory = {
    body: Joi.object().keys({
        id: Joi.string().required(),
        is_active: Joi.boolean().required(),
    }),
};

module.exports = {
    productcategoryAdd,
    deleteProductcategory,
    updateProductcategory,
    statusProductcategory,
    editProductcategory,
};
