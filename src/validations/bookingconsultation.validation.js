const Joi = require('joi');
const { email, objectId } = require('./custom.validation');

// Add Booking Consultation
const bookingconsultationadd = {
  body: Joi.object().keys({
    fname: Joi.string().required(),
    lname: Joi.string().required(),
    email: Joi.string().required().custom(email),
    bdate: Joi.string().required(),
    btime: Joi.string().required(),
    joinlink: Joi.optional(),
  }),
};

// Update Booking Consultation
const bookingconsultationupdate = {
  body: Joi.object().keys({
    fname: Joi.string().required(),
    lname: Joi.string().required(),
    email: Joi.string().required().custom(email),
    bdate: Joi.string().required(),
    btime: Joi.string().required(),
    joinlink: Joi.optional(),
  }),
};
// Update Booking Consultation
const bookingconsultationupdateAdmin = {
  body: Joi.object().keys({
    joinlink: Joi.optional(),
    bookingstatus: Joi.string().required(),
  }),
};
// Status Booking Consultation
const bookingconsultationstatuschange = {
  body: Joi.object().keys({
    id: Joi.string().required(),
    bookingstatus: Joi.string().required(),
  }),
};

// Delete Booking Consultation
const bookingconsultationdelete = {
  body: Joi.object().keys({
    id: Joi.string().required(),
  }),
};

// Booking Consultation Details
const bookingconsultationdetails = {
  body: Joi.object().keys({
    id: Joi.string().custom(objectId),
  }),
};
// Booking Stripe Payment
const consultationpayment = {
  body: Joi.object().keys({
    tokenid: Joi.string().required(),
    bookingData: Joi.object(),
  }),
};

module.exports = {
  bookingconsultationadd,
  bookingconsultationdelete,
  bookingconsultationdetails,
  bookingconsultationupdate,
  bookingconsultationstatuschange,
  bookingconsultationupdateAdmin,
  consultationpayment,
};
