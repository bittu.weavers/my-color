const Joi = require('joi');
const { objectId } = require('./custom.validation');

// Add Product To Wishlist
const addproducttowishlist = {
  body: Joi.object().keys({
    productId: Joi.string().custom(objectId),
  }),
};

// Delete Product Wishlist
const deleteproductwishlist = {
  body: Joi.object().keys({
    id: Joi.string().required(),
  }),
};

module.exports = {
  addproducttowishlist,
  deleteproductwishlist,
};
