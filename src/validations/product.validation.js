const Joi = require('joi');

// Add product
const addProduct = {
    body: Joi.object().keys({
        title: Joi.string().required(),
        short_desc: Joi.optional(),
        regular_price: Joi.number().required(),
        sell_price: Joi.number().optional(),
        price: Joi.number().required(),
        details: Joi.optional(),
        ingredients: Joi.optional(),
        weight: Joi.optional(),
        category_id: Joi.optional(),
        sku: Joi.string().optional(),
        stock: Joi.number().required(),
        remaining_stock: Joi.number().optional(),
        faq: Joi.optional(),
        variation: Joi.optional(),
        pimage: Joi.optional(),
    }),
};

// Update product
const updateProduct = {
    body: Joi.object().keys({
        title: Joi.string().required(),
        short_desc: Joi.optional(),
        regular_price: Joi.number().required(),
        sell_price: Joi.number().optional(),
        price: Joi.number().required(),
        details: Joi.optional(),
        ingredients: Joi.optional(),
        weight: Joi.optional(),
        category_id: Joi.optional(),
        sku: Joi.string().optional(),
        stock: Joi.number().required(),
        remaining_stock: Joi.number().optional(),
        faq: Joi.optional(),
        variation: Joi.optional(),
        pimage: Joi.optional(),
    }),
};

// Add product Review
const addReview = {
    body: Joi.object().keys({
        rating_count: Joi.number().required(),
        review_desc: Joi.string().required(),
        product_id: Joi.string().required(),
    }),
};

// Update review status

const reviewStatusUpdate = {
    body: Joi.object().keys({
        id: Joi.string().required(),
        is_approve: Joi.boolean().required(),
    }),
};
// module.exports = { addProduct, addReview, reviewStatusUpdate };
// Add product
const productListAndFilter = {
    body: Joi.object().keys({
        search: Joi.string().optional(),
        category: Joi.array().optional(),
        filters: Joi.string().optional(),
        minPrice: Joi.number().optional(),
        maxPrice: Joi.number().optional(),
    }),
};

module.exports = {
    productListAndFilter,
    reviewStatusUpdate,
    addReview,
    updateProduct,
    addProduct
}