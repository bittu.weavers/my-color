const Joi = require('joi');
const { objectId, email } = require('./custom.validation');

const addAddress = {
  body: Joi.object().keys({
    userId: Joi.string().custom(objectId),
    firstname: Joi.string().required(),
    lastname: Joi.string().required(),
    addressline1: Joi.string().required(),
    addressline2: Joi.string().allow('').optional(),
    city: Joi.string().required(),
    zipcode: Joi.string().required(),
    state: Joi.string().required(),
    country: Joi.string().required(),
    countrycode: Joi.string().required(),
    phonecode: Joi.string().required(),
    phone: Joi.string().required(),
    addEmail: Joi.string().required().custom(email),
    addType: Joi.string().required(),
  }),
};

const getAddressList = {
  body: Joi.object().keys({
    userId: Joi.string().custom(objectId),
  }),
};

const addressDetail = {
  body: Joi.object().keys({
    userId: Joi.string().custom(objectId),
    addressId: Joi.string().custom(objectId),
  }),
};

const updateAddress = {
  body: Joi.object().keys({
    userId: Joi.string().custom(objectId),
    addressId: Joi.string().custom(objectId),
    firstname: Joi.string().required(),
    lastname: Joi.string().required(),
    addressline1: Joi.string().required(),
    addressline2: Joi.string().allow('').optional(),
    city: Joi.string().required(),
    zipcode: Joi.string().required(),
    state: Joi.string().required(),
    country: Joi.string().required(),
    countrycode: Joi.string().required(),
    phonecode: Joi.string().required(),
    phone: Joi.string().required(),
    addEmail: Joi.string().required().custom(email),
    addType: Joi.string().required(),
  }),
};

const deleteAddress = {
  body: Joi.object().keys({
    userId: Joi.string().custom(objectId),
    addressId: Joi.string().custom(objectId),
  }),
};

const changeStatusAddress = {
  body: Joi.object().keys({
    userId: Joi.string().custom(objectId),
    addressId: Joi.string().custom(objectId),
    primaryStatus: Joi.boolean(),
  }),
};

module.exports = {
  addAddress,
  getAddressList,
  addressDetail,
  updateAddress,
  deleteAddress,
  changeStatusAddress,
};
