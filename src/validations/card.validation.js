const Joi = require('joi');

const addCard = {
    body: Joi.object().keys({
        token: Joi.string().required(),
    }),
};;

module.exports = {
    addCard,
}