const { version } = require('../../package.json');

const swaggerDef = {
  openapi: '3.0.0',
  info: {
    title: 'My Color',
    version,
    license: {
      name: 'Weavers Web',
      url: 'https://www.weavers-web.com/',
    },
  },
  servers: [
    {
      url: `https://api-mycolor.weavers-web.com/v1`,
    },
  ],
};

module.exports = swaggerDef;
