const httpStatus = require('http-status');
const aws = require('aws-sdk');
const path = require('path');
const config = require('../config/config');
const catchAsync = require('../utils/catchAsync');

const s3 = new aws.S3({
    accessKeyId: config.s3.S3_ACCESS_KEY,
    secretAccessKey: config.s3.S3_SECRET_KEY,
    region: config.s3.S3_BUCKET_REGION,
});
const checkImage = async(req, res, next, body, folderPath) => {
    for (let key of Object.keys(body)) {
        if (body[key] instanceof Array) {
            await Promise.all(body[key].map(async(element,index) => {
                await checkImage(req, res, next, element, folderPath);
            }));
        } else {
            if (key == folderPath && typeof body[key] == 'object') {
                  await deleteUploadFile(body[key], folderName);
                //const url = await fileUploadSingle(req, res, body[key], key);
                //body[key] = 'url';
            }
        }
    }
}
const deleteUploadFile = async(old_url, folderPath) => {
    const folderName = folderPath.split('_')[0];
    let key = old_url.split(folderName+'/');
    const params1 = {
        Bucket: config.s3.S3_BUCKET_PATH,
        Key: folderName+'/'+decodeURIComponent(key[1]),
    };
    s3.deleteObject(params1, (error, data) => {
        if (error) {
            console.log(error);
        }
    });
};
const removeFile = (folderPath) => async(req, res, next) => {
    await checkImage(req, res, next, req.body, folderPath);
    next();
};

module.exports = removeFile;
