const httpStatus = require('http-status');
const tokenService = require('../services/token.service');

const auth = async (req, res, next) => {
  try {
    let token = null;
    if (req.headers.authorization) {
      const [key, value] = req.headers.authorization.split(' ');
      if (key === 'Bearer') {
        token = value;
      } else if (req.query && req.query.token) {
        token = req.query.token;
      }
    }
    // console.log(token);
    const verifyToken = await tokenService.checkverifyToken(token, 'refresh', 'user');
    if (verifyToken) {
      next();
    } else {
      res.status(httpStatus.FORBIDDEN).send({
        serverResponse: {
          code: 403,
          message: 'Session Expired',
        },
      });
    }
  } catch (error) {
    /**
     * If error then check the type of error
     * if error type is TokenExpiredError : send new token
     * else if error type is JsonWebTokenError : send error
     */
    if (error.name.toString() === 'TokenExpiredError') {
      res.status(httpStatus.FORBIDDEN).send({
        serverResponse: {
          code: 403,
          message: 'Not found Token',
        },
      });
    } else if (error.name.toString() === 'JsonWebTokenError') {
      res.status(httpStatus.UNAUTHORIZED).send({
        serverResponse: {
          code: 401,
          message: 'Please authenticate',
        },
      });
    } else {
      res.status(httpStatus.FORBIDDEN).send({
        serverResponse: {
          code: 403,
          message: `Error !!! Unexpected Token.${error.name.toString()}`,
        },
      });
    }
  }
};

module.exports = auth;
